# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 128.199.73.105 (MySQL 5.7.21-0ubuntu0.16.04.1)
# Database: touchstu_jb
# Generation Time: 2018-04-13 09:15:03 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table te_acl
# ------------------------------------------------------------

DROP TABLE IF EXISTS `te_acl`;

CREATE TABLE `te_acl` (
  `role_id` int(10) unsigned NOT NULL,
  `permission` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table te_appointments
# ------------------------------------------------------------

DROP TABLE IF EXISTS `te_appointments`;

CREATE TABLE `te_appointments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `schedule_id` int(10) unsigned NOT NULL,
  `syllabus_id` int(10) unsigned DEFAULT NULL,
  `seats` int(10) unsigned NOT NULL,
  `start_time` timestamp NULL DEFAULT NULL,
  `end_time` timestamp NULL DEFAULT NULL,
  `is_auto` tinyint(1) NOT NULL DEFAULT '0',
  `assessed_at` timestamp NULL DEFAULT NULL,
  `assessed_by` int(11) DEFAULT NULL,
  `notes` mediumtext COLLATE utf8_unicode_ci,
  `notes2` mediumtext COLLATE utf8_unicode_ci,
  `marked_at` timestamp NULL DEFAULT NULL,
  `marked_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `schedule_id` (`schedule_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table te_appointments_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `te_appointments_users`;

CREATE TABLE `te_appointments_users` (
  `appointment_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `syllabus_id` int(10) unsigned DEFAULT NULL,
  `is_exam` tinyint(1) DEFAULT '0',
  `is_missed` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  KEY `appointment_id` (`appointment_id`),
  KEY `user_id` (`user_id`),
  KEY `syllabus_id` (`syllabus_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table te_attendances
# ------------------------------------------------------------

DROP TABLE IF EXISTS `te_attendances`;

CREATE TABLE `te_attendances` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `student_id` int(10) unsigned NOT NULL,
  `appointment_id` int(10) unsigned NOT NULL,
  `attend` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `student_id` (`student_id`),
  KEY `appointment_id` (`appointment_id`),
  KEY `attend` (`attend`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table te_migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `te_migrations`;

CREATE TABLE `te_migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table te_notifications
# ------------------------------------------------------------

DROP TABLE IF EXISTS `te_notifications`;

CREATE TABLE `te_notifications` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `message` longtext COLLATE utf8_unicode_ci,
  `link` longtext COLLATE utf8_unicode_ci,
  `type` enum('danger','warning','info','success') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'info',
  `is_read` tinyint(1) NOT NULL DEFAULT '0',
  `observer` longtext COLLATE utf8_unicode_ci,
  `observer_id` longtext COLLATE utf8_unicode_ci,
  `observer_id2` longtext COLLATE utf8_unicode_ci,
  `observer_id3` longtext COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table te_remarks
# ------------------------------------------------------------

DROP TABLE IF EXISTS `te_remarks`;

CREATE TABLE `te_remarks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `student_id` int(10) unsigned NOT NULL,
  `appointment_id` int(10) unsigned NOT NULL,
  `s1` int(2) DEFAULT NULL,
  `s2` int(2) DEFAULT NULL,
  `s3` int(2) DEFAULT NULL,
  `s4` int(2) DEFAULT NULL,
  `s5` int(2) DEFAULT NULL,
  `s6` int(2) DEFAULT NULL,
  `s7` longtext COLLATE utf8_unicode_ci,
  `g1` int(2) DEFAULT NULL,
  `g2` int(2) DEFAULT NULL,
  `g3` int(2) DEFAULT NULL,
  `g4` longtext COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `g5` double(2,1) DEFAULT NULL,
  `g6` double(2,1) DEFAULT NULL,
  `g7` double(2,1) DEFAULT NULL,
  `g8` longtext COLLATE utf8_unicode_ci,
  `v1` double(2,1) DEFAULT NULL,
  `v2` double(2,1) DEFAULT NULL,
  `v3` double(2,1) DEFAULT NULL,
  `v4` longtext COLLATE utf8_unicode_ci,
  `r1` double(2,1) DEFAULT NULL,
  `r2` double(2,1) DEFAULT NULL,
  `r3` double(2,1) DEFAULT NULL,
  `r4` longtext COLLATE utf8_unicode_ci,
  `w1` double(2,1) DEFAULT NULL,
  `w2` double(2,1) DEFAULT NULL,
  `w3` double(2,1) DEFAULT NULL,
  `w4` longtext COLLATE utf8_unicode_ci,
  `s8` double(2,1) DEFAULT NULL,
  `s9` double(2,1) DEFAULT NULL,
  `s10` double(2,1) DEFAULT NULL,
  `s11` longtext COLLATE utf8_unicode_ci,
  `l1` double(2,1) DEFAULT NULL,
  `l2` double(2,1) DEFAULT NULL,
  `l3` double(2,1) DEFAULT NULL,
  `l4` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `student_id` (`student_id`),
  KEY `appointment_id` (`appointment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table te_schedules
# ------------------------------------------------------------

DROP TABLE IF EXISTS `te_schedules`;

CREATE TABLE `te_schedules` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `course_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `course_id` (`course_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table te_schedules_students
# ------------------------------------------------------------

DROP TABLE IF EXISTS `te_schedules_students`;

CREATE TABLE `te_schedules_students` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `schedule_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `start_date` date NOT NULL,
  `expiry_date` date NOT NULL,
  `total_session` int(11) DEFAULT '0',
  `used_session` int(11) DEFAULT '0',
  `exam` int(11) DEFAULT '0',
  `used_exam` int(11) DEFAULT '0',
  `miss_count` int(10) unsigned DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `schedule_id` (`schedule_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table te_schedules_tutors
# ------------------------------------------------------------

DROP TABLE IF EXISTS `te_schedules_tutors`;

CREATE TABLE `te_schedules_tutors` (
  `schedule_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table te_settings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `te_settings`;

CREATE TABLE `te_settings` (
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table te_settings_notifications
# ------------------------------------------------------------

DROP TABLE IF EXISTS `te_settings_notifications`;

CREATE TABLE `te_settings_notifications` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `message` text COLLATE utf8_unicode_ci,
  `link` text COLLATE utf8_unicode_ci,
  `type` enum('danger','warning','info','success') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'info',
  `target` text COLLATE utf8_unicode_ci NOT NULL,
  `target_id1` text COLLATE utf8_unicode_ci,
  `target_id2` text COLLATE utf8_unicode_ci,
  `target_id3` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table te_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `te_users`;

CREATE TABLE `te_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role` int(11) NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ic` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8_unicode_ci,
  `avatar` text COLLATE utf8_unicode_ci,
  `age` int(11) DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remarks` text COLLATE utf8_unicode_ci,
  `ref_id` text COLLATE utf8_unicode_ci,
  `start_date` date DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_inactive` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `role` (`role`),
  KEY `is_inactive` (`is_inactive`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table te_weblog
# ------------------------------------------------------------

DROP TABLE IF EXISTS `te_weblog`;

CREATE TABLE `te_weblog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `path` text NOT NULL,
  `method` text NOT NULL,
  `input` text,
  `uid` int(11) DEFAULT NULL,
  `role` int(11) DEFAULT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
