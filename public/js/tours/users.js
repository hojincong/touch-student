$().ready(function(){
	if(RegExp('tour', 'gi').test(window.location.search)){
		var helpGuide = introJs();
		if(tab == 'schedules'){
			helpGuide.setOptions({
				steps:[
					{
						element: '#user-header',
						intro: 'These are your personal infos.'
					},
					{
						element: '#main-header-tabs li:nth-child(1)',
						intro: 'Your class details will be shown here.',
						position: 'right'
					},
					{
						element: '.panel-group',
						intro: 'You can check your progression. Pdf report is also available, click "Download Report".',
						position: 'top'
					}
				]
			});
		}else if(tab == 'appointments'){
			helpGuide.setOptions({
				steps:[
					{
						element: '#main-header-tabs li:nth-child(2)',
						intro: 'Here are your appointment history.',
						position: 'right'
					},
					{
						element: '.table-responsive',
						intro: 'Newly booked appointment will also be shown here. You can check them anytime and never miss a class.',
						position: 'top'
					}
				]
			});
		}else if(tab == 'assessments'){
			helpGuide.setOptions({
				steps:[
					{
						element: '#main-header-tabs li:nth-child(3)',
						intro: 'Here are the assessments given by your teacher. ',
						position: 'right'
					},
					{
						element: '.appointment-remarks',
						intro: 'We suggest you check them after every appointment. Understand your strengths and weaknesses is helpful in learning.',
						position: 'top'
					}
				]
			});
		}
		helpGuide.setOption('doneLabel', 'Next Page').oncomplete(function(){
			window.location.href = nextTour;
		});
		helpGuide.start();
	}
});