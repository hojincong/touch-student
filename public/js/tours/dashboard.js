$().ready(function(){
	if(RegExp('tour', 'gi').test(window.location.search)){
		var helpGuide = introJs();
		helpGuide.setOptions({
			steps:[
				{
					element: '.table-responsive',
					intro: 'New notifications will be shown here. We will inform you about holidays, exam and when you\'re absent.'
				},
			]
		});
		helpGuide.setOption('doneLabel', 'Next Page').oncomplete(function(){
			window.location.href = nextTour;
		});
		helpGuide.start();
	}
});