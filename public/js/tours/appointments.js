function loadTour(){
	if(RegExp('tour', 'gi').test(window.location.search)){
		var helpGuide = introJs();
		helpGuide.setOptions({
			steps:[
				{
					element: '.fc-center',
					intro: 'Book your next appointment here! Just click on the time you want. Remember, you can still cancel them anytime but not after 12AM (Midnight) on day of the appointment. Blue color represents your appointments, green represents available and red represents no more seats'
				},
				{
					element: '#calendar-help',
					intro: 'Description about booking will be shown here if you are not clear.'
				},
				{
					element: ($('#menu').css('display') == 'block') ? '#menu .help' : '#bar-header .help',
					intro: 'Thanks for finishing the tour. We hope you enjoy our system and you can replay it here. You can also call us at 07-2266893.',
					position:  'right'
				}
			]
		});
		helpGuide.start();
	}
}