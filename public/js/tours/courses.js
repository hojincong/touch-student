$().ready(function(){
	if(RegExp('tour', 'gi').test(window.location.search)){
		var helpGuide = introJs();
		helpGuide.setOptions({
			steps:[
				{
					element: '.table-responsive',
					intro: 'Your courses will shown here. You can download handouts and mp3 files used in class after every appointment!'
				},
			]
		});
		helpGuide.setOption('doneLabel', 'Next Page').oncomplete(function(){
			window.location.href = nextTour;
		});
		helpGuide.start();
	}
});