$().ready(function(){
	$('img').each(function(i, s){
		$(this).on('load', function(){
			EXIF.getData(this, function() {
				var w = $(this).outerWidth();
			 	var h = $(this).outerHeight();
			 	var isChrome = !!window.chrome && !(!!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0);

			 	if((EXIF.getTag(this, 'Orientation') == '6' || EXIF.getTag(this, 'Orientation') == '8') && (isChrome)){
			 		$(this).css({'transform' : 'rotate(90deg)', 'transition' : 'transform 0s'});
			 		parent = $(this).parent();
			 		if(parent[0].id != 'menu-header'){
			 			parent.css('height', w+16);
			 		}
			 	}
			 });
		});

		if(this.complete){
			$(this).load();
		}
	});
});