$().ready(function(){

	var calendar = $('#calendar');
	var schedules = $('#calendar-schedules').data('id');
	if(typeof calendarLoaded == 'undefined')
		calendarLoaded = false;
	if(calendar.length && (calendarLoaded == false)){
		var eventUrl = '/appointment/events/';
		if(schedules) eventUrl += schedules;
		calendar.fullCalendar({
			defaultView: 'agendaWeek',
			events: eventUrl,
			height: "auto",
			editable: false,
			minTime: '08:00',
			slotDuration: '00:15:00',
			allDaySlot: false,
			firstDay: 1,
			header: {
				left: '',
				center: 'title',
				right: 'today prev,next'
			},
			viewRender: function(view, element){
				var header = $('td.fc-widget-header');
				header.width(header.outerWidth());
				header.affix({offset: {top: header.offset().top}});
			},
			windowResize: function(view) {
				var header = $('td.fc-widget-header');
				header.width('auto');
				header.width(header.outerWidth());
				header.affix({offset: {top: header.offset().top}});
			},
			eventMouseover: function(event, jse, view){
				field = $(jse.currentTarget);
				if (!(field.data && field.data('bs.tooltip'))) {
					field.tooltip({
						title: event.title,
						container: 'body'
					});
					field.tooltip('toggle');
				}
			}
		});
	}

	var paginator = $('#date-paginator');
	if(paginator.length){
		paginator.datepaginator({
			itemWidth: '55',
			showOffDays: false,
			selectedDate: selectedDate,
			onSelectedDateChanged: function(event, date){
				window.location = url + '/' + date.format('YYYY/MM/DD');
			}
		});
	}

	var mactivator = $('#month-activator');
	if(mactivator.length){
		mactivator.popover({
			container: 'body',
			content: function(){
				return '<div id="month-selector"></div>';
			},
			html: true,
			placement: 'bottom',
			trigger: 'manual'
		});

		mactivator.click(function(){
			mactivator.popover('toggle');
		});

		mactivator.on('inserted.bs.popover', function(){
			var mpaginator = $('#month-selector');
			if(mpaginator.length){
				mpaginator.datepicker({
					format: "mm",
					startView: 1,
					minViewMode: 1,
					maxViewMode: 4
				});
				mpaginator.datepicker('update', new Date(year, month-1, 1));
				mpaginator.datepicker().on('changeMonth', function(e){
					window.location = url + '/' + e.format("yyyy/mm");
				});
			}
		});

		mactivator.on('show.bs.popover', function(){
			$('#month-selector').datepicker('show');
		});

		mactivator.on('hidden.bs.popover', function(){
			$('#month-selector').datepicker('hide');
			$('#month-selector').datepicker('destroy');
		});
	}

	var start_time = $('#start_time');
	var end_time  = $('#end_time');
	var options = {
		format: 'YYYY-MM-DD HH:mm',
		sideBySide: true
	};
	if(start_time.length)
		start_time.datetimepicker(options);
	if(end_time.length)
		end_time.datetimepicker(options);

	/* repeating event in form */

	//toggle additional form
	var is_repeating = $('input[name=is_repeating]');
	var repeating = $('.repeating');
	var repeating_form = function(){
		var val = $(is_repeating).is(':checked');
		if(val){
			repeating.show();
		}else{
			repeating.hide();
		}
	};
	is_repeating.change(repeating_form);
	repeating_form();

	//datetimepicker
	var end_date = $('#end_date');
	var roptions = {
		format: 'YYYY-MM-DD'
	};
	if(end_date.length)
		end_date.datetimepicker(roptions);

	//preview calendar
	var prevcal = $('#preview-cal');
	if(prevcal.length){
		var updatePrevCal = function(){
			var start = $('#start_time').data('DateTimePicker').date();
			var until = $('#end_date').data('DateTimePicker').date();

			if(start==null || until==null) return;

			//generate events
			var events = [];
			start = moment(start.format('YYYY-MM-DD'));
			do{
				events.push({
					start: start.format('YYYY-MM-DD'),
					rendering: 'background'
				});
				start.add(7, 'days');
			}while(start.isSameOrBefore(until));

			//update calendar
			prevcal.fullCalendar('destroy');
			prevcal.fullCalendar({
				defaultView: 'month',
				height: 'auto',
		  		events: events
			});
		};
		$('#start_time').on('dp.change', updatePrevCal);
		$('#end_date').on('dp.change', updatePrevCal);
		updatePrevCal();
	}

	/*  */

	var search = $('input[name="name"]');
	var rows = $('#appointment-student-list tbody tr');
	if(search.length && rows.length){
		search.keyup(function(){
			var value = this.value.trim();
			if((value == '')){
				rows.show();
				return;
			}

			rows.hide();

			rows.filter(function(i, v){
				var $t = $(this);
				return ($t.text().toLowerCase().indexOf(value.toLowerCase()) >= 0);
			}).show();
		});
	}

	var formSubmit = false;
	var currentSubmit;

	var student_form = $('#appointment-student-form');
	var user_id_add = $('#appointment-student-form input[name="user_id"]');
	var student_add = $('.appointment-student-add');
	if(student_form.length && student_add.length && user_id_add.length){
		student_add.click(function(){
			var id = $(this).data('user-id');
			user_id_add.val(id);

			if(formSubmit){
				return;
			}else{
				event.preventDefault();
				currentSubmit = student_form;
				$('#modal-student-form').modal('show');
			}
		});
	}

	var student_form_remove = $('#appointment-student-form-remove');
	var user_id = $('#appointment-student-form-remove input[name="user_id"]');
	var student_remove = $('.appointment-student-remove');
	if(student_form_remove.length && student_remove.length && user_id.length){
		student_remove.click(function(){
			var id = $(this).data('user-id');
			user_id.val(id);

			if(formSubmit){
				return;
			}else{
				event.preventDefault();
				currentSubmit = student_form_remove;
				$('#modal-student-form-remove').modal('show');
			}
		});
	}

	var student_form_submit = $('.modal-student-form-submit');
	if(student_form_submit.length){
		student_form_submit.click(function(){
			formSubmit = true;
			currentSubmit.submit();
		});
	}

	var lesson_form = $('#appointment-lesson-add');
	var lesson_id = $('#appointment-lesson-add input[name="lesson_id"]');
	var lesson_add = $('.lesson-add');
	if(lesson_form.length && lesson_add.length && lesson_id.length){
		lesson_add.click(function(){
			var id = $(this).data('lesson-id');
			lesson_id.val(id);
			lesson_form.submit();
		});
	}

	var assessment_question = $('.assessment-question');
	if(assessment_question.length){
		assessment_question.popover();
	}

});