$().ready(function(){

	if($('#ref_id_auto').prop('checked')){
		$('input[name=ref_id]').attr('disabled', "true");
	}
	$('#ref_id_auto').change(function(){
		if(this.checked){
			$('input[name=ref_id]').attr('disabled', "true");
		}else{
			$('input[name=ref_id]').removeAttr('disabled');
		}
	});

	if(!$('#password_auto').prop('checked')){
		$('input[name=password]').removeAttr('disabled');
	}
	$('#password_auto').change(function(){
		if(this.checked){
			$('input[name=password]').attr('disabled', "disabled");
		}else{
			$('input[name=password]').removeAttr('disabled');
		}
	});

	val = $('select[name=role]').val();
	if((val == '1') || (val == '2')){
		$('#password_auto').prop('checked', false);
		$('input[name=password]').removeAttr('disabled');
		$('#input_ic .emphasis').hide();
		$('#input_password_auto').hide();
		$('#user-schedule-panel').hide();
	}
	$('select[name=role]').change(function(){
		val = $(this).val();
		if((val == '1') || (val == '2')){
			$('#password_auto').prop('checked', false);
			$('input[name=password]').removeAttr('disabled');
			$('#input_ic .emphasis').hide();
			$('#input_password_auto').hide();
			$('#user-schedule-panel').hide();
		}else{
			$('#password_auto').prop('checked', true);
			$('input[name=password]').attr('disabled', "disabled");
			$('#input_ic .emphasis').show();
			$('#input_password_auto').show();
			$('#user-schedule-panel').show();
		}
	});

	var start_date = $('#start_date');
	if(start_date.length){
		start_date.datetimepicker({
			format: "YYYY-MM-DD"
		});
	}
	
	var sstart_date = $('.sstart_date');
	if(sstart_date.length){
		sstart_date.datetimepicker({
			format: "YYYY-MM-DD"
		});
	}

	var sexpiry_date = $('.sexpiry_date');
	if(sexpiry_date.length){
		sexpiry_date.datetimepicker({
			format: "YYYY-MM-DD"
		});
	}

	$('#user-ap-helper form').submit(function(event){
		event.preventDefault();
		window.location.href = $(this).prop('action') + "/" + $(this).children().children('select').val();
	});

	var assessment_question = $('.assessment-question');
	if(assessment_question.length){
		assessment_question.popover();
	}

});