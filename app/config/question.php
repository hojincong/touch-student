<?php

return array(

	'_meta'=> array(
		'versions'	=> array(
			'ver1'	=> '2016-05-25', //last day
			'ver2'	=> 'default'
		)
	),

	'ver1' => array(

		'speaking' => array(
			array(
				'title' 	=> 'Did the student speak in class?',
				'type'		=> 'range',
				'model'		=> 's1'
			),
			array(
				'title' 	=> 'Is there fluency in his/her speaking?',
				'type'		=> 'range',
				'model'		=> 's2'
			),
			array(
				'title' 	=> 'Did he/she give relevant points?',
				'type'		=> 'range',
				'model'		=> 's3'
			),
			array(
				'title' 	=> 'Did he/she elaborate the points?',
				'type'		=> 'range',
				'model'		=> 's4'
			),
			array(
				'title' 	=> 'Did he/she participate in class activities?',
				'type'		=> 'range',
				'model'		=> 's5'
			),
			array(
				'title' 	=> 'Did he/she cooperate with other students?',
				'type'		=> 'range',
				'model'		=> 's6'
			),
			array(
				'title' 	=> 'Others (please specify)',
				'type'		=> 'textarea',
				'model'		=> 's7'
			)
		),
		'grammar' => array(
			array(
				'title' 	=> 'Can the student understand teacher\'s explanation?',
				'type'		=> 'range',
				'model'		=> 'g1'
			),
			array(
				'title' 	=> 'Did he/she ask titles when doesn\'t understand?',
				'type'		=> 'range',
				'model'		=> 'g2'
			),
			array(
				'title' 	=> 'Can he/she do the exercise without problem?',
				'type'		=> 'range',
				'model'		=> 'g3'
			),
			array(
				'title' 	=> 'Others (please specify)',
				'type'		=> 'textarea',
				'model'		=> 'g4'
			)
		)

	),

	'ver2' => array(

		'grammar'	=> array(
			array(
				'title' 	=> 'S-V Agreement',
				'question'	=> 'Did the student use correct subject-verb agreement?',
				'model'		=> 'g5'
			),
			array(
				'title' 	=> 'Parallelism',
				'question'	=> 'Did the student implement parallelism in verb use, noun-pronoun agreement, proper use of adjectives, adverbs, prepositions, conjunctions, interjection and tenses?',
				'model'		=> 'g6'
			),
			array(
				'title' 	=> 'Spelling & Sentence Structure',
				'question'	=> 'Did the student punctuate sentences accordingly with the correct spelling and well-constructed varied sentence structure?',
				'model'		=> 'g7'
			),
			array(
				'title' 	=> 'Others (please specify)',
				'type'		=> 'textarea',
				'model'		=> 'g8'
			)
		),

		'vocabulary'	=> array(
			array(
				'title' 	=> 'Relevance',
				'question'	=> 'Were most of the words highly related to the major ideas in the text?',
				'model'		=> 'v1'
			),
			array(
				'title' 	=> 'Word Choice',
				'question'	=> 'Were all the word choices appropriate, detailed and specific?',
				'model'		=> 'v2'
			),
			array(
				'title' 	=> 'Accuracy',
				'question'	=> 'Were the sentences expressed accurately as in the text and include some elaboration?',
				'model'		=> 'v3'
			),
			array(
				'title' 	=> 'Others (please specify)',
				'type'		=> 'textarea',
				'model'		=> 'v4'
			)
		),

		'speaking'	=> array(
			array(
				'title' 	=> 'Pronunciation',
				'question'	=> 'Was the delivery of speech clear, fluid, sustained and intelligible?',
				'model'		=> 's8'
			),
			array(
				'title' 	=> 'Conversational Mode',
				'question'	=> 'Was the expression, volume, intonation and pace at a conversational mode?',
				'model'		=> 's9'
			),
			array(
				'title' 	=> 'Eloquence',
				'question'	=> 'Did the student speak with short pauses and self-correct with words or sentence structures?',
				'model'		=> 's10'
			),
			array(
				'title' 	=> 'Others (please specify)',
				'type'		=> 'textarea',
				'model'		=> 's11'
			)
		),

		'listening' => array(
			array(
				'title' 	=> 'Attentiveness',
				'question'	=> 'Was the student attentive, courteous and sensitive to the ideas?',
				'model'		=> 'l1'
			),
			array(
				'title' 	=> 'Productivity',
				'question'	=> 'Was the student curious, attentive and productive to the lesson taught?',
				'model'		=> 'l2'
			),
			array(
				'title' 	=> 'Association',
				'question'	=> 'Did the student link to prior knowledge in the subject and assimilate knowledge from other areas?',
				'model'		=> 'l3'
			),
			array(
				'title' 	=> 'Others (please specify)',
				'type'		=> 'textarea',
				'model'		=> 'l4'
			)
		),

		'reading'	=> array(
			array(
				'title' 	=> 'Comprehension',
				'question'	=> 'Were the answers given mostly correct and demonstrate excellent comprehension with opinions fully justified?',
				'model'		=> 'r1'
			),
			array(
				'title' 	=> 'Supporting Evidence',
				'question'	=> 'Were the answers given mostly include supporting evidences from the lesson with paraphrases often included in answers?',
				'model'		=> 'r2'
			),
			array(
				'title' 	=> 'Connections',
				'question'	=> 'Did the students make innovative connections, asked probing questions, make logical and creative inferences?',
				'model'		=> 'r3'
			),
			array(
				'title' 	=> 'Others (please specify)',
				'type'		=> 'textarea',
				'model'		=> 'r4'
			)
		),

		'writing'	=> array(
			array(
				'title' 	=> 'Coherence',
				'question'	=> 'Did the student innovatively and creatively justified the central idea(topic sentence), clearly defined parapgrahs, transitions and other structures appropriately?',
				'model'		=> 'w1'
			),
			array(
				'title' 	=> 'Vocabulary Precision/Choice',
				'question'	=> 'Did the student skillfully and imaginatively use varied language/vocabulary appropriate to the task?',
				'model'		=> 'w2'
			),
			array(
				'title' 	=> 'Semantics',
				'question'	=> 'Did the student use complete and well-structured sentences with less errors in spelling, grammar and punctuation?',
				'model'		=> 'w3'
			),
			array(
				'title' 	=> 'Others (please specify)',
				'type'		=> 'textarea',
				'model'		=> 'w4'
			)
		)

	)

);