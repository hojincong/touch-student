<?php

return array(

	'attachment_path' => __DIR__ . getenv('ATTACHMENT_PATH'),

	'avatar_path' => __DIR__ . getenv('AVATAR_PATH'),

	'telno'	=> getenv('CONTACT_NO', '07-2266893'),

	'super_admin' => explode(",", getenv('SUPER_ADMIN'))

);