<?php

App::missing(function($exception){
	return Redirect::route('users.index')->with('msg_error', 'Sorry, Page Not Found.');
});

App::error(function ($e) {
	
	if(is_a($e, 'Illuminate\Database\Eloquent\ModelNotFoundException')){
		$action = array(
			'User' 			=> 'UserController@index',
			'Appointment' 	=> 'AppointmentController@index',
			'Attachment'	=> 'HomeController@index',
			'Course'		=> 'CourseController@index',
			'Schedule'		=> 'ScheduleController@index',
			'Syllabus'		=> 'CourseController@index'
		);
		$message = array(
			'User' 			=> 'User Not Found.',
			'Appointment' 	=> 'Appointment Not Found.',
			'Attachment'	=> 'Attachment Not Found.',
			'Course'		=> 'Course Not Found.',
			'Schedule'		=> 'Class Not Found.',
			'Syllabus'		=> 'Syllabus Not Found.'
		);
		$model = $e->getModel();

		if(isset($action[$model]) && isset($message[$model]))
			return Redirect::action($action[$model])->with('msg_error', $message[$model]);
	}elseif(!Config::get('app.debug')){
		return Response::view('errors.default');
	}
		
});