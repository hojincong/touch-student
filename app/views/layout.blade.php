<!doctype html>
<html>
	<head>
		<title>Touch Learning & Edu Advisory Sdn Bhd</title>
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,600,700,300&amp;subset=latin" rel="stylesheet" type="text/css">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link rel="stylesheet" href="{{ asset('lib/bootstrap/dist/css/bootstrap.min.css') }}?v={{ $assets_version }}">
		<link rel="stylesheet" href="{{ asset('lib/fontawesome/css/font-awesome.min.css') }}?v={{ $assets_version }}">
		<link rel="stylesheet" href="{{ asset('lib/ionicons/css/ionicons.min.css') }}?v={{ $assets_version }}">
		<link rel="stylesheet" href="{{ asset('css/theme.css') }}?v={{ $assets_version }}">
		<link rel="stylesheet" href="{{ asset('css/style.css') }}?v={{ $assets_version }}">
		<link rel="stylesheet" href="{{ asset('css/mainmenu.css') }}?v={{ $assets_version }}">
		@section('header_css')
		@show
	</head>
	<body>
		<div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="myModalLabel">Delete Warning</h4>
					</div>
					<div class="modal-body">
						Are you sure you want to delete? Record cannot be recovered.
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
						<button id="modal-delete-btn" type="button" class="btn btn-danger">Delete</button>
					</div>
				</div>
			</div>
		</div>

		<!-- Header Bar -->
		<nav id="bar-header" class="navbar navbar-inverse navbar-fixed-top">
		  	<div class="container-fluid">
		    	<div class="navbar-header">
		    		@if($cu->role == 3)
		    		<span class="help btn navbar-btn"><a class="help btn navbar-btn" href="{{ action('HomeController@getIndex') }}?tour=1"><i class="fa fa-question"></i></a></span>
		    		@else
					=<a class="btn navbar-btn" href="{{ action('SettingController@getIndex') }}"><i class="fa fa-cog"></i></a>
		    		@endif
		      		<a class="navbar-brand" href="{{ action('UserController@index') }}">TouchEdu</a>
		      		<a id="logout" href="{{ action('AuthController@getLogout') }}" class="btn navbar-btn">Logout</a>
		    	</div>
		    </div>
		</nav>

		<!-- Footer Bar -->
		<div id="bar-footer" class="navbar navbar-fixed-bottom">
			<a class="{{ $active == 'dashboard' ? 'active' : '' }}" href="{{ action('HomeController@getIndex') }}">
				<span class="fa fa-envelope"></span>
				<span class="badge">{{ $count['dashboard'] or 0 }}</span>
			</a>
			<a class="{{ $active == 'user' ? 'active' : '' }}" href="{{ action('UserController@index') }}">
				@if($cu->role <= 1)
				<span class="fa fa-users"></span>
				<span class="badge">{{ $count['user'] or 0}}</span>
				@else
				<span class="fa fa-user"></span>
				@endif
			</a>
			<a class="{{ $active == 'course' ? 'active' : '' }}" href="{{ action('CourseController@index') }}">
				<span class="fa fa-book"></span>
				@if($cu->role <= 1)
				<span class="badge">{{ $count['course'] or 0 }}</span>
				@endif
			</a>
			@if($cu->role <= 2)
			<a class="{{ $active == 'schedule' ? 'active' : '' }}" href="{{ action('ScheduleController@index') }}">
				<span class="fa fa-clock-o"></span>
				@if($cu->role <= 1)
				<span class="badge">{{ $count['schedule'] or 0 }}</span>
				@endif
			</a>
			@endif
			<a class="{{ $active == 'appointment' ? 'active' : '' }}" href="{{ action('AppointmentController@index') }}">
				<span class="fa fa-calendar"></span>
			</a>
		</div>

		<!-- Main Menu(Left) -->
		<div id="menu">
			<div id="menu-header">
				<img src="{{ $cu->avatar != '' ? asset('img/avatar/'.$cu->avatar) : asset('img/gravatar.png') }}" width='80' height='80' alt="" class="img-circle">
				<span class="semibold">{{ $cu->name or $cu->email }}</span>
				<br>
				@if($cu->role <= 1)
				<a href="{{ route('users.edit', array($cu->id)) }}" class="slim">Profile</a> |
				@elseif($cu->role == 3)
				<a href="{{ action('HomeController@getIndex') }}?tour=1" class="help slim">Help</a> |
				@endif
				<a href="{{ action('AuthController@getLogout') }}" class="slim">Logout</a>
			</div>
			<div id="menu-nav">
				<ul class="nav nav-pills nav-stacked">
					<li class="{{ $active == 'dashboard' ? 'active' : '' }}"><a href="{{ action('HomeController@getIndex') }}">
						<span class="icons" data-icon="ion-ios-email"></span> 
						Notifications 
						<span class="badge">{{ $count['dashboard'] or 0 }}</span>
					</a></li>
					<li class="{{ $active == 'user' ? 'active' : '' }}"><a href="{{ action('UserController@index') }}">
						<i class="icons" data-icon="ion-ios-people"></i>
						@if($cu->role <= 1)
						Users <span class="badge">{{ $count['user'] or 0 }}</span>
						@elseif($cu->role == 2)
						Users
						@else
						My Records
						@endif
					</a></li>
					<li class="{{ $active == 'course' ? 'active' : '' }}"><a href="{{ action('CourseController@index') }}">
						<i class="icons" data-icon="ion-ios-paper"></i>
						@if($cu->role <= 1)
						Courses <span class="badge">{{ $count['course'] or 0 }}</span>
						@else
						My Courses
						@endif
					</a></li>
					@if($cu->role < 3)
					<li class="{{ $active == 'schedule' ? 'active' : '' }}"><a href="{{ action('ScheduleController@index') }}">
						<i class="icons" data-icon="ion-ios-clock"></i>
						@if($cu->role <= 1)
						Classes <span class="badge">{{ $count['schedule'] or 0 }}</span>
						@elseif($cu->role == 2)
						My Classes
						@endif
					</a></li>
					@endif
					<li class="{{ $active == 'appointment' ? 'active' : '' }}"><a href="{{ action('AppointmentController@index') }}">
						<i class="icons" data-icon="ion-ios-calendar"></i>
						Appointments
					</a></li>
					@if($cu->role <= 1)
					<li class="divider"></li>
					<li class="{{ $active == 'setting' ? 'active' : '' }}"><a href="{{ action('SettingController@getIndex') }}">
						<i class="icons" data-icon="ion-ios-gear"></i> 
						Settings
					</a></li>
					@endif
				</ul>
			</div>
		</div>

		<div id="breadcrumb">
			<div id="menu-logo">
				<h1><img src="{{ asset('img/logo.png') }}" alt="">TOUCH LEARNING</h1>
			</div>
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-6 col-md-9">
						<ol class="breadcrumb">
							@section('breadcrumb')
							@show
						</ol>
					</div>
					<div class="col-sm-6 col-md-3">
						@if($cu->role <= 1)
						{{ Form::open(array('action' => array('UserController@search'), 'method' => 'get', 'id' => 'user-search-form')) }}
								<!--<div class="input-group">
									@if(isset($search) && $search)
									<span class="input-group-btn">
										<a href="{{ action('UserController@index') }}" class="btn btn-danger">
											<span class="fa fa-close"></span>
										</a>
									</span>
									@endif
									<input type="search" name="q" value="{{ Input::get('q') }}" placeholder="Searching Any User?" class="form-control">
									<span class="input-group-btn">
										<button type="submit" class="btn btn-primary">
											<span class="fa fa-search"></span>
										</button>
									</span>
								</div>-->
						{{ Form::close() }}
						@endif
					</div>
				</div>
			</div>
		</div>

		<div id="main-header">
			@section('header')
			@show
		</div>

		<div id="main-wrapper">
			<div class="container-fluid">
				<div id="header" class="row">
					<div id="header-page-title" class="col-md-6">
						@section('page_title')
						@show
					</div>
					<div id="header-page-action" class="col-md-6 right">
						@section('page_action')
						@show
					</div>
				</div>
				<div id="message" class="row">
					<div class="col-md-12">
						@if(Session::has('msg_success'))
							<div class="alert alert-success alert-dismissible" role="alert">
							  	<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
							  	{{ Session::get('msg_success') }}
							</div>
						@endif
						@if(Session::has('msg_error'))
							<div class="alert alert-danger alert-dismissible" role="alert">
							  	<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
							  	{{ Session::get('msg_error') }}
							</div>
						@endif
					</div>
				</div>
				@yield('contents')
				<footer>
					<p>Need Help? Call <span style="text-decoration: underline">{{ Config::get('touchedu.telno') }}</span> © 2015{{ '-'.date('Y') }} Touch Learning & Edu Advisory Sdn Bhd</p>
				</footer>
			</div>
		</div>

		<script src="{{ asset('lib/jquery/dist/jquery.min.js') }}"></script>
		<script src="{{ asset('lib/jquery-ui/jquery-ui.min.js') }}"></script>
		<script src="{{ asset('lib/bootstrap/dist/js/bootstrap.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('lib/exif-js/exif.js') }}"></script>
		<script type="text/javascript" src="{{ asset('js/imgfix.js') }}"></script>
		@if($cu->role == 3)
		<link rel="stylesheet" href="{{ asset('lib/intro.js/minified/introjs.min.css') }}">
		<script src="{{ asset('lib/intro.js/minified/intro.min.js') }}"></script>
		@endif
		<script type="text/javascript">
		$().ready(function(){
			var confirmDelete = false;
			var currentDelete;
			$('input[name="_method"][value="DELETE"]').parent().submit(function(event){
				if(confirmDelete){
					return;
				}else{
					event.preventDefault();
					currentDelete = $(this);
					$('#modal-delete').modal('show');
				}
			});
			$('#modal-delete-btn').click(function(){
				confirmDelete = true;
				currentDelete.submit();
			});

			$('#menu-nav .nav li').each(function(){

				if(!$(this).has('.icons')) return;

				var icon = $(this).children('a').children('.icons');

				if($(this).hasClass('active')){
					icon.addClass(icon.data('icon'));
				}else{
					icon.addClass(icon.data('icon')+"-outline");

					icon.parent('a').hover(function(){
						icon.addClass(icon.data('icon')).removeClass(icon.data('icon')+"-outline");
					}, function(){
						icon.addClass(icon.data('icon')+"-outline").removeClass(icon.data('icon'));
					});
				}

			});

			var icon = $('#menu-nav .nav li.active a .icons');
			icon.addClass(icon.data('icon'));
		});
		</script>
		@section('footer_js')
		@show
		@if(App::environment('production'))
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-62464804-1', 'auto');
		  ga('send', 'pageview');
		</script>
		@endif
	</body>
</html>