<!doctype html>
<html>
	<head>
		<title>Touch Learning & Edu Advisory Sdn Bhd</title>
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,600,700,300&amp;subset=latin" rel="stylesheet" type="text/css">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link rel="stylesheet" href="{{ asset('lib/bootstrap/dist/css/bootstrap.min.css') }}?v={{ $assets_version }}">
		<link rel="stylesheet" href="{{ asset('lib/fontawesome/css/font-awesome.min.css') }}?v={{ $assets_version }}">
		<link rel="stylesheet" href="{{ asset('lib/ionicons/css/ionicons.min.css') }}?v={{ $assets_version }}">
		<link rel="stylesheet" href="{{ asset('css/theme.css') }}?v={{ $assets_version }}">
		<link rel="stylesheet" href="{{ asset('css/style.css') }}?v={{ $assets_version }}">
		<link rel="stylesheet" href="{{ asset('css/mainmenu.css') }}?v={{ $assets_version }}">
		@section('header_css')
		@show
	</head>
	<body>
		@yield('contents')
		
		<script src="{{ asset('lib/jquery/dist/jquery.min.js') }}"></script>
		<script src="{{ asset('lib/bootstrap/dist/js/bootstrap.min.js') }}"></script>
		<script src="{{ asset('lib/jquery-ui/jquery-ui.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('lib/exif-js/exif.js') }}"></script>
		<script type="text/javascript" src="{{ asset('js/imgfix.js') }}"></script>
		@section('footer_js')
		@show
	</body>
</html>