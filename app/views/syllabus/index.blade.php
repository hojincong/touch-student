@extends('layout')

@section('breadcrumb')
	<li><a href="{{ action('CourseController@index') }}">Course Management</a></li>
	<li class="active">
		{{ io((isset($is_exam) && $is_exam), 'Edit Exam', 'Edit Lessons') }}
	</li>
@stop

@section('page_title')
	<h1>Edit Course</h1>
@stop

@section('page_action')
@if(isset($course))
	<a href="{{ route('courses.show', array($course->id)) }}" class="btn btn-primary"><i class="fa fa-eye"></i> View</a>
	{{ Form::open(array('action' => array('CourseController@destroy', $course->id), 'method' => 'delete', 'class' => 'form-ib')) }}
		<button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i> Delete</button>
	{{ Form::close() }}
@endif
@stop

@section('footer_js')
<script type="text/javascript" src="{{ asset('lib/jqueryui-touch-punch/jquery.ui.touch-punch.min.js') }}"></script>
	<script type="text/javascript">
	$().ready(function(){
		$('#syllabus-list').sortable({
			items: "> .panel",
			handle: ".handle"
		});

		$('#syllabus-reorder').submit(function(){
			order = [];
			$('#syllabus-list .panel').each(function(){
				order.push($(this).data('id'));
			});
			$('input[name=order]').val(order.join());
		});
	});
	</script>
@stop

@section('contents')
<div class="row">
	<div class="col-md-12">
		<ul class="nav nav-tabs">
		  	<li role="presentation"><a href="{{ action('CourseController@edit', array($courseId)) }}">General Info</a></li>
		  	<li role="presentation" {{ io(!isset($is_exam), 'class="active"') }}><a href="{{ action('SyllabusController@index', array($courseId)) }}">Lessons <span class="badge">{{ @$tab_count['syllabus'] }}</span></a></li>
		  	<li role="presentation" {{ io((isset($is_exam) && $is_exam), 'class="active"') }}><a href="{{ action('SyllabusController@exam', array($courseId)) }}">Exams <span class="badge">{{ @$tab_count['exam'] }}</span></a></li>
		</ul>
	</div>
</div>
<div class="row">
	<div class="col-md-6 col-md-push-6 col-lg-5 col-lg-push-7">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="panel-title">
					<span class="fa fa-file-text"></span>
					Add New {{ io(isset($is_exam), 'Exam', 'Lesson') }}
				</h4>
			</div>
			<div class="panel-body">
				<div class="row">
				{{ Form::open(array('action' => array('SyllabusController@store', $courseId), 'files' => true)) }}
					<div class="col-md-6">
						<div class="form-group {{ hasError($errors, 'name', !Session::has('syllabusId')) }}">
							{{ Form::text('name', null, array('class' => 'form-control', 'placeholder' => 'Lesson Topic, Required')) }}
							{{ errorBlock($errors, 'name', !Session::has('syllabusId')) }}
						</div>
						<div class="form-group">
							{{ Form::textarea('description', null, array('class' => 'form-control', 'rows' => 4, 'placeholder' => 'Lesson Description/Objective, Optional')) }}
						</div>
						@if(isset($is_exam) && $is_exam)
							{{ Form::hidden('is_exam', '1') }}
						@endif
					</div>
					<div class="col-md-6">
						<div class="form-group {{ hasError($errors, 'attachment_url', !Session::has('syllabusId')) }} has-feedback">
							{{ Form::text('attachment_url[]', '', array('class' => 'form-control',  'placeholder' => 'Reference URL')) }}
							<span class="glyphicon glyphicon-link form-control-feedback" aria-hidden="true"></span>
						</div>
						<div class="form-group {{ hasError($errors, 'attachment_url', !Session::has('syllabusId')) }} has-feedback">
							{{ Form::text('attachment_url[]', '', array('class' => 'form-control',  'placeholder' => 'Reference URL')) }}
							<span class="glyphicon glyphicon-link form-control-feedback" aria-hidden="true"></span>
						</div>
						<div class="form-group {{ hasError($errors, 'attachment_url', !Session::has('syllabusId')) }} has-feedback">
							{{ errorBlock($errors, 'attachment_url', !Session::has('syllabusId')) }}
						</div>
						<div class="form-group {{ hasError($errors, 'attachment', !Session::has('syllabusId')) }}">
							{{ Form::file('attachments[]', array('class' => 'form-control')) }}
							{{ Form::file('attachments[]', array('class' => 'form-control')) }}
							{{ Form::file('attachments[]', array('class' => 'form-control')) }}
							{{ Form::file('attachments[]', array('class' => 'form-control')) }}
							{{ errorBlock($errors, 'attachments', Session::has('syllabusId')) }}
							<span class="help-block">File size maximum 20MB.</span>
						</div>
					</div>
				</div>
			</div>
			<div class="panel-footer">
				<input type="submit" value="Save New {{ io(isset($is_exam), 'Exam', 'Lesson') }}" class="btn btn-primary btn-block">
			</div>
			{{ Form::close() }}
		</div>
	</div>
	<div class="col-md-6 col-md-pull-6 col-lg-7 col-lg-pull-5">
		<div class="panel-group" id="syllabus-list">
		<?php //$i = 1; ?>
		@forelse($syllabuses as $syllabus)
			<div data-id="{{ $syllabus->id }}" class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">
						<span class="handle"><i class="fa fa-navicon"></i></span>
						<span href="#syllabus-{{ $syllabus->id }}" data-toggle="collapse">{{ $syllabus->name }}</span>
					</h4>
					{{ Form::open(array('action' => array('SyllabusController@destroy', $courseId, $syllabus->id), 'method' => 'delete', 'class' => 'syllabus-delete pull-right')) }}
						<input type="submit" class="btn btn-danger btn-xs" value="Delete">
					{{ Form::close() }}
				</div>
				<div id="syllabus-{{ $syllabus->id }}" class="panel-collapse collapse {{ (Session::has('syllabusId') && (Session::get('syllabusId') == $syllabus->id)) ? 'in' : '' }}">
					<div class="panel-body">
						<div class="row">
						{{ Form::model($syllabus, array('action' => array('SyllabusController@update', $courseId, $syllabus->id) , 'method' => 'put', 'files' => true) ) }}
							<div class="col-md-6">
								<div class="form-group {{ hasError($errors, 'name.'.$syllabus->id, (Session::has('syllabusId') && (Session::get('syllabusId') == $syllabus->id))) }}">
									{{ Form::text('name['.$syllabus->id.']', $syllabus->name, array('class' => 'form-control', 'placeholder' => 'Syllabus Topic, Required')) }}
									{{ errorBlock($errors, 'name.'.$syllabus->id, (Session::has('syllabusId') && (Session::get('syllabusId') == $syllabus->id))) }}
								</div>
								<div class="form-group">
									{{ Form::textarea('description['.$syllabus->id.']', $syllabus->description, array('class' => 'form-control', 'rows' => 4, 'placeholder' => 'Syllabus Description, Optional')) }}
								</div>
								<div class="form-group">
									<input type="submit" value="Update Syllabus" class="btn btn-primary btn-block">
								</div>
							</div>
							<div class="col-md-6">
								@if($syllabus->attachurl()->count() > 0)
								<div id="attachment-list">
								@foreach($syllabus->attachurl as $attachurl)
									<a href="{{ $attachurl->url }}" target="_blank">{{ $attachurl->url }}</a>
									[<a href="{{ action('AttachmentController@destroyUrl', array($attachurl->id)) }}">Delete</a>]<br>
								@endforeach
								</div>
								@endif
								<div class="form-group {{ hasError($errors, 'attachment_url', (Session::has('syllabusId') && (Session::get('syllabusId') == $syllabus->id))) }}">
									{{ Form::text('attachment_url[]', null, array('class' => 'form-control',  'placeholder' => 'Reference URL')) }}
									{{ Form::text('attachment_url[]', null, array('class' => 'form-control',  'placeholder' => 'Reference URL')) }}
									{{ Form::text('attachment_url[]', null, array('class' => 'form-control',  'placeholder' => 'Reference URL')) }}
									{{ errorBlock($errors, 'attachment_url', (Session::has('syllabusId') && (Session::get('syllabusId') == $syllabus->id))) }}
								</div>
								@if($syllabus->attachment()->count() > 0)
								<div id="attachment-list">
								@foreach($syllabus->attachment as $attachment)
									<a href="{{ action('AttachmentController@show', array($attachment->id)) }}">{{ $attachment->name }}</a>
									({{ round($attachment->size/(1024*1024), 2) }} MB)
									[<a href="{{ action('AttachmentController@destroy', array($attachment->id)) }}">Delete</a>]<br>
								@endforeach
								</div>
								@endif
								<div class="form-group {{ hasError($errors, 'attachment', (Session::has('syllabusId') && (Session::get('syllabusId') == $syllabus->id))) }}">
									{{ Form::file('attachments[]', array('class' => 'form-control')) }}
									{{ Form::file('attachments[]', array('class' => 'form-control')) }}
									{{ Form::file('attachments[]', array('class' => 'form-control')) }}
									{{ errorBlock($errors, 'attachments', (Session::has('syllabusId') && (Session::get('syllabusId') == $syllabus->id))) }}
									<span class="help-block">File Size Maximum 20MB.</span>
								</div>
							</div>
							@if(isset($syllabus->is_exam) && $syllabus->is_exam)
								{{ Form::hidden('is_exam', '1') }}
							@endif
						{{ Form::close() }}
						</div>
					</div>
				</div>
			</div>
		@empty
			<div class="panel panel-default">
				<div class="panel-body">No {{ io(isset($is_exam), 'exam', 'lesson') }} in this course yet.</div>
			</div>
		@endforelse
		@if(count($syllabuses) > 0)
		{{ Form::open(array('action' => array('SyllabusController@reorder', $courseId), 'id' => 'syllabus-reorder')) }}
			{{ Form::hidden('order') }}
			@if(isset($is_exam) && $is_exam)
				{{ Form::hidden('is_exam', '1') }}
			@endif
			<input type="submit" value="Reorder {{ io(isset($is_exam), 'Exam', 'Lesson') }}" class="btn btn-primary btn-block">
		{{ Form::close() }}
		@endif
		</div>
	</div>
</div>
@stop