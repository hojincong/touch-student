@extends('layout')

@section('breadcrumb')
	<li><a href="{{ action('ScheduleController@index') }}">Class Management</a></li>
@stop

@section('page_title')
	<h1>Class Management</h1>
	<h2>Manage teachers, students and materials for classes.</h2>
@stop

@section('page_action')
@if($cu->role <= 1)
	<a href="{{ action('ScheduleController@create') }}" class="btn btn-primary"><span class="fa fa-plus"></span> Add Class</a>
@endif
@stop

@section('header_css')
	<link rel="stylesheet" href="{{ asset('css/schedule.css') }}">
@stop

@section('contents')
	<div class="row">
		<div class="col-md-12">
			<div class="table-responsive">
				<table id="schedule-list" class="table table-hover table-striped">
					<thead>
						<tr>
							<th>Id</th>
							<th>Course Title</th>
							<th>Tutors</th>
							<th>Students</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
						@forelse($schedules as $schedule)
							<tr>
								<td>{{ $schedule->id }}</td>
								<td>
									@if(isset($schedule->course))
									<a href="{{ route('courses.show', array($schedule->course->id)) }}">{{ $schedule->course->name }}</a>
									@else
										Course Deleted
									@endif
								</td>
								<td>
									@foreach($schedule->tutors as $tutor)
										<a href="{{ action('UserController@edit', array($tutor->id)) }}">{{ $tutor->name }}</a><br>
									@endforeach
								</td>
								<td><a href="{{ action('ScheduleController@editStudents', array($schedule->id)) }}">{{ $schedule->students()->count() }}</a></td>
								<td class="td-delete">
								@if($cu->role <= 1)
									<a class="btn btn-warning" href="{{ action('ScheduleController@edit', array($schedule->id)) }}"><i class="fa fa-pencil"></i></a>
									{{ Form::open(array('action' => array('ScheduleController@destroy', $schedule->id), 'method' => 'delete')) }}
										<button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i></button>
									{{ Form::close() }}
								@elseif($cu->role == 2)
									<a class="btn btn-primary" href="{{ action('ScheduleController@editStudents', array($schedule->id)) }}"><i class="fa fa-eye"></i> View Students </a>
								@endif
								</td>
							</tr>
						@empty
							<tr>
								<td colspan="5">No schedule found.</td>
							</tr>
						@endforelse
					</tbody>
					<tfoot>
						<tr>
							<td>Id</td>
							<td>Course Title</td>
							<td>Tutors</td>
							<td>Students</td>
							<td>Actions</td>
						</tr>
					</tfoot>
				</table>
			</div>
		</div>
	</div>
@stop