@extends('layout')

@section('breadcrumb')
	<li><a href="{{ action('ScheduleController@index') }}">Class Management</a></li>
	<li class="active">
	@if(isset($schedule))
	Edit Class
	@else
	Add Class
	@endif
	</li>
@stop

@section('page_title')
	<h1>Edit Class Students</h1>
@stop

@section('page_action')
@if(isset($schedule) && ($cu->role <= 1))
{{ Form::open(array('action' => array('ScheduleController@destroy', $schedule->id), 'method' => 'delete')) }}
	<button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i> Delete</button>
{{ Form::close() }}
@endif
@stop

@section('contents')
@if(isset($schedule) && ($cu->role <= 1))
<div class="row">
	<div class="col-md-12">
		<ul class="nav nav-tabs">
		  	<li role="presentation">
		  		<a href="{{ route('schedules.edit', array($schedule->id)) }}">General Info</a>
		  	</li>
		  	<li class="active" role="presentation">
		  		<a href="{{ action('ScheduleController@editStudents', array($schedule->id)) }}">Students <span class="badge">{{ $tabs ['students'] or 0 }}</span></a>
		  	</li>
		</ul>
	</div>
</div>
@endif
<div class="row">
	<div class="col-md-12">
		<div class="table-responsive">
			<table id="user-list" class="table table-hover table-striped">
				<thead>
					<tr>
						<th>Ref. Id</th>
						<th>Avatar</th>
						<th>Name</th>
						<th>Age</th>
						<th>Phone</th>
						<th>Progress</th>
						<th>Remarks</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					@forelse($users as $user)
						<tr>
							<td>{{ $user->ref_id }}</td>
							<td class="td-thumbnail">
							@if(isset($user->avatar) && $user->avatar != '')
							<img src="{{ asset('img/avatar/'.$user->avatar) }}" class="img-thumbnail">
							@endif
							</td>
							<td>{{ $user->name }}</td>
							<td>{{ $user->age }}</td>
							<td>{{ $user->phone }}</td>
							<td>
								<?php 
								$id = $user->id;
								$appointments = $user->appointments->groupBy('schedule_id');
								$miss = $user->appointments->filter(function($a) use ($id){
									return $a->attendance->filter(function($attend) use ($id){
										return (($attend->student_id == $id) && ($attend->attend == 0));
									})->count();
								})->groupBy('schedule_id');
								?>
								@forelse($user->schedules as $s)
									@if($s->id == $schedule->id)
									<?php $a = ((isset($appointments[$s->id]) ? count($appointments[$s->id]) : 0)+$s->pivot->used_session) - min(isset($miss[$s->id]) ? count($miss[$s->id]) : 0, $s->pivot->miss_count); ?>
									 {{ $a }}/{{ $s->pivot->total_session }}<br>
									@endif
								@empty
									0
								@endforelse
							</td>
							<td class="td-remarks">{{ $user->remarks }}</td>
							<td class="td-delete">
								<a href="{{ route('users.show', array($user->id)) }}" class="btn btn-primary"><i class="fa fa-eye"></i> View</a>
								@if($cu->role <= 1)
								<a class="btn btn-warning" href="{{ action('UserController@edit', array($user->id)) }}"><i class="fa fa-pencil"></i> Edit</a>
								@endif
							</td>
						</tr>
					@empty
						<tr>
							<td colspan="9">No user found.</td>
						</tr>
					@endforelse
				</tbody>
				<tfoot>
					<tr>
						<td>Ref. Id</td>
						<td>Avatar</td>
						<td>Name</td>
						<td>Age</td>
						<td>Phone</td>
						<td>Schedule</td>
						<td>Remarks</td>
						<td>Actions</td>
					</tr>
				</tfoot>
			</table>
		</div>
		{{ $users->links() }}
	</div>
</div>
@stop