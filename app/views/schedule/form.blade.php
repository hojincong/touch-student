@extends('layout')

@section('breadcrumb')
	<li><a href="{{ action('ScheduleController@index') }}">Class Management</a></li>
	<li class="active">
	@if(isset($schedule))
	Edit Class
	@else
	Add Class
	@endif
	</li>
@stop

@section('page_title')
	@if(isset($schedule))
	<h1>Edit Class</h1>
	@else
	<h1>Add Class</h1>
	@endif
@stop

@section('page_action')
@if(isset($schedule))
{{ Form::open(array('action' => array('ScheduleController@destroy', $schedule->id), 'method' => 'delete')) }}
	<button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i> Delete</button>
{{ Form::close() }}
@endif
@stop

@section('footer_js')
<script type="text/javascript">
$().ready(function(){

	var input_schedule_time = '<div>\
					{{ Form::select('day[]', $days, null, array('class' => 'form-control')) }}\
					{{ Form::text('shour[]', null, array('class' => 'form-control')) }}\
					{{ Form::text('smin[]', null, array('class' => 'form-control')) }}\
					<span>Until</span>\
					{{ Form::text('ehour[]', null, array('class' => 'form-control')) }}\
					{{ Form::text('emin[]', null, array('class' => 'form-control')) }}\
					<a class="btn btn-warning remove"><span class="glyphicon glyphicon-minus"></span></a>\
				</div>';

	$('#input-schedule-add').click(function(){
		$('#input-schedule-times').append(input_schedule_time);
	});

	$('#input-schedule').on('click', '.remove', function(){
		$(this).parent().remove();
	});
});
</script>
@stop

@section('contents')
@if(isset($schedule))
<div class="row">
	<div class="col-md-12">
		<ul class="nav nav-tabs">
		  	<li class="active" role="presentation">
		  		<a href="{{ route('schedules.edit', array($schedule->id)) }}">General Info</a>
		  	</li>
		  	<li role="presentation">
		  		<a href="{{ action('ScheduleController@editStudents', array($schedule->id)) }}">Students <span class="badge">{{ $tabs['students'] or 0 }}</span></a>
		  	</li>
		</ul>
	</div>
</div>
@endif
<div class="row">
	<div class="col-md-6">
		@if(count($course) == 0)
			<div class="alert alert-warning" role="alert">
				You haven't created any course. Please create a new course <a href="{{ action('CourseController@create') }}">here</a>.
			</div>
		@else
		@if(isset($schedule))
			{{ Form::model($schedule, array(
				'action' => array('ScheduleController@update',$schedule->id),
				'method' => 'put'
			)) }}
		@else
			{{ Form::open(array('action' => 'ScheduleController@store')) }}
		@endif
			<div class="form-group {{ hasError($errors, 'course') }}">
				<label for="name">*Course (Changing course may cause problems for existing student)</label>
				@if(!isset($schedule->course))
				(Original course deleted.)
				@endif
				{{ Form::select('course', $course, isset($schedule->course) ? $schedule->course->id : null, array('class' => 'form-control')) }}
				{{ errorBlock($errors, 'course') }}
			</div>
			<label for="tutor">*Teacher</label>
			<?php $i = 0; ?>
			@if(isset($schedule))
			<?php $schedule->tutors->each(function($tutor) use ($tutors, $errors, &$i){ ?>
			<div class="form-group {{ hasError($errors, 'tutors.'.$i) }}">
				{{ Form::select('tutors['.$i.']', $tutors, $tutor->id, array('class' => 'form-control')) }}
				{{ errorBlock($errors, 'tutors.'.$i) }}
			</div>
			<?php $i++; }); ?>
			@endif
			@for($j = $i; $j < $i+3; $j++)
			<div class="form-group {{ hasError($errors, 'tutors.'.$j) }}">
				{{ Form::select('tutors['.$j.']', $tutors, null, array('class' => 'form-control')) }}
				{{ errorBlock($errors, 'tutors.'.$j) }}
			</div>
			@endfor
			<!--<div id="input-schedule" class="form-group">
				<label for="schedule">Time Schedule(24 hour format)</label>
				@if(isset($schedule))
					{{--@foreach($schedule->times()->orderBy('day')->get() as $time)
						<div>
							{{ Form::select('day[]', $days, $time->day, array('class' => 'form-control')) }}
							{{ Form::text('shour[]', $time->shour, array('class' => 'form-control')) }}
							{{ Form::text('smin[]', $time->smin, array('class' => 'form-control')) }}
							<span>Until</span>
							{{ Form::text('ehour[]', $time->ehour, array('class' => 'form-control')) }}
							{{ Form::text('emin[]', $time->emin, array('class' => 'form-control')) }}
							<a class="btn btn-warning remove"><span class="glyphicon glyphicon-minus"></span></a>
						</div>
					@endforeach--}}
				@endif
				<div id="input-schedule-times"></div>
				<div>
					<a class="btn btn-warning" id="input-schedule-add"><span class="glyphicon glyphicon-plus"></span></a>
				</div>
			</div>-->
			<div class="form-group">
				@if(isset($schedule))
				<input type="submit" value="Update Class Info" class="btn btn-primary btn-block">
				@else
				<input type="submit" value="Create New Class" class="btn btn-primary btn-block">
				@endif
			</div>
		{{ Form::close() }}
	</div>
</div>
@endif
@stop