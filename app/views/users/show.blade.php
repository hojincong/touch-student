@extends('layout')

@section('breadcrumb')
	<li><a href="{{ action('UserController@index') }}">User Management</a></li>
	<li class="active">View User</li>
@stop

@section('footer_js')
	<script type="text/javascript" src="{{ asset('js/users.js') }}"></script>
	@if($cu->role == 3)
	<script type="text/javascript">
		@if($tabs == 'schedules')
		var nextTour = '{{ action('UserController@showAppointments', array($id)) }}?tour=1';
		@elseif($tabs == 'appointments')
		var nextTour = '{{ action('UserController@showAssessments', array($id)) }}?tour=1';
		@elseif($tabs == 'assessments')
		var nextTour = '{{ action('CourseController@index') }}?tour=1';
		@endif
		var tab = '{{ $tabs }}';
	</script>
	<script type="text/javascript" src="{{ asset('js/tours/users.js') }}"></script>
	@endif
@stop

@section('header_css')
	<link rel="stylesheet" href="{{ asset('css/appointments.css') }}">
@stop

@section('header')
	<div id="user-header" class="jumbotron">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-6">
					<h1>{{ $user->name }}</h1>
				</div>
				<div id="main-header-actions" class="col-md-6">
					@if($cu->role <= 1)
					<a href="{{ route('users.edit', array($user->id)) }}" class="btn btn-warning"><i class="fa fa-pencil"></i> Edit</a>
					{{ Form::open(array('action' => array('UserController@destroy', $user->id), 'method' => 'delete', 'class' => 'form-ib')) }}
						<button class="btn btn-danger"><i class="fa fa-trash"></i> Delete</button>
					{{ Form::close() }}
					@endif
				</div>
			</div>
			<div class="row">
				<div class="col-md-2">
					@if(isset($user->avatar) && $user->avatar != '')
						<img src="{{ asset('img/avatar/'.$user->avatar) }}" class="user-tn img-thumbnail">
					@else
						<img src="{{ asset('img/gravatar.png') }}" class="user-tn img-thumbnail">
					@endif
				</div>
				<div class="col-md-3">
					<p>Ref Id: {{ $user->ref_id or '-' }}</p>
					<p>Age: {{ $user->age or '-' }}</p>
					@if($is_current_user || ($cu->role <= 1))
					<p>IC/Passport: {{ $user->ic or '-' }}</p>
					<p>Email: {{ $user->email or '-' }}</p>
					<p>No: {{ $user->phone or '-' }}</p>
					@endif
				</div>
				<div class="col-md-3">
					<p>Start Date: {{ ($user->start_date == '0000-00-00' || empty($user->start_date)) ? '-' : $user->start_date }}</p>
					<p>Address: {{ $user->address or '-' }}</p>
					<p>Remarks: {{ $user->remarks or '-' }}</p>
				</div>
			</div>
		</div>
	</div>
	<div id="main-header-tabs">
		<div class="container-fluid">
			<ul class="nav nav-tabs">
				<li @if($tabs == "schedules") class="active" @endif>
					<a href="{{ action('UserController@show', array($id)) }}">Classes <span class="badge"></span></a>
				</li>
				@if($user->role == '3')
				<li @if($tabs == "appointments") class="active" @endif>
					<a href="{{ action('UserController@showAppointments', array($id)) }}">Appointments <span class="badge"></span></a>
				</li>
				<li @if($tabs == "assessments") class="active" @endif>
					<a href="{{ action('UserController@showAssessments', array($id)) }}">Assessments <span class="badge"></span></a>
				</li>
				@endif
			</ul>
		</div>
	</div>
@stop

@section('contents')
@if($tabs == "schedules")
<div class="row">
	<div class="col-md-6">
		<div class="panel-group">
			@forelse($schedules as $s)
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">{{ $s->name }}</h4>
					@if(($user->role == 3) && (($cu->id == $user->id) || ($cu->role <= 2)))
					<a target="_blank" href="{{ action('ReportController@getReport', array($user->id, $s->id)) }}" class="btn btn-xs btn-primary pull-right download-report">Download Report</a>
					@endif
				</div>
				@if($user->role == '3')
				<div class="panel-body">
					<p><b>Start Date:</b> {{ $s->pivot->start_date }}</p>
					<p><b>Expiry Date:</b> {{ $s->pivot->expiry_date }}</p>
					<?php $ec = (isset($exams[$s->id]) ? count($exams[$s->id]) : 0)+$s->pivot->used_exam ?>
					<p><b>Exam:</b> {{ $ec }}/{{ $s->pivot->exam }}@if($s->pivot->exam > 0)
						(@for($i=1;$i<=$s->pivot->exam;$i++)
							@if($s->pivot->exam==$i)
								{{ ($s->pivot->total_session - 1) }}
							@else
								{{ floor($s->pivot->total_session/$s->pivot->exam)*$i }},
							@endif
						@endfor)
					@endif</p>
					<?php $mc = isset($miss[$s->id]) ? count($miss[$s->id]) : 0 ?>
					<p><b>Absent:</b> {{ $mc }}/{{ $s->pivot->miss_count }}</p>
					<?php $a = ((isset($appointments[$s->id]) ? count($appointments[$s->id]) : 0)+$s->pivot->used_session) - min(isset($miss[$s->id]) ? count($miss[$s->id]) : 0, $s->pivot->miss_count) ?>
					<p><b>Progress:</b> {{ $a."/".$s->pivot->total_session }}</p>
				</div>
				@endif
			</div>
			@empty
				<p>This user is not added to any schedule.</p>
			@endforelse
		</div>
	</div>
</div>
@elseif($tabs == "appointments")
<div class="row">
	<div class="col-md-12">
		<div id="user-ap-helper">
			@if(isset($schedule) && (count($schedule) > 0))
			<form action="{{ action('UserController@showAppointments', array($id)) }}" class="form-inline">
				<div class="form-group">
					<label for="filter">Class Filter: </label>
					{{ Form::select('filter', $schedule, $sid, array('class' => 'form-control')) }}
					<button type="submit" class="btn btn-primary">Go</button>
				</div>
			</form>
			@else
				Student is not enrolled in any class.
			@endif
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="table-responsive">
			<table class="table table-striped table-bordered">
				<thead>
					<tr>
						<th>No</th>
						<th>Date</th>
						<th>Time</th>
						<th>Lesson Plan</th>
						<th>Attendance</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<?php $missed = 0; ?>
					@forelse($appointments as $a)
					<?php 
						$attend = $a->attendance()->where('student_id', $id)->first(); 
						$is_missed = (isset($attend->attend) && !$attend->attend);
						if($is_missed) $missed++;
					?>
						<tr>
							<td>
								@if($attend == null)
									{{ ++$index }}
								@elseif($is_missed && ($missed <= $max_miss))
									-
								@else
									{{ ++$index }}
								@endif
							</td>
							<td>{{ date('Y-m-d', strtotime($a->start_time)) }}</td>
							<td>{{ date('H:i', strtotime($a->start_time)).' - '.date('H:i', strtotime($a->end_time)) }}</td>
							<td>
								@if(isset($a->pivot->syllabus_id))
									<?php $s = Syllabus::withTrashed()->find($a->pivot->syllabus_id) ?>
									{{ @$s->name }} 
									{{ @$s->is_exam ? '(Exam)' : '' }}
									{{ @$s->deleted_at ? '(Deleted)' : '' }}
								@else
									-
								@endif
							</td>
							<td>
								@if($attend == null)
								-
								@else
								<i class="fa fa-{{ $is_missed ? 'close' : 'check' }}"></i>
								@endif
							</td>
							<td>
								<a href="{{ route('appointment.show', array($a->id)) }}" class="btn btn-primary"><i class="fa fa-eye"></i> View</a>
								@if($cu->role <= 1)
								<a href="{{ action('AppointmentController@edit', array($a->id)) }}" class="btn btn-warning"><i class="fa fa-pencil"></i> Edit</a>
								@endif
							</td>
						</tr>
					@empty
						<tr>
							<td colspan="6">No appointment in this class was found.</td>
						</tr>
					@endforelse
				</tbody>
				<tfoot>
					<tr>
						<td>No</td>
						<td>Date</td>
						<td>Time</td>
						<td>Lesson Plan</td>
						<td>Attendance</td>
						<td>Action</td>
					</tr>
				</tfoot>
			</table>
			@if(count($appointments))
			{{-- $appointments->links() --}}
			@endif
		</div>
	</div>
</div>
@elseif($tabs == 'assessments')
@if(isset($schedule) && (count($schedule) > 0))
<div class="row">
	<div class="col-md-12">
		<ul class="nav nav-tabs">
			@foreach($versions as $i=>$v)
			<li @if($ver == $v) class="active" @endif>
				<a href="{{ action('UserController@showAssessments', array($id, $sid)) }}?ver={{ $v }}">
					@if($qmeta[$v] == 'default')
						@if(isset($versions[$i+1]))
							{{ $qmeta[$versions[$i+1]].' to '.date('Y-m-d') }}
						@else
							{{ '2015-01-01 to '.date('Y-m-d') }}
						@endif
					@else
						@if(isset($versions[$i+1]))
							{{ $qmeta[$versions[$i+1]].' to '.$qmeta[$versions[$i]] }}
						@else
							{{ '2015-01-01 to '.$qmeta[$versions[$i]] }}
						@endif
					@endif
				</a>
			</li>
			@endforeach

			@if(count($schedule) <= 1)
			@foreach($schedule as $sid=>$name)
			<li class="pull-right">
				<a>{{ $name }}</a>
			</li>
			@endforeach
			@else
			<li class="active dropdown pull-right">
				<a href="" class="dropdown-toggle" data-toggle="dropdown" data-target="schedules-list">{{ $schedule[$sid] }} <span class="caret"></span></a>
				<ul class="dropdown-menu" id="schedules-list">
					@foreach($schedule as $sid=>$name)
					<li><a href="{{ action('UserController@showAssessments', array($id, $sid)) }}">{{ $name }}</a></li>
					@endforeach
				</ul>
			</li>
			@endif
		</ul>
	</div>
</div>
@if($remarks->getTotal() > 0)
<div class="row">
	<div class="col-md-12">
		@foreach($questions as $section=>$sq)
		<div class="table-responsive">
			<table class="appointment-remarks table table-striped table-bordered table-hover">
				<thead>
					<tr>
						<th>{{ ucfirst($section) }} Assessment</th>
						@if(isset($remarks))
						@foreach($remarks as $r)
							<th><a href="{{ route('appointment.show', array($r['appointment_id'])) }}">{{ $r['order']}}</a></th>
						@endforeach
						@endif
					</tr>
				</thead>
				<tbody>
					@foreach($sq as $q)
						<tr>
							<td>
								{{ $q['title'] }}
								@if(isset($q['question']))
								<a class="btn btn-default pull-right assessment-question" data-toggle="popover" data-content="{{ $q['question'] }}"><span class="fa fa-info"></span></a>
								@endif
							</td>
							@if(isset($remarks))
							@foreach($remarks as $r)
								<td><p>{{ $r[$q['model']] or '-' }}</p></td>
							@endforeach
							@endif
						</tr>
					@endforeach
				</tbody>
				<tfoot>
					<tr>
						<th>{{ ucfirst($section) }} Assessment</th>
						@if(isset($remarks))
						@foreach($remarks as $r)
							<td><a href="{{ route('appointment.show', array($r['appointment_id'])) }}">{{ $r['order']}}</a></td>
						@endforeach
						@endif
					</tr>
				</tfoot>
			</table>
		</div>
		@endforeach
		{{--<div class="table-responsive">
			<table class="appointment-remarks table table-striped table-bordered table-hover">
				<thead>
					<tr>
						<th>Speaking Assessment</th>
						@if(isset($remarks))
						@foreach($remarks as $r)
							<th><a href="{{ route('appointment.show', array($r['appointment_id'])) }}">{{ $r['order']}}</a></th>
						@endforeach
						@endif
					</tr>
				</thead>
				<tbody>
					@foreach($questions['speaking'] as $q)
						<tr>
							<td>{{ $q['question'] }}</td>
							@if(isset($remarks))
							@foreach($remarks as $r)
								<td><p>{{ $r[$q['model']] or '-' }}</p></td>
							@endforeach
							@endif
						</tr>
					@endforeach
				</tbody>
				<tfoot>
					<tr>
						<td>Speaking Assessment</td>
						@if(isset($remarks))
						@foreach($remarks as $r)
							<td><a href="{{ route('appointment.show', array($r['appointment_id'])) }}">{{ $r['order']}}</a></td>
						@endforeach
						@endif
					</tr>
				</tfoot>
			</table>
		</div>
		<div class="table-responsive">
			<table class="appointment-remarks table table-striped table-bordered table-hover">
				<thead>
					<tr>
						<th>Grammar Assessment</th>
						@if(isset($remarks))
						@foreach($remarks as $r)
							<th><a href="{{ route('appointment.show', array($r['appointment_id'])) }}">{{ $r['order']}}</a></th>
						@endforeach
						@endif
					</tr>
				</thead>
				<tbody>
					@foreach($questions['grammar'] as $q)
						<tr>
							<td>
								{{ $q['question'] }}
							</td>
							@if(isset($remarks))
							@foreach($remarks as $r)
								<td><p>{{ $r[$q['model']] or '-' }}</p></td>
							@endforeach
							@endif
						</tr>
					@endforeach
				</tbody>
				<tfoot>
					<tr>
						<td>Grammar Assessment</td>
						@if(isset($remarks))
						@foreach($remarks as $r)
							<td><a href="{{ route('appointment.show', array($r['appointment_id'])) }}">{{ $r['order']}}</a></td>
						@endforeach
						@endif
					</tr>
				</tfoot>
			</table>
		</div>--}}
	</div>
</div>
@else
<div class="row">
	<div class="col-md-12">
		<div id="user-ap-helper">
			You don't have any apppointments between these dates yet.
		</div>
	</div>
</div>	
@endif
@else
<div class="row">
	<div class="col-md-12">
		<div id="user-ap-helper">
			You haven't enrolled in any class.
		</div>
	</div>
</div>
@endif
@if(isset($remarks))
{{ $remarks->appends(array('ver' => $ver))->links() }}
@endif
@endif
@stop