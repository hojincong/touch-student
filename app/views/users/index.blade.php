@extends('layout')

@section('header_css')
	<link rel="stylesheet" href="{{ asset('css/user.css') }}">
@stop

@section('breadcrumb')
	@if(isset($search) && $search == true)
	<li><a href="{{ action('UserController@index') }}">User Management</a></li>
	<li>Search</li>
	@else
	<li class="active"><a href="{{ action('UserController@index') }}">User Management</a></li>
	@endif
@stop

@section('page_title')
	<h1>User Management</h1>
@stop

@section('page_action')
	<div id="user-action" class="row">
		<div class="col-sm-6">
			<div class="btn-group btn-group-justified">
				<a href="{{ action('UserController@create') }}" class="btn btn-primary"><span class="fa fa-user-plus"></span> Add User</a>
				<div class="btn-group">
				  	<button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
					@if($role == '3' && $inactive == false)
						Students
					@elseif($role == '3' && $inactive == true)
						Inactive
					@elseif($role == '2')
						Teachers
					@else
						Admins
					@endif
					<span class="caret"></span>
				  	</button>
				  	<ul class="dropdown-menu dropdown-menu-right">
				    	<li @if(($role == '3') && ($inactive == false)) class="active" @endif>
				    		<a href="?role=3">Students <span class="badge">{{ $user_count['student'] }}</span></a>
				    	</li>
				    	<li @if(($role == '3') && ($inactive == true)) class="active" @endif>
				    		<a href="?role=3&inactive=1">Inactive <span class="badge">{{ $user_count['inactive'] }}</span></a>
				    	</li>
				    	<li @if($role == '2') class="active" @endif>
				    		<a href="?role=2">Teachers <span class="badge">{{ $user_count['teacher'] }}</span></a>
				    	</li>
				    	<li @if($role == '1') class="active" @endif>
				    		<a href="?role=1">Admins <span class="badge">{{ $user_count['admin'] }}</span></a>
				    	</li>
				  	</ul>
				</div>
			</div>
		</div>
		<div class="col-sm-6">
			{{ Form::open(array('action' => array('UserController@search'), 'method' => 'get')) }}
				<div class="form-group">
					<div class="input-group">
						@if(isset($search) && $search)
						<span class="input-group-btn">
							<a href="{{ action('UserController@index') }}" class="btn btn-danger">
								<span class="fa fa-close"></span>
							</a>
						</span>
						@endif
						<input type="search" name="q" value="{{ Input::get('q') }}" placeholder="Searching Any User?" class="form-control">
						<span class="input-group-btn">
							<button type="submit" class="btn btn-primary">
								<span class="fa fa-search"></span>
							</button>
						</span>
					</div>
				</div>
			{{ Form::close() }}
		</div>
	</div>

@stop

@section('contents')
<div class="row">
	<div class="col-md-12">
		<div class="table-responsive">
			<table id="user-list" class="table table-hover table-striped">
				<thead>
					<tr>
						<th>Ref. Id</th>
						<th>Avatar</th>
						<th>Name</th>
						<th>Age</th>
						<th>Phone</th>
						<th>Classes</th>
						<th>Remarks</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					@forelse($users as $user)
						<tr>
							<td>{{ $user->ref_id }}</td>
							<td class="td-thumbnail">
							@if(isset($user->avatar) && $user->avatar != '' && App::environment('production'))
							<img src="{{ asset('img/avatar/'.$user->avatar) }}" class="img-thumbnail">
							@endif
							</td>
							<td>{{ $user->name }}</td>
							<td>{{ $user->age }}</td>
							<td>{{ $user->phone }}</td>
							<td><a href="{{ action('UserController@editSchedules', array($user->id)) }}">
								<?php 
								$id = $user->id;
								$appointments = $user->appointments->groupBy('schedule_id');
								$miss = $user->appointments->filter(function($a) use ($id){
									return $a->attendance->filter(function($attend) use ($id){
										return (($attend->student_id == $id) && ($attend->attend == 0));
									})->count();
								})->groupBy('schedule_id');
								?>
								@forelse($user->schedules as $s)
									<?php $a = ((isset($appointments[$s->id]) ? count($appointments[$s->id]) : 0)+$s->pivot->used_session) - min(isset($miss[$s->id]) ? count($miss[$s->id]) : 0, $s->pivot->miss_count); ?>
									{{ $s->name }} [{{ $a }}/{{ $s->pivot->total_session }}({{ min(isset($miss[$s->id]) ? count($miss[$s->id]) : 0, $s->pivot->miss_count) }})]<br>
								@empty
									-
								@endforelse
								{{-- $user->schedules->count() --}}
							</a></td>
							<td class="td-remarks">{{ $user->remarks }}</td>
							<td class="td-delete">
								<a href="{{ route('users.show', array($user->id)) }}" class="btn btn-primary"><i class="fa fa-eye"></i></a>
								<a class="btn btn-warning" href="{{ action('UserController@edit', array($user->id)) }}"><i class="fa fa-pencil"></i></a>
								{{ Form::open(array('action' => array('UserController@destroy', $user->id), 'method' => 'delete')) }}
									<button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i></button>
								{{ Form::close() }}
							</td>
						</tr>
					@empty
						<tr>
							<td colspan="9">No user found.</td>
						</tr>
					@endforelse
				</tbody>
				<tfoot>
					<tr>
						<td>Ref. Id</td>
						<td>Avatar</td>
						<td>Name</td>
						<td>Age</td>
						<td>Phone</td>
						<td>Classes</td>
						<td>Remarks</td>
						<td>Actions</td>
					</tr>
				</tfoot>
			</table>
		</div>
		{{ $paginator }}
	</div>
</div>
@stop