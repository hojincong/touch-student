@extends('layout')

@section('breadcrumb')
	<li><a href="{{ action('UserController@index') }}">User Management</a></li>
	<li class="active">Edit User Class</li>
@stop

@section('page_title')
	<h1>Edit User</h1>
@stop

@section('page_action')
<a href="{{ route('users.show', array($id)) }}" class="btn btn-primary">
	<i class="fa fa-eye"></i> View
</a>
{{ Form::open(array('action' => array('UserController@destroy', $id), 'method' => 'delete', 'class' => 'form-ib')) }}
	<button class="btn btn-danger"><i class="fa fa-trash"></i> Delete</button>
{{ Form::close() }}
@stop

@section('header_css')
	<link rel="stylesheet" href="{{ asset('lib/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}">
@stop

@section('footer_js')
<script src="{{ asset('lib/moment/moment.js') }}" type="text/javascript"></script>
<script type="text/javascript" src="{{ asset('lib/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/users.js') }}"></script>
@stop

@section('contents')
<div class="row">
	<div class="col-md-12">
		<ul class="nav nav-tabs">
		  	<li role="presentation"><a href="{{ route('users.edit', array($id)) }}">General Info</a></li>
		  	<li role="presentation"  class="active"><a href="{{ action('UserController@editSchedules', array($id)) }}">Classes </a></li>
		  	<li role="presentation"><a href="{{ action('UserController@editAppointments', array($id)) }}">Appointments </a></li>
		</ul>
	</div>
</div>
<div class="row">
	<div class="col-md-push-6 col-md-6">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="panel-title">Add to Class</h4>
			</div>
			<div class="panel-body">
				@if(count($schedule_list) > 1)
				{{ Form::open(array('action' => array('UserController@createSchedules', $id))) }}
				<div class="form-group {{ hasError($errors, 'schedule_id') }}">
					<label for="schedule_id">Schedule</label>
					{{ Form::select('schedule_id', $schedule_list, null, array('class' => 'form-control')) }}
					{{ errorBlock($errors, 'schedule_id') }}
				</div>
				<div class="row">
					<div class="col-md-6">
						<div id="input_sstartdate" class="form-group {{ hasError($errors, 'start_date') }}">
							<label for="start_date">Start Date</label><br>
							{{ Form::text('start_date', null, array('class' => 'form-control sstart_date')) }}
							{{ errorBlock($errors, 'start_date') }}
						</div>
					</div>
					<div class="col-md-6">
						<div id="input_expirydate" class="form-group {{ hasError($errors, 'expiry_date') }}">
							<label for="expiry_date">Expiry Date</label><br>
							{{ Form::text('expiry_date', null, array('class' => 'form-control sexpiry_date')) }}
							{{ errorBlock($errors, 'expiry_date') }}
						</div>
					</div>
				</div>
				<div id="input_student_info" class="form-group @if($errors->has('total_session') || $errors->has('used_session') || $errors->has('exam') || $errors->has('used_exam') || $errors->has('miss_count')) has-error @endif">
					<label>Total Session/Used Session/Exam/Used Exam/Absent Count</label><br>
					{{ Form::text('total_session', '', array('class' => 'form-control', 'placeholder' => 'Total')) }}
					{{ Form::text('used_session', 0, array('class' => 'form-control', 'placeholder' => 'Used')) }}
					{{ Form::text('exam', null, array( 'class' => 'form-control', 'placeholder' => 'Exam')) }}
					{{ Form::text('used_exam', 0, array( 'class' => 'form-control', 'placeholder' => 'Used')) }}
					{{ Form::text('miss_count', 5, array('class' => 'form-control')) }}
					{{ errorBlock($errors, 'total_session') }}
					{{ errorBlock($errors, 'used_session') }}
					{{ errorBlock($errors, 'exam') }}
					{{ errorBlock($errors, 'used_exam') }}
					{{ errorBlock($errors, 'miss_count') }}
				</div>
				<input type="submit" value="Add Student to Class" class="btn btn-primary btn-block">
				{{ Form::close() }}
				@else
				<p>No classes available for this student.</p>
				@endif
			</div>
		</div>
	</div>
	<div class="col-md-pull-6 col-md-6">
	@foreach($schedules as $s)
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="panel-title">
					{{ $s->name }}
				</h4>
				{{ Form::open(array('action' => array('UserController@destroySchedules', $id, $s->id), 'method' => 'delete', 'class' => 'user-schedules-delete')) }}
						<input type="submit" class="btn btn-danger btn-xs" value="Delete">
					{{ Form::close() }}
			</div>
			<div class="panel-body">
				{{ Form::open(array('action' => array('UserController@updateSchedules', $id, $s->id))) }}
				<div class="row">
					<div class="col-md-6">
						<div id="input_sstartdate" class="form-group {{ hasError($errors, 'sstart_date.'.$s->id) }}">
							<label for="sstart_date">Start Date</label><br>
							{{ Form::text('start_date['.$s->id.']', $s->pivot->start_date, array('class' => 'form-control sstart_date')) }}
							{{ errorBlock($errors, 'sstart_date.'.$s->id) }}
						</div>
					</div>
					<div class="col-md-6">
						<div id="input_sexpirydate" class="form-group {{ hasError($errors, 'sexpiry_date.'.$s->id) }}">
							<label for="sexpiry_date">Expiry Date</label><br>
							{{ Form::text('expiry_date['.$s->id.']', $s->pivot->expiry_date, array('class' => 'form-control sexpiry_date')) }}
							{{ errorBlock($errors, 'sexpiry_date.'.$s->id) }}
						</div>
					</div>
				</div>
				<div id="input_student_info" class="form-group @if($errors->has('total_session.'.$s->id) || $errors->has('used_session.'.$s->id) || $errors->has('exam.'.$s->id) || $errors->has('used_exam.'.$s->id) || $errors->has('miss_count.'.$s->id)) has-error @endif">
					<label>Total Session/Used Session/Exam/Used Exam/Absent Count</label><br>
					{{ Form::text('total_session['.$s->id.']', $s->pivot->total_session, array('class' => 'form-control', 'placeholder' => 'Total')) }}
					{{ Form::text('used_session['.$s->id.']', $s->pivot->used_session, array('class' => 'form-control', 'placeholder' => 'Used')) }}
					{{ Form::text('exam['.$s->id.']', $s->pivot->exam, array( 'class' => 'form-control', 'placeholder' => 'Exam')) }}
					{{ Form::text('used_exam['.$s->id.']', $s->pivot->used_exam, array( 'class' => 'form-control', 'placeholder' => 'Used')) }}
					{{ Form::text('miss_count['.$s->id.']', $s->pivot->miss_count, array('class' => 'form-control')) }}
					{{ errorBlock($errors, 'total_session.'.$s->id) }}
					{{ errorBlock($errors, 'used_session.'.$s->id) }}
					{{ errorBlock($errors, 'exam.'.$s->id) }}
					{{ errorBlock($errors, 'used_exam.'.$s->id) }}
					{{ errorBlock($errors, 'miss_count.'.$s->id) }}
				</div>
				<input type="submit" value="Update Info" class="btn btn-primary btn-block">
				{{ Form::close() }}
			</div>
		</div>
	@endforeach	
	</div>
</div>
@stop