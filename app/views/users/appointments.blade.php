@extends('layout')

@section('breadcrumb')
	<li><a href="{{ action('UserController@index') }}">User Management</a></li>
	<li class="active">Edit User Schedules</li>
@stop

@section('page_title')
	<h1>Edit User</h1>
@stop

@section('page_action')
@if(isset($user))
<a href="{{ route('users.show', array($id)) }}" class="btn btn-primary">
	<i class="fa fa-eye"></i> View
</a>
{{ Form::open(array('action' => array('UserController@destroy', $user->id), 'method' => 'delete', 'class' => 'form-ib')) }}
	<button class="btn btn-danger"><i class="fa fa-trash"></i> Delete</button>
{{ Form::close() }}
@endif
@stop

@section('header_css')
	<link rel="stylesheet" href="{{ asset('lib/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}">
@stop

@section('footer_js')
<script src="{{ asset('lib/moment/moment.js') }}" type="text/javascript"></script>
<script type="text/javascript" src="{{ asset('lib/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/users.js') }}"></script>
@stop

@section('contents')
<div class="row">
	<div class="col-md-12">
		<ul class="nav nav-tabs">
		  	<li role="presentation"><a href="{{ route('users.edit', array($id)) }}">General Info</a></li>
		  	<li role="presentation"><a href="{{ action('UserController@editSchedules', array($id)) }}">Classes</a></li>
		  	<li role="presentation" class="active"><a href="{{ action('UserController@editAppointments', array($id)) }}">Appointments</a></li>
		</ul>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div id="user-ap-helper">
			@if(isset($schedule) && (count($schedule) > 0))
			<form action="{{ action('UserController@editAppointments', array($id)) }}" class="form-inline">
				<div class="form-group">
					<label for="filter">Class Filter: </label>
					{{ Form::select('filter', $schedule, $sid, array('class' => 'form-control')) }}
					<button type="submit" class="btn btn-primary">Go</button>
				</div>
			</form>
			@else
				Student is not enrolled in any class.
			@endif
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="table-responsive">
			<table class="table table-striped table-bordered">
				<thead>
					<tr>
						<th>Id</th>
						<th>Date</th>
						<th>Time</th>
						<th>Lesson Plan</th>
						<th>Attendance</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<?php $missed = 0; ?>
					@forelse($appointments as $a)
					<?php 
						$attend = $a->attendance()->where('student_id', $id)->first(); 
						$is_missed = !(isset($attend->attend) && $attend->attend);
						if($is_missed) $missed++;
					?>
						<tr>
							<td>
								@if($attend == null)
									{{ ++$index }}
								@elseif($is_missed && ($missed <= $max_miss))
									-
								@else
									{{ ++$index }}
								@endif
							</td>
							<td>{{ date('Y-m-d', strtotime($a->start_time)) }}</td>
							<td>{{ date('H:i', strtotime($a->start_time)).' - '.date('H:i', strtotime($a->end_time)) }}</td>
							<td>
								@if(isset($a->pivot->syllabus_id))
									<?php $s = Syllabus::withTrashed()->find($a->pivot->syllabus_id) ?>
									{{ $s->name }} 
									{{ $s->is_exam ? '(Exam)' : '' }}
									{{ $s->deleted_at ? '(Deleted)' : '' }}
								@else
									-
								@endif
							</td>
							<td>
								@if($attend == null)
								-
								@else
								<i class="fa fa-{{ $is_missed ? 'close' : 'check' }}"></i>
								@endif
							</td>
							<td>
								<a href="{{ route('appointment.show', array($a->id)) }}" class="btn btn-primary"><i class="fa fa-eye"></i> View</a>
								<a href="{{ action('AppointmentController@edit', array($a->id)) }}" class="btn btn-warning"><i class="fa fa-pencil"></i> Edit</a>
								{{ Form::open(array('action' => array('UserController@destroyAppointments', $id, $a->id), 'method' => 'delete', 'class' => 'form-ib')) }}
								<button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i> Remove</button>
								{{ Form::close() }}
							</td>
						</tr>
					@empty
						<tr>
							<td colspan="6">No appointment in this class was found.</td>
						</tr>
					@endforelse
				</tbody>
				<tfoot>
					<tr>
						<td>Id</td>
						<td>Date</td>
						<td>Time</td>
						<td>Lesson Plan</td>
						<td>Attendance</td>
						<td>Action</td>
					</tr>
				</tfoot>
			</table>
			@if(count($appointments))
			{{ $appointments->links() }}
			@endif
		</div>
	</div>
</div>
@stop