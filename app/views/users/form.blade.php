@extends('layout')

@section('breadcrumb')
	<li><a href="{{ action('UserController@index') }}">User Management</a></li>
	<li class="active">
	@if(isset($user))
	Edit User
	@else
	Add User
	@endif
	</li>
@stop

@section('page_title')
	@if(isset($user))
	<h1>Edit User</h1>
	@else
	<h1>Add User</h1>
	@endif
@stop

@section('page_action')
@if(isset($user))
<a href="{{ route('users.show', array($user->id)) }}" class="btn btn-primary">
	<i class="fa fa-eye"></i> View
</a>
@if($cu->role <= 1)
{{ Form::open(array('action' => array('UserController@destroy', $user->id), 'method' => 'delete', 'class' => 'form-ib')) }}
	<button class="btn btn-danger"><i class="fa fa-trash"></i> Delete</button>
{{ Form::close() }}
@endif
@endif
@stop

@section('header_css')
	<link rel="stylesheet" href="{{ asset('lib/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}">
@stop

@section('footer_js')
<script src="{{ asset('lib/moment/moment.js') }}" type="text/javascript"></script>
<script type="text/javascript" src="{{ asset('lib/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/users.js') }}"></script>
@stop

@section('contents')
@if(isset($user) && ($cu->role <= 1))
<div class="row">
	<div class="col-md-12">
		<ul class="nav nav-tabs">
		  	<li role="presentation" class="active"><a href="{{ route('users.edit', array($id)) }}">General Info</a></li>
		  	@if($user->role == '3')
		  	<li role="presentation"><a href="{{ action('UserController@editSchedules', array($id)) }}">Classes </a></li>
		  	<li role="presentation"><a href="{{ action('UserController@editAppointments', array($id)) }}">Appointments </a></li>
		  	@endif
		</ul>
	</div>
</div>
@endif
@if(isset($user))
		{{ Form::model($user, array(
			'action' => array('UserController@update',$user->id),
			'files' => true,
			'method' => 'put'
		)) }}
	@else
		{{ Form::open(array('action' => 'UserController@store', 'files' => true)) }}
	@endif
<fieldset @if(isset($user) && ($cu->role >= $user->role)) disabled @endif>
<div class="row">
	<div class="col-md-6">
		@if(!isset($user))
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="panel-title">General Info</h4>
			</div>
			<div class="panel-body">
		@endif
			@if(isset($user) && isset($user->avatar) && $user->avatar != '')
			<div class="row">
				<div class="col-sm-12 col-md-8 col-lg-6">
					<img width="150" src="{{ asset('img/avatar/'.$user->avatar) }}" class="img-thumbnail">
				</div>
			</div>
			<div class="form-group">
				{{ Form::checkbox('avatar_del', '1', false, array('id' => 'avatar_del')) }}
				<label for="avatar_del">Delete Avatar</label>
			</div>
			@endif
			<div class="form-group {{ hasError($errors, 'avatar') }}">
				<label for="avatar">Avatar</label>
				{{ Form::file('avatar', array('class' => 'form-control')) }}
				{{ errorBlock($errors, 'avatar') }}
				<span class="help-block">Filetype must be .jpg or .png</span>
			</div>
			<div class="form-group {{ hasError($errors, 'role') }}">
				<label for="role">*Role</label>
				{{ Form::select('role', array('3' => 'Student', '2' => 'Teacher', '1' => 'Admin'), null, array('class' => 'form-control')) }}
				{{ errorBlock($errors, 'role') }}
			</div>
			<div id="input_name" class="form-group {{ hasError($errors, 'name') }}">
				<label for="name">*Name</label>
				{{ Form::text('name', null, array('class' => 'form-control')) }}
				{{ errorBlock($errors, 'name') }}
			</div>
			<div class="form-group {{ hasError($errors, 'password') }}">
				<label for="password">*Password</label>
				{{ Form::text('password', '', array('class' => 'form-control', 'disabled', 'placeholder' => isset($user) ? 'WARNING: Fill in only when change password.' : '')) }}
				<div id="input_password_auto">
					{{ Form::checkbox('password_auto', null, !isset($user), array('id' => 'password_auto')) }}
					{{ Form::label('password_auto', 'same as ic/passport') }}
				</div>
				{{ errorBlock($errors, 'password') }}
			</div>
			<div id="input_ic" class="form-group {{ hasError($errors, 'ic') }}">
				<label for="phone"><span class="emphasis">*</span>IC/Passport</label>
				{{ Form::text('ic', null, array('class' => 'form-control', 'placeholder' => 'eg: 0000000000000')) }}
				{{ errorBlock($errors, 'ic') }}
			</div>
			<div id="input_ref_id" class="form-group">
				<label for="role">Ref. Id</label>
				{{ Form::text('ref_id', null, array('class' => 'form-control')) }}
				@if(!isset($user))
				{{-- Form::checkbox('ref_id_auto', null, null, array('id' => 'ref_id_auto'))--}}
				{{-- Form::label('ref_id_auto', 'auto generate id') --}}
				@endif
			</div>
			<div id="input_startdate" class="form-group {{ hasError($errors, 'start_date') }}">
				<label for="start_date">Start Date</label><br>
				{{ Form::text('start_date', null, array('class' => 'form-control', 'id' => 'start_date')) }}
				{{ errorBlock($errors, 'start_date') }}
			</div>
			<div id="input_email" class="form-group {{ hasError($errors, 'email') }}">
				<label for="name"><span class="emphasis" style="display: none">*</span>Email</label>
				{{ Form::text('email', null, array('class' => 'form-control')) }}
				{{ errorBlock($errors, 'email') }}
			</div>
			<div class="form-group">
				<label for="age">Age</label>
				{{ Form::text('age', null, array('class' => 'form-control')) }}
			</div>
			<div class="form-group">
				<label for="phone">Phone Number</label>
				{{ Form::text('phone', null, array('class' => 'form-control', 'placeholder' => 'eg: 010-0000000')) }}
			</div>
			<div class="form-group">
				<label for="address">Address</label>
				{{ Form::textarea('address', null, array('class' => 'form-control', 'rows' => 4)) }}
			</div>
			<div class="form-group">
				<label for="remarks">Remarks</label>
				{{ Form::textarea('remarks', null, array('class' => 'form-control', 'rows' => 4)) }}
			</div>
	@if(!isset($user))
			</div>
		</div>
	</div>
	<div class="col-md-6" id="user-schedule-panel">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="panel-title">Assign to Classes</h4>
			</div>
			<div class="panel-body">
				@if(count($schedule) > 1)
				@for($i=0; $i<=2;$i++)
				<div class="form-group {{ hasError($errors, 'schedule_id.'.$i) }}">
					<label for="schedule_id">Class</label>
					{{ Form::select('schedule_id['.$i.']', $schedule, null, array('class' => 'form-control')) }}
					{{ errorBlock($errors, 'schedule_id.'.$i) }}
				</div>
				<div class="row">
					<div class="col-md-6">
						<div id="input_sstartdate" class="form-group {{ hasError($errors, 'sstart_date.'.$i) }}">
							<label for="sstart_date">Start Date</label><br>
							{{ Form::text('sstart_date['.$i.']', null, array('class' => 'form-control sstart_date')) }}
							{{ errorBlock($errors, 'sstart_date.'.$i) }}
						</div>
					</div>
					<div class="col-md-6">
						<div id="input_sexpirydate" class="form-group {{ hasError($errors, 'sexpiry_date.'.$i) }}">
							<label for="sexpiry_date">Expiry Date</label><br>
							{{ Form::text('sexpiry_date['.$i.']', null, array('class' => 'form-control sexpiry_date')) }}
							{{ errorBlock($errors, 'sexpiry_date.'.$i) }}
						</div>
					</div>
				</div>
				<div id="input_student_info" class="form-group @if($errors->has('total_session.'.$i) || $errors->has('used_session.'.$i) || $errors->has('exam.'.$i) || $errors->has('miss_count.'.$i)) has-error @endif">
					<label>Total Session/Used Session/Exam/Used Exam/Absent Count</label><br>
					{{ Form::text('total_session['.$i.']', null, array('class' => 'form-control', 'placeholder' => 'Total')) }}
					{{ Form::text('used_session['.$i.']', 0, array('class' => 'form-control', 'placeholder' => 'Used')) }}
					{{ Form::text('exam['.$i.']', null, array( 'class' => 'form-control', 'placeholder' => 'Exam')) }}
					{{ Form::text('used_exam['.$i.']', 0, array( 'class' => 'form-control', 'placeholder' => 'Used')) }}
					{{ Form::text('miss_count['.$i.']', 5, array('class' => 'form-control')) }}
					{{ errorBlock($errors, 'total_session.'.$i) }}
					{{ errorBlock($errors, 'used_session.'.$i) }}
					{{ errorBlock($errors, 'exam.'.$i) }}
					{{ errorBlock($errors, 'used_exam.'.$i) }}
					{{ errorBlock($errors, 'miss_count.'.$i) }}
				</div>
				<hr>
				@endfor
				@else
				<p>You have not created any schedule.</p>
				@endif
			</div>
		</div>
	</div>
	@endif
</div>
@if(!isset($user))
<div class="row">
	<div class="col-md-12">
		<div class="form-group">
			<input type="submit" value="Create New User" class="btn btn-primary btn-block">
		</div>
	</div>
</div>
@else
</div>
<div class="row">
	<div class="col-md-6">
		<div class="form-group">
			<input type="submit" value="Update User Info" class="btn btn-primary btn-block">
		</div>
	</div>
</div>
@endif
</fieldset>
{{ Form::close() }}
@stop