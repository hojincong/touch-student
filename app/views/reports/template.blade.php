<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<style>
			body{
				margin: 0;
				padding:5px;
				font-family: Helvetica, Arial, sans-serif;
			}
			.page-break {
			    page-break-after: always;
			}
			#title{
				font-size: 28px;
				margin-top: 0;
				text-align: center;
				margin-bottom: 15px;
			}
			#info{
				width: 100%;
				margin-bottom: 20px;
			}
			#info tr td.label{
				width: 10%;
			}
			#info tr td.content{
				width: 30%;
				font-weight: 900;
			}
			.profile{
				width: 15%;
				text-align: right;
			}
			.profile img{
				display: inline-block;
				width: 120px;
				max-height: 150px;
				@if((App::environment() == 'production') && $avatar[1])
				transform: rotate(90deg);
				@endif
			}
			hr{
				border: none;
				border-top: 1px solid #000;
			}
			.table{
				width: 100%;
				border-collapse: collapse;
			}
			.table thead tr th,
			.table tfoot tr td{
				font-weight: 500;
				border: 1px solid #000;
			}
			.table td{
				border: 1px solid #000;
				padding: 5px 7px;
				text-align: center;
			}
			.assessment{
				page-break-after: always;
			}
		</style>
	</head>
	<body onload="window.print();">
		<h1 id="title">Touch Learning &amp; Edu Adv S/B Student Report</h1>
		<table id="info">
			<tbody>
				<tr>
					<td class="label">Ref Id</td>
					<td class="content">{{ $user->ref_id or '' }}</td>
					<td class="label">Name</td>
					<td class="content">{{ $user->name }}</td>
					<td class="profile" rowspan="5">
						@if(App::environment() == 'production')
						<img src="data:image/png;base64,{{ $avatar[0] }}" alt="">
						@endif
					</td>
				</tr>
				<tr>
					<td class="label">Age</td>
					<td class="content">{{ $user->age or '' }}</td>
					<td class="label">Course Level</td>
					<td class="content">{{ $schedule->course->name or '' }}</td>
				</tr>
				<tr>
					<td class="label">IC/Passport</td>
					<td class="content">{{ $user->ic or '' }}</td>
					<td class="label">Counselling Session</td>
					<td class="content">{{ $info->total_session or 0 }} sessions ({{ $info->exam or 0 }} Exam)</td>
				</tr>
				<tr>
					<td class="label">Email</td>
					<td class="content">{{ $user->email or '' }}</td>
					<td class="label">Start Date</td>
					<td class="content">{{ $info->start_date or '' }}</td>
				</tr>
				<tr>
					<td class="label">No</td>
					<td class="content">{{ $user->phone or '' }}</td>
					<td class="label">Expiry Date</td>
					<td class="content">{{ $info->expiry_date or '' }}</td>
				</tr>
				<tr>
					<td class="label">Address</td>
					<td class="content">{{ $user->address or '-' }}</td>
				</tr>
			</tbody>
		</table>

		<table class="table appointments">
			<thead>
				<tr>
					<th style="width: 3%;">No</th>
					<th style="width: 10%;">Date</th>
					<th style="width: 10%;">Time</th>
					<th style="width: 21%;">Topics</th>
					<th style="width: 5%;">Attend</th>
					<th style="width: 1px;"></th>
					<th style="width: 3%;">No</th>
					<th style="width: 10%;">Date</th>
					<th style="width: 10%;">Time</th>
					<th style="width: 21%;">Topics</th>
					<th style="width: 5%;">Attend</th>				
				</tr>
			</thead>
			<?php $t = ceil(count($appointment)/2); ?>
			<?php $l = ($t > 0) ? array_fill(0, $t, true) : array(); ?>
			@forelse($l as $i=>$a)
			<tr>
				<?php $a = $appointment->offsetGet($i); ?>
				<td>{{ $i+1+$info->used_session }}</td>
				<td>{{ date('Y/m/d', strtotime($a->start_time)) }}</td>
				<td>{{ date('H:i', strtotime($a->start_time)) }} - {{ date('H:i', strtotime($a->end_time)) }}</td>
				<td>
					@if(isset($a->pivot->syllabus_id))
						<?php $s = Syllabus::withTrashed()->find($a->pivot->syllabus_id) ?>
						{{ $s->name or '(Lesson Deleted)' }} {{ $s->is_exam ? '(E)' : '' }}
					@else
						-
					@endif
				</td>
				<td>
					<?php $attend = $a->attendance()->where('student_id', $user->id)->first(); ?>
					{{ (isset($attend->attend) && $attend->attend) ? 'Y' : 'N' }}
				</td>
				<td></td>
				@if(($i+$t) < count($appointment))
				<?php $a = $appointment->offsetGet($i + $t); ?>
				<td>{{ $i+1+$t+$info->used_session }}</td>
				<td>{{ date('Y/m/d', strtotime($a->start_time)) }}</td>
				<td>{{ date('H:i', strtotime($a->start_time)) }} - {{ date('H:i', strtotime($a->end_time)) }}</td>
				<td>
					@if(isset($a->pivot->syllabus_id))
						<?php $s = Syllabus::withTrashed()->find($a->pivot->syllabus_id) ?>
						{{ $s->name or '(Lesson Deleted)' }} {{ $s->is_exam ? '(E)' : '' }}
					@else
						-
					@endif
				</td>
				<td>
					<?php $attend = $a->attendance()->where('student_id', $user->id)->first(); ?>
					{{ (isset($attend->attend) && $attend->attend) ? 'Y' : 'N' }}
				</td>
				@else
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				@endif
			</tr>
			@empty
			<tr>
				<td colspan="11">No appointment yet.</td>
			</tr>
			@endforelse
			<tfoot>
				<tr>
					<td>No</td>
					<td>Date</td>
					<td>Time</td>
					<td>Topics</td>
					<td>Attend</td>
					<td></td>
					<td>No</td>
					<td>Date</td>
					<td>Time</td>
					<td>Topics</td>
					<td>Attend</td>
				</tr>
			</tfoot>
		</table>

		<div class="page-break"></div>

		@foreach($versions as $i=>$version)
		<h1>
			@if($i == 0)
			2015-01-01 to {{ $remarks_meta[$version] }}
			@elseif($remarks_meta[$version] == 'default')
			{{ $remarks_meta[$versions[$i-1]] }} to {{ date('Y-m-d') }}
			@else
			{{ $remarks_meta[$versions[$i-1]] }} to {{ $remarks_meta[$version] }}
			@endif
		</h1>
		<?php $vvalue = $questions[$version]; ?> 
			@foreach($vvalue as $section=>$svalue)
				<table class="assessment table">
					<thead>
						<tr>
							<th style="width: 100px">{{ ucfirst($section) }} Assessment</th>
							@if(isset($remarks[$version]))
								@foreach($svalue as $q)
									<th>
										{{{ $q['title'] }}}
										@if(isset($q['question']))
										<br>({{{ $q['question'] }}})
										@endif
									</th>
								@endforeach
							@endif
						</tr>
					</thead>
					<tbody>
						@if(isset($remarks[$version]))
							@forelse($remarks[$version] as $r)
							<tr>
								<td>{{ $r['order'] }}</td>
								@foreach($svalue as $q)
									<td>{{ $r[$q['model']] or '-' }}</td>
								@endforeach
							</tr>
							@empty
							<tr>
								<td colspan="{{ count($svalue)+1 }}">No assessments yet.</td>
							</tr>
							@endforelse
						@endif
					</tbody>
					<tfoot>
						<tr>
							<td style="width: 100px">{{ ucfirst($section) }} Assessment</td>
							@if(isset($remarks[$version]))
								@foreach($svalue as $q)
									<td>
										{{{ $q['title'] }}}
										@if(isset($q['question']))
										<br>({{{ e($q['question']) }}})
										@endif
									</td>
								@endforeach
							@endif
						</tr>
					</tfoot>
				</table>
			@endforeach
		@endforeach
	</body>
</html>