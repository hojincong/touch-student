@extends('layout')

@section('breadcrumb')
	<li><a href="{{ action('AppointmentController@index') }}">Appointment Management</a></li>
	<li class="active">Exam List</li>
@stop

@section('page_title')
	<h1>Appointment Management</h1>
@stop

@section('page_action')
	<a href="{{ action('AppointmentController@create') }}" class="btn btn-primary"><span class="fa fa-plus"></span> Add Appointment</a>
@stop

@section('header_css')
	<link type="text/css" rel="stylesheet" href="{{ asset('lib/bootstrap-datepicker/css/datepicker.css') }}">
	<link type="text/css" rel="stylesheet" href="{{ asset('css/appointments.css') }}">
@stop

@section('footer_js')
	<script type="text/javascript" src="{{ asset('lib/moment/moment.js') }}"></script>
	<script type="text/javascript" src="{{ asset('lib/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
	<script type="text/javascript">
		var url = '{{ action('AppointmentController@listExam') }}';
		var year = {{ $year }};
		var month = {{ $month }};
	</script>
	<script type="text/javascript" src="{{ asset('js/appointments.js') }}"></script>
@stop

@section('contents')
	<div class="row">
		<div class="col-md-12">
			<ul class="nav nav-tabs">
				<li><a href="{{ action('AppointmentController@index') }}">Calendar</a></li>
			  	<li><a href="{{ action('AppointmentController@listIndex') }}">List View</a></li>
			  	<li class="active"><a href="{{ action('AppointmentController@listExam') }}">Exam List</a></li>
			</ul>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<nav id="month-paginator">
				<ul class="pagination">
					@for($i=1;$i<=12;$i++)
					<li<?php ($i == $month) && print(' class="active"') ?>>
						<a href="{{ action('AppointmentController@listExam', array($year, $i)) }}">
							{{ $i }} <br>
							{{ date('M', mktime(0,0,0,$i,10)) }}
						</a>
						@if($i == 6)
						<i id="month-activator" class="glyphicon glyphicon-calendar"></i>
						@endif
					</li>
					@endfor
				</ul>
			</nav>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<p id="appointment-total">Showing {{ count($appointments) }} Appointment(s) with Exam</p>
			<div class="table-responsive">
				<table id="appointment-list" class="table table-bordered">
					<thead>
						<tr>
							<th>Id</th>
							<th>Classes</th>
							<th>Students</th>
							<th>Exam</th>
							<th>Date</th>
							<th>Time</th>
							<th>Marked At</th>
							<th>Marked By</th>
							<th>Remarks 1</th>
							<th>Remarks 2</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						@forelse($appointments as $a)
							<?php $i = 0; ?>
							@foreach($a->user as $u)
							<tr>
								@if($i==0)
								<td rowspan="{{ $a->user->count() }}">{{ $a->id }}</td>
								<td rowspan="{{ $a->user->count() }}"><a href="{{ action('ScheduleController@edit', array($a->schedule_id)) }}">{{ $a->schedule->name }}</a></td>
								@endif
								<td>
									<a href="{{ route('users.show', array($u->id)) }}">
										{{ $u->name }}
									</a>
								</td>
								<td>
									({{ $processor->studentAptNo($u->id, $a->id) }}/{{ $u->schedules->find($a->schedule->id)->pivot->total_session }}) 
									<?php $sf = Syllabus::withTrashed()->find($u->pivot->syllabus_id) ?>
									@if(isset($sf))
									{{ $sf->name }} 
									{{ $sf->deleted_at ? '(Deleted)' : '' }}
									@else
									-
									@endif
								</td>
								@if($i==0)
								<td rowspan="{{ count($a->user) }}">
									{{ date('Y-m-d', strtotime($a->start_time)) }} 
								</td>
								<td rowspan="{{ count($a->user) }}">
									{{ date('G : i', strtotime($a->start_time)) }} 
									-
									{{ date('G : i', strtotime($a->end_time)) }}
								</td>
								<td rowspan="{{ count($a->user) }}">{{ $a->marked_at or '-' }}</td>
								<td rowspan="{{ count($a->user) }}">{{ $a->examMarker->name or '-' }}</td>
								<td rowspan="{{ count($a->user) }}">{{ ((!empty($a->notes) && isset($a->notes)) ? $a->notes : '-') }}</td>
								<td rowspan="{{ count($a->user) }}">{{ ((!empty($a->notes2) && isset($a->notes2)) ? $a->notes2 : '-') }}</td>
								<td rowspan="{{ count($a->user) }}" class="td-delete">
									<a href="{{ action('AppointmentController@edit', array($a->id)) }}" class="btn btn-warning"><i class="fa fa-pencil"></i> Edit info</a>
									<a href="{{ action('AppointmentController@getRemarks', array($a->id)) }}" class="btn btn-warning"><i class="fa fa-pencil"></i> Mark Exam</a>
								</td>
								@endif
							</tr>
							<?php $i++; ?>
							@endforeach
						@empty
						<tr>
							<td colspan="11">No student taking exam this month.</td>
						</tr>
						@endforelse
					</tbody>
					<tfoot>
						<tr>
							<td>Id</td>
							<td>Classes</td>
							<td>Students</td>
							<td>Exam</td>
							<td>Date</td>
							<td>Time</td>
							<td>Marked At</td>
							<td>Marked By</td>
							<td>Remarks 1</td>
							<td>Remarks 2</td>
							<td>Action</td>
						</tr>
					</tfoot>
				</table>
			</div>
		</div>
	</div>
@stop