@if(isset($appointment))
<a href="{{ route('appointment.show', array($appointment->id)) }}" class="btn btn-primary">
	<i class="fa fa-eye"></i> View
</a>
{{ Form::open(array('action' => array('AppointmentController@destroy', $appointment->id), 'method' => 'delete', 'class' => 'form-ib')) }}
	<button class="btn btn-danger"><i class="fa fa-trash"></i> Delete</button>
{{ Form::close() }}
@endif