@extends('layout')

@section('header_css')
	<link rel="stylesheet" href="{{ asset('css/appointments.css') }}">
@stop

@section('footer_js')
	<script type="text/javascript" src="{{ asset('js/appointments.js') }}"></script>
@stop

@section('breadcrumb')
	<li><a href="{{ action('AppointmentController@index') }}">Appointment Management</a></li>
	<li><a href="{{ action('AppointmentController@listIndex') }}">List View</a></li>
	<li class="active">Assessments</li>
@stop

@section('page_title')
	<h1>Assessments</h1>
	<h4>{{ date('Y-m-d G:i', strtotime( $appointment->start_time)) }} - {{ date('G:i', strtotime( $appointment->end_time)) }}</h4>
@stop

@section('page_action')
	@include('appointments.page_actions', compact($appointment))
@stop

@section('contents')
@include('appointments.tabs', array('id' => $id, 'active' => 'remarks'))
<div class="row">
	<div class="col-md-12">
		<p id="remarks-title">Rate Students' Performance From <b>1</b> &lt;The Worst&gt; To <b>5</b> &lt;The Best&gt;</p id="remarks-title">
	</div>
</div>
<div class="row">
	<div class="col-md-12">
{{ Form::open(array('route' => array('appointment.remarks.update', $id))) }}
@foreach($questions as $section=>$remarks)
		<div class="table-responsive">
			<table class="appointment-remarks table table-striped table-bordered table-hover">
				<thead>
					<tr>
						<th>{{ ucfirst($section) }} Assessment</th>
						@forelse($student as $u)
							<th><a href="{{ route('users.show', array($u->id)) }}">{{ $u->name }}</a></th>
						@empty
							<th>No Student Enrolled</th>
						@endforelse
					</tr>
				</thead>
				<tbody>
@foreach($remarks as $question)
				<tr>
					<td>
						<span>{{ $question['title'] }} </span>
						@if(isset($question['question']))
						<a class="btn btn-default pull-right assessment-question" data-toggle="popover" data-content="{{ $question['question'] }}"><span class="fa fa-info"></span></a>
						@endif
					</td>
@forelse($student as $u)
					<td class="{{ hasDanger($errors, $question['model'].'.'.$u->id) }}<?php (isset($question['type']) &&  $question['type'] == 'textarea') && print('top'); ?>">
						<?php $uid = $u->id; $r = $records->filter(function($r) use ($uid){
							return ($r->student_id == $uid);
						})->first();?>
@if(!isset($question['type']))
						{{ Form::number($question['model'].'['.$u->id.']', isset($r->{$question['model']}) ? $r->{$question['model']} : 0, array('min' => '0', 'max' => '5', 'step' => '0.1', 'class' => 'form-control')) }}
@elseif($question['type'] == 'range')
						{{ Form::number($question['model'].'['.$u->id.']', isset($r->{$question['model']}) ? $r->{$question['model']} : 0, array('min' => '0', 'max' => '5', 'step' => '0.1', 'class' => 'form-control')) }}
@elseif($question['type'] == 'textarea')
						{{ Form::textarea($question['model'].'['.$u->id.']', isset($r->{$question['model']}) ? $r->{$question['model']} : null, array('class' => 'form-control vresizable', 'rows' => '4', 'cols' => '0', 'placeholder' => 'Remarks')) }}				
@endif		
					</td>
@empty
					<td>-</td>
@endforelse
				</tr>
@endforeach
				</tbody>
				<tfoot>
					<tr>
						<td>{{ ucfirst($section) }} Assessment</td>
						@forelse($student as $u)
							<td><a href="{{ route('users.show', array($u->id)) }}">{{ $u->name }}</a></td>
						@empty
							<td>No Student Enrolled</td>
						@endforelse
					</tr>
				</thead>
			</table>
		</div>
@endforeach
@if($student->count())
	@if($has_exam)
		<div class="checkbox">
		    <label>
		      	{{ Form::checkbox('exam_marking', 1, isset($appointment->marked_at)) }} All exam marking has been completed
		    </label>
  		</div>
  	@endif
		<input type="submit" class="btn btn-primary btn-block" value="Update Assessments">
@endif
		{{ Form::close() }}
	</div>
</div>
@stop