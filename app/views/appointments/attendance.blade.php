@extends('layout')

@section('header_css')
	<link rel="stylesheet" href="{{ asset('css/appointments.css') }}">
@stop

@section('footer_js')
	<script type="text/javascript" src="{{ asset('js/appointments.js') }}"></script>
@stop

@section('breadcrumb')
	<li><a href="{{ action('AppointmentController@index') }}">Appointment Management</a></li>
	<li><a href="{{ action('AppointmentController@listIndex') }}">List View</a></li>
	<li class="active">Attendance</li>
@stop

@section('page_title')
	<h1>Attendance</h1>
	<h4>{{ date('Y-m-d G:i', strtotime( $appointment->start_time)) }} - {{ date('G:i', strtotime( $appointment->end_time)) }}</h4>
@stop

@section('page_action')
	@include('appointments.page_actions', compact($appointment))
@stop

@section('contents')
@include('appointments.tabs', array('id' => $id, 'active' => 'attendance'))
@if(!$is_updated)
<div class="row">
	<div class="col-md-12">
		<div class="alert alert-warning">
			Reminder: Please update students attendance.
		</div>
	</div>
</div>
@endif
<div class="row">
	<div class="col-md-12">
		<div class="table-responsive">
			<table id="appointment-attendance" class="table table-striped table-bordered table-hover">
				<thead>
					<tr>
						<th>Students</th>
						@forelse($student as $u)
							<th><a href="{{ route('users.show', array($u->id)) }}">{{ $u->name }}</a></th>
						@empty
							<th>No Student Enrolled</th>
						@endforelse
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						{{ Form::open(array('route' => array('appointment.attend.update', $id))) }}
						<td>Attendance</td>
						@forelse($student as $u)
							<td><label>{{ Form::checkbox('attend[]', $u->id, count($attendance->filter(function($a) use ($u){
								return (($a->student_id == $u->id) && ($a->attend));
							}))) }}</label></td>
						@empty
							<td><label>-</label></td>
						@endforelse
						<td>
						@if($student->count())
							<input type="submit" value="Update" class="btn btn-primary">
						@endif
						</td>
						{{ Form::close() }}
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>
@stop