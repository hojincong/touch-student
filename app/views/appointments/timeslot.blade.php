@extends('layout')

@section('breadcrumb')
	<li><a href="{{ action('AppointmentController@index') }}">Appointment Management</a></li>
	<li class="active">
	Add Time Slot
	</li>
@stop

@section('page_title')
	<h1>Add Timeslot</h1>
@stop

@section('page_action')
@stop

@section('contents')
{{ Form::open(array('action' => 'AppointmentController@storeTimeslot')) }}
	<div class="row">
		<div class="col-md-3 col-lg-2">
			<div class="form-group {{ hasError($errors, 'start_time') }}">
				{{ Form::label('start_time', 'Start Time (In 24 Hour Format)') }}
				{{ Form::text('start_time', null, array('class' => 'form-control', 'id' => 'start_time', 'placeholder' => 'eg: 11')) }}
				{{ errorBlock($errors, 'start_time') }}
			</div>
			<div class="form-group {{ hasError($errors, 'start_time_minute') }}">
				{{ Form::text('start_time_minute', null, array('class' => 'form-control', 'id' => 'start_time_minute', 'placeholder' => 'eg: 0')) }}
				{{ errorBlock($errors, 'start_time_minute') }}
			</div>
		</div>
		<div class="col-md-3 col-lg-2">
			<div class="form-group {{ hasError($errors, 'end_time') }}">
				{{ Form::label('end_time', 'End Time (In 24 Hour Format)') }}
				{{ Form::text('end_time', null, array('class' => 'form-control', 'id' => 'end_time', 'placeholder' => 'eg: 13')) }}
				{{ errorBlock($errors, 'end_time') }}
			</div>
			<div class="form-group {{ hasError($errors, 'end_time_minute') }}">
				{{ Form::text('end_time_minute', null, array('class' => 'form-control', 'id' => 'end_time_minute', 'placeholder' => 'eg: 45')) }}
				{{ errorBlock($errors, 'end_time_minute') }}
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6 col-lg-4">
			<input type="submit" value="Create New Time Slot" class="btn btn-primary btn-block">
		</div>
	</div>
{{ Form::close() }}
@stop