@extends('layout')

@section('breadcrumb')
	<li><a href="{{ action('AppointmentController@index') }}">Appointment Management</a></li>
	<li class="active">List View</li>
@stop

@section('page_title')
	<h1>Appointment Management</h1>
@stop

@section('page_action')
	<a href="{{ action('AppointmentController@create') }}" class="btn btn-primary"><span class="fa fa-plus"></span> Add Appointment</a>
@stop

@section('header_css')
	<link rel="stylesheet" href="{{ asset('lib/bootstrap-datepicker/css/datepicker.css') }}">
	<link rel="stylesheet" href="{{ asset('css/appointments.css') }}">
@stop

@section('footer_js')
	<script type="text/javascript" src="{{ asset('lib/moment/moment.js') }}"></script>
	<script type="text/javascript" src="{{ asset('lib/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
	<script type="text/javascript" src="{{ asset('lib/bootstrap-datepaginator/dist/bootstrap-datepaginator.min.js') }}"></script>
	<script type="text/javascript">
		var url = '{{ action('AppointmentController@listIndex') }}';
		var selectedDate = '{{ $date }}';
	</script>
	<script type="text/javascript" src="{{ asset('js/appointments.js') }}"></script>
@stop

@section('contents')
	<div class="row">
		<div class="col-md-12">
			<ul class="nav nav-tabs">
				<li><a href="{{ action('AppointmentController@index') }}">Calendar</a></li>
			  	<li class="active"><a href="{{ action('AppointmentController@listIndex') }}">List View</a></li>
			  	<li><a href="{{ action('AppointmentController@listExam') }}">Exam List</a></li>
			</ul>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div id="date-paginator"></div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<p id="appointment-total">Showing {{ count($appointments) }} Appointment(s)</p>
			<div class="table-responsive">
				<table id="appointment-list" class="table table-hover table-striped">
					<thead>
						<tr>
							<th>Id</th>
							<th>Classes</th>
							<th>Lesson Plan</th>
							<th>Seats</th>
							<th>Start Time</th>
							<th>End Time</th>
							<th>Attendance</th>
							<th>Assessed At</th>
							<th>Assessed By</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						@forelse($appointments as $a)
							<tr>
								<td>{{ $a->id }}</td>
								<td><a href="{{ action('ScheduleController@edit', array($a->schedule->id)) }}">{{ $a->schedule->name }}</a></td>
								<td>
									@forelse($a->syllabus as $s)
									<?php $sf = Syllabus::withTrashed()->find($s) ?>
									{{ $sf->name }} {{ $sf->deleted_at ? '(Deleted)' : '' }}<br>
									@empty
									-
									@endforelse
								</td>
								<td>{{ $a->user()->count() }} / {{ $a->seats }}</td>
								<td>{{ date('G : i', strtotime( $a->start_time)) }}</td>
								<td>{{ date('G : i', strtotime( $a->end_time)) }}</td>
								<td><i class="fa fa-{{ (count($a->attendance) >= count($a->user)) ? "check" : "close" }}"></i></td> 
								<td>{{ $a->assessed_at or '-' }}</td>
								<td>{{ $a->assessmentMarker->name or '-' }}</td>
								<td class="td-delete">
									<a href="{{ route('appointment.show', array($a->id)) }}" class="btn btn-primary"><i class="fa fa-eye"></i> View</a>
									<a href="{{ action('AppointmentController@edit', array($a->id)) }}" class="btn btn-warning"><i class="fa fa-pencil"></i> Edit</a>
									@if($cu->role <= 1)
									{{ Form::open(array('action' => array('AppointmentController@destroy', $a->id), 'method' => 'delete')) }}
										<button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i> Delete</button>
									{{ Form::close() }}
									@endif
								</td>
							</tr>
						@empty
							<tr>
								<td colspan="9">No appointment found.</td>
							</tr>
						@endforelse
					</tbody>
					<tfoot>
						<tr>
							<td>Id</td>
							<td>Classes</td>
							<td>Lesson Plan</td>
							<td>Seats</td>
							<td>Start Time</td>
							<td>End Time</td>
							<td>Attendance</td>
							<td>Assessed At</td>
							<td>Assessed By</td>
							<td>Action</td>
						</tr>
					</tfoot>
				</table>
			</div>
		</div>
	</div>
@stop