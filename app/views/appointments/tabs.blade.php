<div class="row">
	<div class="col-md-12">
		<ul class="nav nav-tabs">
		  	<li role="presentation" @if($active == 'general') class="active" @endif>
		  		<a href="{{ action('AppointmentController@edit', array($id)) }}">General Info</a>
		  	</li>
		  	<li role="presentation" @if($active == 'student') class="active" @endif>
		  		<a href="{{ action('AppointmentController@getStudents', array($id)) }}">Students</a>
		  	</li>
		  	<li role="presentation" @if($active == 'lesson') class="active" @endif>
		  		<a href="{{ action('AppointmentController@getLesson', array($id)) }}">Lesson Plan</a>
		  	</li>
		  	<li role="presentation" @if($active == 'attendance') class="active" @endif>
		  		<a href="{{ action('AppointmentController@getAttend', array($id)) }}">Attendance</a>
		  	</li>
		  	<li role="presentation" @if($active == 'remarks') class="active" @endif>
		  		<a href="{{ action('AppointmentController@getRemarks', array($id)) }}">Assessments</a>
		  	</li>
		</ul>
	</div>
</div>