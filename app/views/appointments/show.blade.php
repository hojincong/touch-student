@extends('layout')

@section('breadcrumb')
	<li><a href="{{ action('AppointmentController@index') }}">Appointment Management</a></li>
	<li><a href="{{ action('AppointmentController@listIndex') }}">List View</a></li>
	<li class="active">Attendance</li>
@stop

@section('header_css')
	<link rel="stylesheet" href="{{ asset('css/appointments.css') }}">
@stop

@section('footer_js')
	<script type="text/javascript" src="{{ asset('lib/exif-js/exif.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/imgfix.js') }}"></script>
@stop

@section('header')
	<div id="ap-header" class="jumbotron">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-6">
					<h1>{{ $appointment->schedule->name }}</h1>
					<p><i class="fa fa-clock-o"></i> {{ date('Y-m-d G : i', strtotime( $appointment->start_time))." - ".date('G : i', strtotime( $appointment->end_time)) }}</p>
				</div>
				<div id="main-header-actions" class="col-md-6">
					<a href="{{ route('appointment.edit', array($appointment->id)) }}" class="btn btn-warning">
						<i class="fa fa-pencil"></i> Edit
					</a>
					{{ Form::open(array('action' => array('AppointmentController@destroy', $appointment->id), 'method' => 'delete', 'class' => 'form-ib')) }}
						<button class="btn btn-danger"><i class="fa fa-trash"></i> Delete</button>
					{{ Form::close() }}
				</div>
			</div>
		</div>
	</div>
	<div id="main-header-tabs">
		<div class="container-fluid">
			<ul class="nav nav-tabs">
				<li class="active"><a href="#">Info</a></li>
			</ul>
		</div>
	</div>
@stop

@section('contents')
<div class="row">
	<div class="col-md-6">
		<div class="panel-group" id="syllabus-list">
			@if(isset($lessons))
				@foreach($lessons as $syllabus)
					@include('courses.panel', compact($syllabus))
				@endforeach
			@else
				<div class="panel panel-default">
					<div class="panel-body">No lesson in this appointment yet.</div>
				</div>
			@endif
		</div>
	</div>
	@if($cu->role <= 2)
	<div class="col-md-6">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">Students</h3>
			</div>
			<table class="table table-hover table-striped">
				<thead>
					<tr>
						<th width="10%">Ref. Id</th>
						<th width="30%">Avatar</th>
						<th width="30%">Name</th>
						<th width="30%">Actions</th>
					</tr>
				</thead>
				<tbody>
					@forelse($users as $user)
						<tr>
							<td>{{ $user->ref_id }}</td>
							<td class="td-thumbnail">
							@if(isset($user->avatar) && $user->avatar != '')
							<img src="{{ asset('img/avatar/'.$user->avatar) }}" class="img-thumbnail">
							@endif
							</td>
							<td>{{ $user->name }}</td>
							<td>
								<a href="{{ route('users.show', array($user->id)) }}" class="btn btn-primary"><i class="fa fa-eye"></i> View</a>
							</td>
						</tr>
					@empty
						<tr>
							<td colspan="4">No student enrolled.</td>
						</tr>
					@endforelse
				</tbody>
				<tfoot>
					<tr>
						<td>Ref. Id</td>
						<td>Avatar</td>
						<td>Name</td>
						<td>Actions</td>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
	@endif
</div>
@stop