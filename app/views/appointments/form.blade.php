@extends('layout')

@section('header_css')
	<link rel="stylesheet" href="{{ asset('lib/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}">
	<link rel="stylesheet" href="{{ asset('lib/fullcalendar/dist/fullcalendar.min.css') }}">
	<link type="text/css" rel="stylesheet" href="{{ asset('css/appointments.css') }}">
@stop

@section('footer_js')
	<script src="{{ asset('lib/moment/moment.js') }}" type="text/javascript"></script>
	<script type="text/javascript" src="{{ asset('lib/fullcalendar/dist/fullcalendar.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('lib/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/appointments.js') }}"></script>
@stop

@section('breadcrumb')
	<li><a href="{{ action('AppointmentController@index') }}">Appointment Management</a></li>
	<li><a href="{{ action('AppointmentController@listIndex') }}">List View</a></li>
	<li class="active">
		@if(isset($appointment))
		Edit
		@else
		Add
		@endif
		Appointment
	</li>
@stop

@section('page_title')
	@if(isset($appointment))
	<h1>Edit Appointment</h1>
	<h4>{{ date('Y-m-d G:i', strtotime( $appointment->start_time)) }} - {{ date('G:i', strtotime( $appointment->end_time)) }}</h4>
	@else
	<h1>Add Appointment</h1>
	@endif
@stop

@section('page_action')
@if(isset($appointment))
	@include('appointments.page_actions', compact($appointment))
@endif
@stop

@section('contents')
@if(isset($appointment))
	@include('appointments.tabs', array('id' => $id, 'active' => 'general'))
@endif
@if(count($schedule_list) > 0)
<div class="row">
	<div class="col-md-6 col-lg-4">
		@if(isset($appointment))
			{{ Form::model($appointment, array(
				'action' => array('AppointmentController@update',$appointment->id),
				'method' => 'put'
			)) }}
		@else
			{{ Form::open(array('action' => 'AppointmentController@store')) }}
		@endif
			<!--When showing, only when create-->
			@if(!isset($appointment))
			<div class="checkbox {{ hasError($errors, 'is_repeating') }}">
				<label>
					{{ Form::checkbox('is_repeating', '1') }}
					<b>Is Repeating Appointment</b>
				</label>
			</div>
			<div class="repeating" style="display: none">
				<div class="form-group {{ hasError($errors, 'end_date') }}">
					<label for="end_date">*Repeat Untill This Date (Including)</label>
					<div id="input_end_date" class="input-group">
						{{ Form::text('end_date', null, array('class' => 'form-control', 'id' => 'end_date' )) }}
						<span class="input-group-addon"><span class="fa fa-calendar"></span></span>
					</div>
					{{ errorBlock($errors, 'end_date') }}
				</div>
			</div>
			@endif

			<div class="form-group {{ hasError($errors, 'schedule_id') }}">
				<label for="schedule_id">*Class</label>
				@if($cu->role > 1)
					{{ Form::select('schedule_id', $schedule_list, null, array('class' => 'form-control', 'disabled')) }}
				@else
					{{ Form::select('schedule_id', $schedule_list, null, array('class' => 'form-control')) }}
				@endif
				{{ errorBlock($errors, 'schedule_id') }}
			</div>
			<div class="form-group {{ hasError($errors, 'classname') }}">
				<label for="classname">Class Name (Optional)</label>
				@if($cu->role > 1)
				{{ Form::text('classname', null, array('class' => 'form-control', 'id' => 'classname', 'disabled')) }}
				@else
				{{ Form::text('classname', null, array('class' => 'form-control', 'id' => 'classname', 'placeholder'=>'e.g.: Writing Skill Class')) }}
				@endif
				{{ errorBlock($errors, 'classname') }}
			</div>
			<div class="form-group {{ hasError($errors, 'seats') }}">
				<label for="seats">*Seats</label>
				@if($cu->role > 1)
				{{ Form::text('seats', null, array('class' => 'form-control', 'id' => 'seats', 'disabled')) }}
				@else
				{{ Form::text('seats', null, array('class' => 'form-control', 'id' => 'seats')) }}
				@endif
				{{ errorBlock($errors, 'seats') }}
			</div>
			<div class="form-group {{ hasError($errors, 'start_time') }}">
				<label for="start_time">*Start Time</label>
				<div id="input_start_time" class="input-group">
					@if($cu->role > 1)
					{{ Form::text('start_time', null, array('class' => 'form-control', 'id' => 'start_time', 'disabled')) }}
					@else
					{{ Form::text('start_time', null, array('class' => 'form-control', 'id' => 'start_time')) }}
					@endif
					<span class="input-group-addon"><span class="fa fa-calendar"></span></span>
				</div>
				{{ errorBlock($errors, 'start_time') }}
			</div>
			<div class="form-group {{ hasError($errors, 'end_time') }}">
				<label for="end_time">*End Time</label>
				<div id="input_end_time" class="input-group">
					@if($cu->role > 1)
					{{ Form::text('end_time', null, array('class' => 'form-control', 'id' => 'end_time', 'disabled')) }}
					@else
					{{ Form::text('end_time', null, array('class' => 'form-control', 'id' => 'end_time' )) }}
					@endif
					<span class="input-group-addon"><span class="fa fa-calendar"></span></span>
				</div>
				{{ errorBlock($errors, 'end_time') }}
			</div>
			<div class="form-group {{ hasError($errors, 'notes') }}">
				<label for="notes">Exam Marking Remarks</label>
				@if($cu->role > 1)
				{{ Form::textarea('notes', null, array('class' => 'form-control', 'id' => 'notes', 'disabled', 'rows' => 3)) }}
				@else
				{{ Form::textarea('notes', null, array('class' => 'form-control', 'id' => 'notes', 'rows' => 3)) }}
				@endif
				{{ errorBlock($errors, 'notes') }}
			</div>
			<div class="form-group {{ hasError($errors, 'notes2') }}">
				<label for="notes2">Exam Marking Remarks 2</label>
				@if($cu->role > 1)
				{{ Form::textarea('notes2', null, array('class' => 'form-control', 'id' => 'notes2', 'disabled', 'rows' => 3)) }}
				@else
				{{ Form::textarea('notes2', null, array('class' => 'form-control', 'id' => 'notes2', 'rows' => 3)) }}
				@endif
				{{ errorBlock($errors, 'notes2') }}
			</div>
			@if($cu->role <= 1)
			<div class="form-group">
				@if(isset($appointment))
				<input type="submit" value="Update Appointment Info" class="btn btn-primary btn-block">
				@else
				<input type="submit" value="Create New Appointment" class="btn btn-primary btn-block">
				@endif
			</div>
			@endif
		{{ Form::close() }}
	</div>
	<div class="col-md-6 col-lg-4">
		<div class="panel panel-default repeating" style="display: none">
  			<div class="panel-heading">
    			<h3 class="panel-title">Preview Repeating Dates</h3>
  			</div>
  			<div class="panel-body">
    			<div id="preview-cal"></div>
    			<p>*repeating appointments will be created are highlighted in green.</p>
  			</div>
		</div>
	</div>
</div>
@else
<p>No schedule available.</p>
@endif
@stop