@extends('layout')

@section('breadcrumb')
	<li><a href="{{ action('AppointmentController@index') }}">Appointment Management</a></li>
	<li class="active">Calendar</li>
@stop

@section('page_title')
	<h1>Appointment Management</h1>
@stop

@section('page_action')
@if($cu->role == 3)
<button id="calendar-help" data-toggle="popover" title="Info" data-placement="bottom" data-content="You can book for appointments within two weeks. You can still cancel them anytime but not after 10PM before the day of the appointment. Blue color represents your appointments, green represents available and red represents no more seats. Click on the tiles to book." class="btn btn-default"><span class="fa fa-info"></span></button> 
@endif
<button id="pHolidays" class="btn btn-default" data-toggle="popover" title="Public Holidays" data-placement="bottom" data-content="{{ $settings['pHolidays'] or '' }}"><span class="fa fa-calendar"></span> Public Holidays</button>
@if($cu->role <= 1)
<a href="{{ action('AppointmentController@create') }}" class="btn btn-primary"><span class="fa fa-plus"></span> Add Appointment</a>
@endif
@if(isset($id))
{{-- <div id="calendar-schedules" data-id="{{ $id }}" class="btn-group">
	<button class="btn btn-default">{{ $schedules[$id] }}</button>
	@if(count($schedules) > 1)
	<button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
		<span class="caret"></span>
	</button>
	<ul class="dropdown-menu dropdown-menu-right">
		@foreach($schedules as $k=>$s)
		@if($k != $id)
		<li><a href="{{ route('appointment.index') }}?class={{ $k }}">{{ $s }}</a></li>
		@endif
		@endforeach
	</ul>
	@endif
</div> --}}
@endif
@stop

@section('header_css')
	<link rel="stylesheet" href="{{ asset('lib/fullcalendar/dist/fullcalendar.min.css') }}">
	<link rel="stylesheet" href="{{ asset('css/schedule.css') }}">
	<link rel="stylesheet" href="{{ asset('css/appointments.css') }}">
@stop

@section('footer_js')
	<script type="text/javascript" src="{{ asset('lib/moment/moment.js') }}"></script>
	<script type="text/javascript" src="{{ asset('lib/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('lib/fullcalendar/dist/fullcalendar.min.js') }}"></script>
	<script type="text/javascript">
	$().ready(function(){
		$('#pHolidays').popover();
		$('#pHolidays').on('shown.bs.popover', function(){
			var id = $(this).attr('aria-describedby');
			$('#' + id).children('.popover-content').css('white-space', 'pre');
		});
	});
	</script>
	@if($cu->role == 3)
	<script type="text/javascript" src="{{ asset('js/tours/appointments.js') }}"></script>
	<script type="text/javascript">
	$().ready(function(){
		$('#calendar-help').popover();

		var calendar = $('#calendar');
		var schedules = $('#calendar-schedules').data('id');
		var eventUrl = '/appointment/events/';
		var calendarLoaded = true;
		var deleteStudent = '{{ action('AppointmentController@deleteEventStudent') }}';
		var addStudent = '{{ action('AppointmentController@addEventStudent') }}';
		if(schedules) eventUrl += schedules;

		calendar.fullCalendar({
			defaultView: 'agendaWeek',
			events: eventUrl,
			height: "auto",
			editable: false,
			minTime: '08:00',
			slotDuration: '00:15:00',
			allDaySlot: false,
			firstDay: 1,
			header: {
				left: '',
				center: 'title',
				right: 'today prev,next'
			},
			eventRender: function(event, element){
				if(event.contains){
					content = '<a class="btn btn-warning ap-delete" data-ap-id="' + event.id + '" href="#">Cancel Booking</a>';
				}else if(!event.available){
					content = '<a class="btn btn-danger" href="#" disabled>No More Seats</a>';
				}else if(moment(event.start).isAfter(moment())){
					content = '<a class="btn btn-primary ap-add" data-ap-id="' + event.id + '" href="#">Book</a>';
				}else{
					content = 'Not Available. Your session has finished or the booking time for this appointment has passed.';
				}

				element.popover({
					container	: '#calendar',
					content		: content,
					html 		: true,
					placement 	: "auto left"
				});
			},
			eventClick: function(){
				return false;
			},
			eventMouseover: function(event, jse, view){
				field = $(jse.currentTarget);
				if (!(field.data && field.data('bs.tooltip'))) {
					field.tooltip({
						title: event.title,
						container: 'body'
					});
					field.tooltip('toggle');
				}
			},
			loading: function(isLoading, view){
				if(isLoading == false){
					loadTour();
				}
			},
			viewRender: function(view, element){
				var header = $('td.fc-widget-header');
				header.width(header.outerWidth());
				header.affix({offset: {top: header.offset().top}});
			},
			windowResize: function(view) {
				var header = $('td.fc-widget-header');
				header.width('auto');
				header.width(header.outerWidth());
				header.affix({offset: {top: header.offset().top}});
			}
		});

		$('body').on('click', '.ap-delete', function(){
			var id = $(this).data('ap-id');
			var form = $('#ap-delete-form');
			form.children('input[name="ap_id"]').val(id);
			form.submit();
		});

		$('body').on('click', '.ap-add', function(){
			var id = $(this).data('ap-id');
			var form = $('#ap-add-form');
			form.children('input[name="ap_id"]').val(id);
			form.submit();
		});
	});
	</script>
	@endif
	<script type="text/javascript" src="{{ asset('js/appointments.js') }}"></script>
@stop

@section('contents')
{{ Form::open(array('action' => 'AppointmentController@addEventStudent', 'id' => 'ap-add-form')) }}
	{{ Form::hidden('ap_id') }}
{{ Form::close() }}
{{ Form::open(array('action' => 'AppointmentController@deleteEventStudent', 'id' => 'ap-delete-form')) }}
	{{ Form::hidden('ap_id') }}
{{ Form::close() }}
	<div class="row">
		<div class="col-md-12">
			<ul class="nav nav-tabs">
				<li class="active"><a href="{{ action('AppointmentController@index') }}">Calendar</a></li>
				@if($cu->role <= 2)
			  	<li><a href="{{ action('AppointmentController@listIndex') }}">List View</a></li>
			  	<li><a href="{{ action('AppointmentController@listExam') }}">Exam List</a></li>
			  	@endif
			  	<li class="active pull-right">
			  		<a href="#" data-id="{{ $id }}" id="calendar-schedules" class="dropdown-toggle" data-toggle="dropdown" aria-controls="calendar-schedules-list" aria-expanded="false">{{ $schedules[$id] }} @if(count($schedules) > 1)<span class="caret"></span>@endif</a>
					@if(count($schedules) > 1)
					<ul class="dropdown-menu dropdown-menu-right" id="calendar-schedules-list">
						@foreach($schedules as $k=>$s)
						@if($k != $id)
						<li><a href="{{ route('appointment.index') }}?class={{ $k }}">{{ $s }}</a></li>
						@endif
						@endforeach
					</ul>
					@endif
					<li class="pull-right">
						<a target="_blank" href="http://myenglishlab.pearson-intl.com/">MyEnglishLab</a>
					</li>
			  	</li>
			</ul>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div id="calendar"></div>
		</div>
	</div>
@stop