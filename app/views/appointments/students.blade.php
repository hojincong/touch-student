@extends('layout')

@section('header_css')
	<link rel="stylesheet" href="{{ asset('css/appointments.css') }}">
@stop

@section('footer_js')
	<script type="text/javascript" src="{{ asset('js/appointments.js') }}"></script>
@stop

@section('breadcrumb')
	<li><a href="{{ action('AppointmentController@index') }}">Appointment Management</a></li>
	<li><a href="{{ action('AppointmentController@listIndex') }}">List View</a></li>
	<li class="active">Students</li>
@stop

@section('page_title')
	<h1>Students</h1>
	<h4>{{ date('Y-m-d G:i', strtotime( $appointment->start_time)) }} - {{ date('G:i', strtotime( $appointment->end_time)) }}</h4>
@stop

@section('page_action')
	@include('appointments.page_actions', compact($appointment))
@stop

@section('contents')
<div class="modal fade" id="modal-student-form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Add Student</h4>
			</div>
			<div class="modal-body">
				Are you sure you want to add student to appointment? 
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">No</button>
				<button type="button" class="btn btn-primary modal-student-form-submit">Yes</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modal-student-form-remove" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Remove Student</h4>
			</div>
			<div class="modal-body">
				Are you sure you want to remove student from appointment? 
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">No</button>
				<button type="button" class="btn btn-danger modal-student-form-submit">Yes</button>
			</div>
		</div>
	</div>
</div>

@include('appointments.tabs', array('id' => $id, 'active' => 'student'))
<div class="row">
	<div class="col-md-6">
		<p>Seats available: {{ $appointment->user()->count() }} / {{ $appointment->seats }} @if($exam) ({{ $exam }} in exam) @endif</p>
		<div class="table-responsive">
			<table id="user-list" class="table table-hover table-striped">
				<thead>
					<tr>
						<th width="10%">Ref. Id</th>
						<th width="30%">Avatar</th>
						<th width="30%">Name</th>
						<th width="30%">Actions</th>
					</tr>
				</thead>
				<tbody>
					@forelse($student as $user)
						<tr>
							<td>{{ $user->ref_id }}</td>
							<td class="td-thumbnail">
							@if(isset($user->avatar) && $user->avatar != '')
							<img src="{{ asset('img/avatar/'.$user->avatar) }}" class="img-thumbnail">
							@endif
							</td>
							<td><a href="{{ route('users.show', array($user->id)) }}">{{ $user->name }}</a></td>
							<td>
								<button data-user-id="{{ $user->id }}" class="appointment-student-remove btn btn-danger">Remove</button>
							</td>
						</tr>
					@empty
						<tr>
							<td colspan="4">No student found.</td>
						</tr>
					@endforelse
				</tbody>
				<tfoot>
					<tr>
						<td>Ref. Id</td>
						<td>Avatar</td>
						<td>Name</td>
						<td>Actions</td>
					</tr>
				</tfoot>
			</table>
			{{ Form::open(array('action' => array('AppointmentController@deleteStudent', $id), 'id' => 'appointment-student-form-remove')) }}
				{{ Form::hidden('user_id', null) }}
			{{ Form::close() }}
		</div>
	</div>
	<div class="col-md-6">
		<div id="appointment-student-search">
			<form action="" class="form-inline">
				<div class="form-group">
					<label for="name">Search Student Name: </label>
					<input type="text" class="form-control" id="name" name="name" placeholder="Student Name" autocomplete="off">
				</div>
			</form>
		</div>
		<div id="appointment-student-list-r" class="table-responsive">
			<table id="appointment-student-list" class="table table-hover table-striped">
				<thead>
					<tr>
						<th width="10%">Ref. Id</th>
						<th width="30%">Avatar</th>
						<th width="30%">Name</th>
						<th width="30%">Actions</th>
					</tr>
				</thead>
				<tbody>
					@forelse($schedule_student as $user)
						<tr>
							<td>{{ $user->ref_id }}</td>
							<td class="td-thumbnail">
							@if(isset($user->avatar) && $user->avatar != '')
							<img src="{{ asset('img/avatar/'.$user->avatar) }}" class="img-thumbnail">
							@endif
							</td>
							<td><a href="{{ route('users.show', $user->id) }}">{{ $user->name }}</a></td>
							<td>
								<button data-user-id="{{ $user->id }}" class="appointment-student-add btn btn-primary">Add</button>
							</td>
						</tr>
					@empty
						<tr>
							<td colspan="4">No student found.</td>
						</tr>
					@endforelse
				</tbody>
				<tfoot>
					<tr>
						<td>Ref. Id</td>
						<td>Avatar</td>
						<td>Name</td>
						<td>Actions</td>
					</tr>
				</tfoot>
			</table>
			{{ Form::open(array('action' => array('AppointmentController@addStudent', $id), 'id' => 'appointment-student-form')) }}
				{{ Form::hidden('user_id', null) }}
			{{ Form::close() }}
		</div>
	</div>
</div>
@stop