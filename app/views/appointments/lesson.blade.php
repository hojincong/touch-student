@extends('layout')

@section('header_css')
	<link rel="stylesheet" href="{{ asset('css/appointments.css') }}">
@stop

@section('footer_js')
	<script type="text/javascript" src="{{ asset('js/appointments.js') }}"></script>
@stop

@section('breadcrumb')
	<li><a href="{{ action('AppointmentController@index') }}">Appointment Management</a></li>
	<li><a href="{{ action('AppointmentController@listIndex') }}">List View</a></li>
	<li class="active">Lesson Plan</li>
@stop

@section('page_title')
	<h1>Lesson Plan</h1>
	<h4>{{ date('Y-m-d G:i', strtotime( $appointment->start_time)) }} - {{ date('G:i', strtotime( $appointment->end_time)) }}</h4>
@stop

@section('page_action')
	@include('appointments.page_actions', compact($appointment))
@stop

@section('contents')
@include('appointments.tabs', array('id' => $id, 'active' => 'lesson'))
<div class="row">
	<div class="col-md-12">
		<div id="appointment-lesson-form">
			{{-- Form::model($appointment, array('route' => array('appointment.lesson.auto', $appointment->id), 'class' => 'form-inline')) --}}
				<!--<div class="checkbox">
					<label>{{ Form::checkbox('is_auto', null, null, array('id' => 'is_auto')) }} Choose Automatically</label>
				</div>
				<input type="submit" value="Save" class="btn btn-primary">-->
			{{-- Form::close() --}}
			{{ Form::open(array('route' => array('appointment.lesson.choose', $id))) }}
				<button type="submit" class="btn btn-primary">Choose Automatically</button>
			{{ Form::close() }}
			{{ Form::open(array('route' => array('appointment.lesson.delete', $id))) }}
				<button type="submit" class="btn btn-danger">Remove Selected Lesson</button>
			{{ Form::close() }}
		</div>
		<div class="table-responsive">
			<table id="appointment-lesson" class="table table-hover table-striped table-bordered">
				<thead>
					<tr>
						<th rowspan="2">
							@if(isset($appointment->schedule->course))
							<a href="{{ route('courses.show', array($appointment->schedule->course->id)) }}">{{ $appointment->schedule->course->name }}</a>
							@else
							(Course Deleted)
							@endif
						</th>
						<th colspan="{{ count($student) }}">Students</th>
						<th rowspan="2">Action</th>
					</tr>
					<tr>
						@forelse($student as $u)
							<th><a href="{{ route('users.show', array($u->id)) }}">{{ $u->name }}</a></th>
						@empty
							<th>No student</th>
						@endforelse
					</tr>
				</thead>
				<tbody>
					<?php $i = 0; ?>
					@forelse($lessons as $lesson)
					<?php $i++; ?>
						<tr>
							<td>{{ $lesson->name }}</td>
							@forelse($student as $u)
								@if($u->pivot->is_exam && ($i == 1))
								<td class="warning" rowspan="{{ count($lessons) }}">
									<?php $s = Syllabus::find($u->pivot->syllabus_id); ?>
									(Exam) {{ $s->name or '-' }}
								</td>
								@elseif($u->pivot->is_exam && ($i != 1))
								@elseif(isset($exams[$u->id]) && ($i == 1))
								<td rowspan="{{ count($lessons) }}">
									(Exam) {{ $exams[$u->id]->name }}
								</td>
								@elseif(isset($exams[$u->id]) && ($i != 1))
								@else
								<td @if(isset($u->pivot->syllabus_id) && ($lesson->id == $u->pivot->syllabus_id)) class="success" @endif>
									<i class="fa fa-{{ isset($history[$u->id][$lesson->id]) ? "check" : "close" }}"></i></td>
								@endif
							@empty
								<td>-</td>
							@endforelse
							<td>
								<a data-lesson-id="{{ $lesson->id }}" class="lesson-add btn btn-primary" @if(isset($appointment->syllabus->id) && $lesson->id == $appointment->syllabus->id) disabled @endif>Set as Lesson</a>
							</td>
						</tr>
					@empty
						<tr>
							<td>No lessons in this schedule. Add here </td>
							@forelse($student as $u)
								<td>-</td>	
							@empty
								<td>-</td>
							@endforelse
							<td>-</td>
						</tr>
					@endforelse
				</tbody>
				<tfoot>
					<tr>
						<td rowspan="2">
							@if(isset($appointment->schedule->course))
							<a href="{{ route('courses.show', array($appointment->schedule->course->id)) }}">{{ $appointment->schedule->course->name }}</a>
							@else
							(Course Deleted)
							@endif
						</td>
						@forelse($student as $u)
							<td><a href="{{ route('users.show', array($u->id)) }}">{{ $u->name }}</a></td>
						@empty
							<td>No student</td>
						@endforelse
						<td rowspan="2">Action</td>
					</tr>
					<tr>
						<td colspan="{{ count($student) }}">Students</td>
					</tr>
				</tfoot>
			</table>
			{{ Form::open(array('route' => array('appointment.lesson.add', $id), 'id' => 'appointment-lesson-add')) }}
				{{ Form::hidden('lesson_id', null) }}
			{{ Form::close() }}
		</div>
	</div>
</div>
@stop