@extends('layout')

@section('breadcrumb')
	<li><a href="{{ action('SettingController@getIndex') }}">Settings</a></li>
	<li class="active">Notifications</li>
@stop

@section('page_title')
	<h1>Notifications</h1>
@stop

@section('page_action')
<a href="{{ action('Setting\NotificationController@create') }}" class="btn btn-primary">
	<span class="fa fa-envelope"></span> Send Notification
</a>
@stop

@section('contents')
@include('settings.tabs', array('active' => 'notifications'))
<div class="row">
	<div class="col-md-12">
		<div class="table-responsive">
			<table id="notification-list" class="table table-hover table-striped">
				<thead>
					<tr>
						<th>Id</th>
						<th>Target User</th>
						<th>Message</th>
						<th>Date</th>
						<th>Read Status</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					@forelse($notifications as $n)
					<tr>
						<td>{{ $n->id }}</td>
						<td>{{ $target[$n->target] }}</td>
						<td>{{ $n->message }}</td>
						<td>{{ date('Y-m-d', strtotime($n->created_at)) }}</td>
						<td>{{ $n->read }}</td>
						<td class="td-delete">
							<a href="{{ action('Setting\\NotificationController@edit', array($n->id))}}" class="btn btn-warning"><i class="fa fa-pencil"></i> Edit</a>
							{{ Form::open(array('action' => array('Setting\\NotificationController@destroy', $n->id), 'method' => 'delete')) }}
								<button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i> Delete</button>
							{{ Form::close() }}
						</td>
					</tr>
					@empty
					<tr>
						<td colspan="6">No notication has been sent.</td>
					</tr>
					@endforelse
				</tbody>
				<tfoot>
					<tr>
						<td>Id</td>
						<td>Target User</td>
						<td>Message</td>
						<td>Date</td>
						<td>Read Status</td>
						<td>Actions</td>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
</div>
@stop