@extends('layout')

@section('breadcrumb')
	<li><a href="{{ action('SettingController@getIndex') }}">Settings</a></li>
	<li><a href="{{ action('Setting\NotificationController@index') }}">Notifications</a></li>
	<li class="active">
	@if(isset($notification))
		Edit
	@else
		Create
	@endif
	</li>
@stop

@section('page_title')
	<h1>New Notification</h1>
@stop

@section('page_action')
<a href="{{ action('Setting\NotificationController@index') }}" class="btn btn-primary">
	<span class="fa fa-chevron-left"></span> Back to List
</a>
@stop

@section('contents')
<div class="row">
	<div class="col-md-8 col-lg-6">
	@if(isset($notification))
		{{ Form::model($notification, array(
			'action' => array('Setting\\NotificationController@update', $id),
			'method' => 'put'
		)) }}
	@else
		{{ Form::open(array('action' => 'Setting\\NotificationController@store')) }}
	@endif
		<div class="form-group {{ hasError($errors, 'target') }}">
			<label for="target">Target User</label>
			{{ Form::select('target', array('all' => 'All User'), 'all', array('class' => 'form-control')) }}
			{{ errorBlock($errors, 'target') }}
		</div>
		<div class="form-group {{ hasError($errors, 'message') }}">
			<label for="message">Message</label>
			{{ Form::textarea('message', null, array('class' => 'form-control', 'rows' => '5')) }}
			{{ errorBlock($errors, 'message') }}
		</div>
		{{ Form::submit(isset($notification) ? 'Update Notification' : 'Send Notification', array('class' => 'btn btn-primary btn-block')) }}
	{{ Form::close() }}
	</div>
</div>
@stop