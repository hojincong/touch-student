<div class="row">
	<div class="col-md-12">
		<ul class="nav nav-tabs">
			<li role="presentation" @if($active == 'general') class="active" @endif>
				<a href="{{ action('SettingController@getIndex') }}">General</a>
			</li>
			<li role="presentation" @if($active == 'notifications') class="active" @endif>
				<a href="{{ action('Setting\NotificationController@index') }}">Notifications</a>
			</li>
		</ul>
	</div>
</div>