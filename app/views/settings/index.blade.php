@extends('layout')

@section('breadcrumb')
	<li class="active">Settings</li>
@stop

@section('page_title')
	<h1>Settings</h1>
@stop

@section('contents')
@include('settings.tabs', array('active' => 'general'))
<div class="row">
	<div class="col-md-10 col-lg-8">
		{{ Form::open(array(
			'action'	=> array('SettingController@postIndex'),
			'method'	=> 'post',
			'class'		=> 'form-horizontal'
		)) }}

		<div class="form-group">
			<label for="" class="col-md-2 control-label">Last Archive Student</label>
			<div class="col-md-10">
				@if(isset($settings['cron_archive_student_lastrun']) && !empty($settings['cron_archive_student_lastrun']))
					At {{ @date('Y-m-d H:i:s', $settings['cron_archive_student_lastrun']) }}<br>
					(Duration: {{ o(@$settings['cron_archive_student_lastrun_duration'], null) }}s)
				@else
					None
				@endif
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-2 control-label" for="pHolidays">Public Holidays<br>(shown above appointment calendar)</label>
			<div class="col-md-10">
				{{ Form::textarea('pHolidays', o(@$settings['pHolidays'], null), array('class' => 'form-control')) }}
			</div>
			
		</div>
		
		{{ Form::submit('Update Settings', array('class' => 'btn btn-primary btn-block')) }}
		{{ Form::close() }}
	</div>
</div>
@stop