@extends('clean')

@section('header_css')
	<link rel="stylesheet" href="{{ asset('css/appointments.css') }}">
	<style>body{background: #fff; padding: 20px;}</style>
@stop

@section('footer_js')
	<script type="text/javascript" src="{{ asset('js/appointments.js') }}"></script>
@stop

@section('contents')
<div class="row">
	<div class="col-md-12">
@foreach($questions as $section=>$remarks)
		<div class="table-responsive">
			<table class="appointment-remarks table table-striped table-bordered table-hover">
				<thead>
					<tr>
						<th>{{ ucfirst($section) }} Assessment</th>
						<th><a href="#">Student 1</a></th>
						<th><a href="#">Student 2</a></th>
					</tr>
				</thead>
				<tbody>
@foreach($remarks as $question)
				<tr>
					<td>
						<span>{{ $question['title'] }} </span>
						@if(isset($question['question']))
						<button class="btn btn-default pull-right assessment-question" data-toggle="popover" data-content="{{ $question['question'] }}"><span class="fa fa-info"></span></button>
						@endif
					</td>
					<td>{{ Form::number('','0' ,array('min' => '0', 'max' => '5', 'step' => '0.1', 'class' => 'form-control')) }}</td>
					<td>{{ Form::number('','0', array('min' => '0', 'max' => '5', 'step' => '0.1', 'class' => 'form-control')) }}</td>
				</tr>
@endforeach
				</tbody>
				<tfoot>
					<tr>
						<td>{{ ucfirst($section) }} Assessment</td>
						<td><a href="#">Student 1</a></td>
						<td><a href="#">Student 2</a></td>
					</tr>
				</thead>
			</table>
		</div>
@endforeach
	</div>
</div>
@stop