@extends('clean')

@section('header_css')
	<link rel="stylesheet" href="{{ asset('css/appointments.css') }}">
	<style>body{background: #fff; padding: 20px;}</style>
@stop

@section('footer_js')
	<script type="text/javascript" src="{{ asset('js/appointments.js') }}"></script>
@stop

@section('contents')
<div class="row">
	<div class="col-md-12">
		<p id="remarks-title">Rate Students' Performance From <b>1</b> &lt;The Worst&gt; To <b>5</b> &lt;The Best&gt;</p id="remarks-title">
	</div>
</div>
<div class="row">
	<div class="col-md-10">
@foreach($questions as $section=>$remarks)
		<div class="table-responsive">
			<table class="appointment-remarks table table-striped table-bordered table-hover">
				<thead>
					<tr>
						<th>{{ ucfirst($section) }} Assessment</th>
						<th><a href="#">Student 1</a></th>
						<th><a href="#">Student 2</a></th>
						<th><a href="#">Student 3</a></th>
						<th><a href="#">Student 4</a></th>
						<th><a href="#">Student 5</a></th>
						<th><a href="#">Student 6</a></th>
					</tr>
				</thead>
				<tbody>
@foreach($remarks as $question) 
				<tr>
					<td>
						<span>{{ $question['title'] }} </span>
						@if(isset($question['question']))
						<button class="btn btn-default pull-right assessment-question" data-toggle="popover" data-content="{{ $question['question'] }}"><span class="fa fa-info"></span></button>
						@endif
					</td>
					<td>{{ Form::number('','0' ,array('min' => '0', 'max' => '5', 'step' => '0.1', 'class' => 'form-control')) }}</td>
					<td>{{ Form::number('','0', array('min' => '0', 'max' => '5', 'step' => '0.1', 'class' => 'form-control')) }}</td>
					<td>{{ Form::number('','0' ,array('min' => '0', 'max' => '5', 'step' => '0.1', 'class' => 'form-control')) }}</td>
					<td>{{ Form::number('','0', array('min' => '0', 'max' => '5', 'step' => '0.1', 'class' => 'form-control')) }}</td>
					<td>{{ Form::number('','0' ,array('min' => '0', 'max' => '5', 'step' => '0.1', 'class' => 'form-control')) }}</td>
					<td>{{ Form::number('','0', array('min' => '0', 'max' => '5', 'step' => '0.1', 'class' => 'form-control')) }}</td>
				</tr>
@endforeach
				</tbody>
				<tfoot>
					<tr>
						<td>{{ ucfirst($section) }} Assessment</td>
						<td><a href="#">Student 1</a></td>
						<td><a href="#">Student 2</a></td>
						<th><a href="#">Student 3</a></th>
						<th><a href="#">Student 4</a></th>
						<th><a href="#">Student 5</a></th>
						<th><a href="#">Student 6</a></th>
					</tr>
				</thead>
			</table>
		</div>
@endforeach
	</div>
	<div class="col-md-2">
		<p><a href="#">Teacher 3</a> updated assessments on 2016-09-06 18:00 </p>
		<p><a href="#">Teacher 1</a> updated assessments on 2016-09-05 14:00 </p>
		<p><a href="#">Admin 1</a> updated assessments on 2016-09-05 13:00 </p>
		<p><a href="#">Teacher 3</a> updated assessments on 2016-09-05 12:00 </p>
		<p><a href="#">Teacher 2</a> updated assessments on 2016-09-04 14:00 </p>
		<p><a href="#">Teacher 1</a> updated assessments on 2016-09-04 12:00 </p>
	</div>
</div>
@stop