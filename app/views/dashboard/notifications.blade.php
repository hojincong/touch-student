@extends('layout')

@section('header_css')
	<link rel="stylesheet" href="{{ asset('css/dashboard.css') }}">
@stop

@section('breadcrumb')
	<li><a href="{{ action('HomeController@getIndex') }}">Home</a></li>
@stop

@section('page_title')
	<h1>Notifications</h1>
@stop

@section('footer_js')
	@if($cu->role == 3)
	<script type="text/javascript">
	var nextTour = '{{ action('UserController@show', array($cu->id)) }}?tour=1';
	</script>
	<script type="text/javascript" src="{{ asset('js/tours/dashboard.js') }}"></script>
	@endif
@stop

@section('contents')
<div class="row">
	<div class="col-md-12">
		<div class="table-responsive">
			<table id="dashboard-notifications" class="table table-hover table-striped">
				<thead>
					<tr>
						<th>Date</th>
						<th></th>
						<th>Message</th>
						<th>
							{{ Form::open(array('action' => 'HomeController@postMarkAllRead')) }}
								{{ Form::submit('Mark all as read', array('class' => 'btn btn-default')) }}
							{{ Form::close() }}
						</th>
					</tr>
				</thead>
				<tbody>
					@forelse($notifications as $n)
					<tr @if(!$n->is_read) class="warning" @endif>
						<td>{{ date('Y-m-d', strtotime($n->created_at)) }}</td>
						<td><span class="fa fa-exclamation"></span></td>
						<td>{{ $n->message }}</td>
						<td>
							@if($n->is_read)
								<span class="label label-info">Read</span>
							@else
								<span class="label label-danger">New</span>
							@endif
						</td>
					</tr>
					@empty
					<tr>
						<td colspan="4">No notifications yet.</td>
					</tr>
					@endforelse
				</tbody>
				<tfoot>
					<tr>
						<td>Date</td>
						<td></td>
						<td>Message</td>
						<td>
							{{ Form::open(array('action' => 'HomeController@postMarkAllRead')) }}
								{{ Form::submit('Mark all as read', array('class' => 'btn btn-default')) }}
							{{ Form::close() }}
						</td>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
</div>
@stop