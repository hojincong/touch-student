<!doctype html>
<html>
	<head>
		<title>Touch Learning & Edu Advisory Sdn Bhd</title>
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,600,700,300&amp;subset=latin" rel="stylesheet" type="text/css">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link rel="stylesheet" href="{{ asset('lib/bootstrap/dist/css/bootstrap.min.css') }}">
		<link rel="stylesheet" href="{{ asset('lib/fontawesome/css/font-awesome.min.css') }}">
		<link rel="stylesheet" href="{{ asset('css/theme.css') }}">
		<link rel="stylesheet" href="{{ asset('css/auth.css') }}">
	</head>
	<body>
		<div id="wrapper" class="container-fluid">
			<h1 id="slogan" class="hidden-sm hidden-xs">Learn What You Love</h1>
			<div class="row">
				<div id="ads-utm" class="col-md-3 hidden-sm hidden-xs">
					<img src="{{ asset('img/login/logo_utm.png') }}" class="img-responsive" alt="">
					<p>
					Part-Time Diploma in<br>
					* Business Administration<br>
					* Finance<br>
					* Entrepreneurship
					</p>
				</div>
				<div id="loginbox" class="col-md-6">
					<div id="loginbox-features" class="col-sm-5">
						<div class="row">
							<div class="col-xs-6 col-sm-12">
								<img src="{{ asset('img/logo.png') }}" alt="">
							</div>
							<div class="col-xs-6 visible-xs">
								<img src="{{ asset('img/login/logo_ielts.png') }}" class="img-responsive" alt="">
								<img src="{{ asset('img/login/logo_toefl.jpg') }}" class="img-responsive" alt="">
								<img src="{{ asset('img/login/logo_muet.gif') }}" class="img-responsive" alt="">
							</div>
						</div>
						<!--<h1>Touch Learning & Edu Advisory</h1>-->
						<div class="hidden-xs">
							<p><span class="fa fa-calendar"></span> Make Appointments</p>
							<p><span class="fa fa-download"></span> Download Handouts</p>
							<p><span class="fa fa-comments"></span> Check Teachers' Remarks</p>
							<p><span class="fa fa-line-chart"></span> Track Progress</p>
						</div>
					</div>
					<div id="loginbox-form" class="col-sm-7">
						<h1>Welcome Back</h1>
						@if(Session::has('msg_error'))
							<div class="alert alert-danger alert-dismissible" role="alert">
							  	<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
							  	{{ Session::get('msg_error') }}
							</div>
						@endif
						{{ Form::open() }}
							<div class="input-group">
							  	<span class="input-group-addon">
							  		<span class="fa fa-user"></span>
							  	</span>
							  	{{ Form::text('name', null, array(
									'class' => 'form-control',
									'placeholder' => 'User Name'
							  	)) }}
							</div>
							<div class="input-group">
							  	<span class="input-group-addon">
							  		<span class="fa fa-lock"></span>
							  	</span>
							  	{{ Form::password('password', array(
									'class' => 'form-control',
									'placeholder' => 'IC/Passport/Password'
							  	)) }}
							</div>
							<p id="slogan-form" class="visible-sm visible-xs">Learn What You Love</p>
							<input type="submit" value="Sign In" class="btn btn-default btn-lg btn-block">
						{{ Form::close() }}
					</div>							
				</div>
				<div id="ads-eng" class="col-md-3 hidden-xs hidden-sm">
					<p>
						<span style="color:red">IELTS</span>/TOEFL Course<br>
						<span style="font-weight: 900">Available Here</span>
					</p>
					<img src="{{ asset('img/login/logo_ielts.png') }}" class="img-responsive" alt="">
					<img src="{{ asset('img/login/logo_toefl.jpg') }}" class="img-responsive" alt="">
					<img src="{{ asset('img/login/logo_muet.gif') }}" class="img-responsive" alt="">
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div id="loginbox-help">
						<p class="hidden-sm hidden-xs">Your Class, Your Voice <span style="text-decoration: underline">touchcomment@hotmail.com</span><br></p>
						Need Help? Call <span style="text-decoration: underline">{{ Config::get('touchedu.telno') }}</span>. © 2015 Touch Learning & Edu Advisory Sdn Bhd			
					</div>		
				</div>
			</div>
			<div id="study" class="row hidden-xs hidden-sm">
				<h1>A Level, Foundation, Diploma, Degree<br>Study At</h1>
				<div class="col-md-2 col-sm-6 helper">
					<span style="color: red">After</span><br>
					SPM/UEC/STPM<br>
					<em>Where & What</em><br>
					<span style="color: red">Should I Study</span>
				</div>
				<div class="col-md-2 col-sm-6">
					<div id="map-my" class="map">
						<img id="" src="{{ asset('img/login/map_my.png') }}" class="img-responsive" alt="">
					</div>
					<h2>Malaysia</h2>
				</div>
				<div class="col-md-2 col-sm-6">
					<div class="map">
						<img src="{{ asset('img/login/map_uk.jpg') }}" class="img-responsive" alt="">
					</div>
					<h2>U.K.</h2>
				</div>
				<div class="col-md-2 col-sm-6">
					<div class="map">
						<img src="{{ asset('img/login/map_sg.png') }}" class="img-responsive" alt="">
					</div>
					<h2>Singapore</h2>	
				</div>
				<div class="col-md-2 col-sm-6">
					<div class="map">
						<img src="{{ asset('img/login/map_usa.png') }}" class="img-responsive" alt="">
					</div>
					<h2>U.S.A</h2>
				</div>
				<div class="col-md-2 col-sm-6">
					<div class="map">
						<img src="{{ asset('img/login/map_aus.png') }}" class="img-responsive" alt="">
					</div>
					<h2>Australia</h2>
				</div>
			</div>
			<div id="study-small" class="row visible-sm visible-xs">
				<div class="col-xs-12">
					<p><span style="color: red">University Registration Centre</span><br>大学咨询与报名中心/ Part-Time Diploma<br>A Level, Foundation, Diploma, Degree</p>
				</div>
				<div class="col-xs-6">
					<div class="map">
						<img src="{{ asset('img/login/flag_sg.jpg') }}" class="img-responsive" alt="">
					</div>
					<p>Singapore</p>	
				</div>
				<div class="col-xs-6">
					<div class="map">
						<img src="{{ asset('img/login/flag_uk.png') }}" class="img-responsive" alt="">
					</div>
					<p>U.K.</p>
				</div>
				<div class="col-xs-6">
					<div id="map-my" class="map">
						<img id="" src="{{ asset('img/login/flag_my.jpg') }}" class="img-responsive" alt="">
					</div>
					<p>Malaysia</p>
				</div>
				<div class="col-xs-6">
					<div class="map">
						<img src="{{ asset('img/login/flag_aus.jpg') }}" class="img-responsive" alt="">
					</div>
					<p>Australia</p>
				</div>
			</div>
		</div>
		<script src="{{ asset('lib/jquery/dist/jquery.min.js') }}"></script>
		<script src="{{ asset('lib/bootstrap/dist/js/bootstrap.min.js') }}"></script>
	</body>
</html>