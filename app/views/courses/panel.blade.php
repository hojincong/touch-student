<div class="panel panel-default">
	<div class="panel-heading">
		<h4 class="panel-title">
			<span href="#syllabus-{{ $syllabus->id }}" data-toggle="collapse">{{ $syllabus->name }}</span>
			@if(isset($new) && $new)
			<span class="badge">New</span>
			@endif
		</h4>
	</div>
	<div id="syllabus-{{ $syllabus->id }}" class="panel-collapse @if(isset($collapse)) collapse @endif">
		<div class="panel-body">
			<pre>{{ $syllabus->description }}</pre>
			@if($syllabus->attachurl()->count() > 0)
			@foreach($syllabus->attachurl as $attachurl)
				<i class="fa fa-link"></i> 
				<a href="{{ $attachurl->url }}" target="_blank">{{ $attachurl->url }}</a> 
				<br>
			@endforeach
			@endif
			@if($syllabus->attachment()->count() > 0)
			@foreach($syllabus->attachment as $attachment)
			<p>
				<i class="fa @if($attachment->mime == "application/pdf")
					fa-file-pdf-o
				@elseif(in_array($attachment->mime, array('video/x-ms-asf', 'audio/mpeg')))
					fa-file-audio-o
				@else
					fa-file-o
				@endif"></i> 
				<a href="{{ action('AttachmentController@show', array($attachment->id)) }}">{{ $attachment->name }}</a> ({{ round($attachment->size/(1024*1024), 2) }} MB)
				@if(($attachment->mime == "video/x-ms-asf") && $is_win)
				<div class="media"> 
				    <object codebase="http://www.apple.com/qtactivex/qtplugin.cab" 
				        classid="clsid:6BF52A52-394A-11d3-B153-00C04F79FAA6" 
				        type="application/x-oleobject"> 
				        <param name="url" value="{{ action('AttachmentController@show', array($attachment->id)) }}"> 
				        <embed src="{{ action('AttachmentController@show', array($attachment->id)) }}" 
				            type="application/x-mplayer2" 
				            pluginspage="http://www.microsoft.com/Windows/MediaPlayer/"></embed> 
				    </object> 
				</div>
				@elseif($attachment->mime == "audio/mpeg")
				<button class="btn btn-primary btn-xs play-mp3" data-src="{{ action('AttachmentController@show', array($attachment->id)) }}"><i class="fa fa-play"></i> Play</button>
				@endif
				</p>
			@endforeach
			@endif
		</div>
	</div>
</div>