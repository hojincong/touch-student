@extends('layout')

@section('breadcrumb')
	@if(isset($search) && $search == true)
	<li><a href="{{ action('CourseController@index') }}">Course Management</a></li>
	<li>Search</li>
	@else
	<li class="active"><a href="{{ action('CourseController@index') }}">Course Management</a></li>
	@endif
@stop

@section('page_title')
@if($cu->role >= 2)
	<h1>My Courses</h1>
	<h2>Browse materials of courses you've attended. </h2>
@else
	<h1>Course Management</h1>
	<h2>Create and edit materials of your courses.</h2>
@endif
@stop

@section('page_action')
@if($cu->role <= 1)
	<a href="{{ action('CourseController@create') }}" class="btn btn-primary"><span class="fa fa-plus"></span> Add Course</a>
@endif
@stop

@section('footer_js')
	@if($cu->role == 3)
	<script type="text/javascript">
		var nextTour = '{{ action('AppointmentController@index') }}?tour=1';
	</script>
	<script type="text/javascript" src="{{ asset('js/tours/courses.js') }}"></script>
	@endif
@stop

@section('contents')
<div class="row">
	<div class="col-md-12">
		<div class="table-responsive">
			<table id="course-list" class="table table-hover table-striped">
				<thead>
					<tr>
						<th>Id</th>
						<th>Title</th>
						<th>Description</th>
						@if($cu->role <= 2)
						<th>Lessons</th>
						<th>Exams</th>
						<th>Attachments</th>
						@endif
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					@forelse($courses as $course)
						<tr>
							<td>{{ $course->id }}</td>
							<td>{{ $course->name }}</td>
							<td>{{ $course->description }}</td>
							@if($cu->role <= 2)
							<td>
								@if($cu->role <= 1)
								<a href="{{ action('SyllabusController@index', array($course->id)) }}">
								{{ $course->syllabus()->where('is_exam', '=', false)->count() }}</a>
								@else
								{{ $course->syllabus()->where('is_exam', '=', false)->count() }}
								@endif
							</td>
							<td>
								@if($cu->role <= 1)
								<a href="{{ action('SyllabusController@exam', array($course->id)) }}">{{ $course->syllabus()->where('is_exam', '=', true)->count() }}</a>
								@else
								{{ $course->syllabus()->where('is_exam', '=', true)->count() }}
								@endif
							</td>
							<td>{{ $course->attachment()->count() }}</td>
							@endif
							<td class="td-delete">
								<a href="{{ route('courses.show', array($course->id)) }}" class="btn btn-primary"><i class="fa fa-eye"></i></a>
								@if($cu->role <= 1)
								<a href="{{ action('CourseController@edit', array($course->id)) }}" class="btn btn-warning"><i class="fa fa-pencil"></i></a>
								{{ Form::open(array('action' => array('CourseController@destroy', $course->id), 'method' => 'delete')) }}
									<button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i></button>
								{{ Form::close() }}
								@endif
							</td>
						</tr>
					@empty
						<tr>
							<td colspan="7">No course found.</td>
						</tr>
					@endforelse
				</tbody>
				<tfoot>
					<tr>
						<td>Id</td>
						<td>Title</td>
						<td>Description</td>
						@if($cu->role <= 2)
						<td>Lessons</td>
						<td>Exams</td>
						<td>Attachments</td>
						@endif
						<td>Actions</td>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
</div>
@stop