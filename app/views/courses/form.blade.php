@extends('layout')

@section('breadcrumb')
	<li><a href="{{ action('CourseController@index') }}">Course Management</a></li>
	<li class="active">
	@if(isset($course))
	Edit Course
	@else
	Add Course
	@endif
	</li>
@stop

@section('page_title')
	@if(isset($course))
	<h1>Edit Course</h1>
	@else
	<h1>Add Course</h1>
	@endif
@stop

@section('page_action')
@if(isset($course))
	<a href="{{ route('courses.show', array($course->id)) }}" class="btn btn-primary"><i class="fa fa-eye"></i> View</a>
	{{ Form::open(array('action' => array('CourseController@destroy', $course->id), 'method' => 'delete', 'class' => 'form-ib')) }}
		<button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i> Delete</button>
	{{ Form::close() }}
@endif
@stop

@section('contents')
@if(isset($course))
<div class="row">
	<div class="col-md-12">
		<ul class="nav nav-tabs">
		  	<li role="presentation" class="active"><a href="{{ action('CourseController@edit', array($id)) }}">General Info</a></li>
		  	<li role="presentation"><a href="{{ action('SyllabusController@index', array($id)) }}">Lessons <span class="badge">{{ @$tab_count['syllabus'] }}</span></a></li>
		  	<li role="presentation"><a href="{{ action('SyllabusController@exam', array($id)) }}">Exams <span class="badge">{{ @$tab_count['exam'] }}</span></a></li>
		</ul>
	</div>
</div>
@endif
<div class="row">
	<div class="col-md-6">
		@if(isset($course))
			{{ Form::model($course, array(
				'action' => array('CourseController@update',$course->id),
				'files' => true,
				'method' => 'put'
			)) }}
		@else
			{{ Form::open(array('action' => 'CourseController@store', 'files' => true)) }}
		@endif
			<div class="form-group {{ hasError($errors, 'name') }}">
				<label for="name">*Title</label>
				{{ Form::text('name', null, array('class' => 'form-control')) }}
				{{ errorBlock($errors, 'name') }}
			</div>
			<div class="form-group">
				<label for="description">Description</label>
				{{ Form::textarea('description', null, array('class' => 'form-control', 'row' => 8)) }}
			</div>
			<div class="form-group">
				@if(isset($course))
				<input type="submit" value="Update Course Info" class="btn btn-primary btn-block">
				@else
				<input type="submit" value="Create New Course" class="btn btn-primary btn-block">
				@endif
			</div>
		{{ Form::close() }}
	</div>
</div>
@stop