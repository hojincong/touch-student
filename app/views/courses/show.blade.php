@extends('layout')

@section('breadcrumb')
	<li><a href="{{ action('CourseController@index') }}">Course Management</a></li>
	<li class="active">View Course</li>
@stop

@section('header')
	<div class="jumbotron">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-6">
					<h1>{{ $course->name }}</h1>
					<p>{{ $course->description }}</p>
				</div>
				@if($cu->role <= 1)
				<div id="main-header-actions" class="col-md-6">
					<a href="{{ route('courses.edit', array($course->id)) }}" class="btn btn-warning"><i class="fa fa-pencil"></i> Edit</a>
					{{ Form::open(array('action' => array('CourseController@destroy', $course->id), 'method' => 'delete')) }}
						<button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i> Delete</button>
					{{ Form::close() }}
				</div>
				@endif
			</div>
		</div>
	</div>
	<div id="main-header-tabs">
		<div class="container-fluid">
			<ul class="nav nav-tabs">
				<li @if($tabs == "lessons") class="active" @endif>
					<a href="{{ route('courses.show', array($course->id)) }}">Lessons @if($cu->role <= 2)<span class="badge">{{ $tab_count['syllabus'] or 0 }}</span>@endif</a>
				</li>
				<li @if($tabs == "exam") class="active" @endif>
					<a href="{{ route('courses.show.exam', array($course->id)) }}">Exams @if($cu->role <= 2)<span class="badge">{{ $tab_count['exam'] or 0 }}</span>@endif</a>
				</li>
			</ul>
		</div>
	</div>
@stop

@section('footer_js')
<script type="text/javascript">
$().ready(function(){
	$('.play-mp3').click(function(){
		$(this).after('<br><audio controls autoplay><source src="' + $(this).data('src') + '" type="audio/mpeg"></audio>');
		$(this).remove();
	});
});
</script>
@stop

@section('contents')
	<div class="row">
		<div class="col-md-6">
			<div class="panel-group" id="syllabus-list">
			@forelse($syllabuses as $syllabus)
				@if($cu->role == 3)
					@include('courses.panel', array('syllabus' => $syllabus, 'collapse' => true, 'new' => in_array($syllabus->id, $recent)))
				@else
					@include('courses.panel', array('syllabus' => $syllabus, 'collapse' => true))
					@endif
			@empty
				<div class="panel panel-default">
					<div class="panel-body">No {{ io($tabs == 'exam', 'exam', 'lesson') }} in this course yet.</div>
				</div>
			@endforelse
			</div>
		</div>
	</div>
@stop