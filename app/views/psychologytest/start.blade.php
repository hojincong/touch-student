@section('content')

<div id="info-box" class="row">
	<div class="col-md-6 col-md-offset-3">
		<h1>测前必读</h1>
		<p>所有题目都是必选题。很可能两个选项都是在描述你，但永远想的是你更是怎么样的，你更在意什么，更喜欢什么。</p>
		<p>这个测试寻找的是你最天生的倾向，不要过多考虑具体的情境，也不要想你的老板、同事、家人、朋友对你的期望。选项​​没有优劣之分，任何一个倾向以及一个性格类型都有其优缺点。请选择真实的自己，而不是期望的自己。</p>
		<p id="start-btn"><a href="{{ action('PsychologyTestController@getStart') }}" class="btn btn-primary btn-lg">开始答题</a></p>
	</div>
</div>
@stop