<!doctype html>
<html>
	<head>
		<title>职业性格测试</title>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link rel="stylesheet" href="{{ asset('lib/bootstrap/dist/css/bootstrap.min.css') }}?v={{ $assets_version }}">
		<link rel="stylesheet" href="{{ asset('css/psychologytest.css') }}">
		<script src="{{ asset('lib/jquery/dist/jquery.min.js') }}"></script>
		<script src="https://code.highcharts.com/highcharts.js"></script>
		<script src="https://code.highcharts.com/highcharts-more.js"></script>
	</head>
	<body>
		<nav id="header-bar" class="navbar navbar-default navbar-fixed-top">
			<div class="container-fluid">
				<div class="navbar-header">
					<a class="navbar-brand" href="{{ action('PsychologyTestController@getIndex') }}">
						<img alt="Brand" src="{{ asset('img/logo.png') }}">
						<h1>职业性格测试系统</h1>
					</a>
				</div>
				@if(Session::has('psychotest'))
				<a id="restart" href="{{ action('PsychologyTestController@getRestart') }}" class="btn btn-warning navbar-btn pull-right">重新开始</a>
				@endif
			</div>
		</nav>
		@section('header')
		@show
		<div class="container-fluid">
			@section('content')
			@show
		</div>
	</body>
</html>