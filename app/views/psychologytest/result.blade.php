@section('content')
	<div id="result-box" class="row">
		<div class="col-md-6 col-md-offset-3">
			<h1><img src="{{ asset('img/logo.png') }}">职业性格测试结果（您的类型：{{ $type }}）</h1>
			<p id="result-graph"></p>
			<div id="explain">
				<p><span class="title">{{ $explain->title }}</span> - <span class="subtitle">{{ $explain->subtitle }}</span></p>
				<p class="text">{{ $explain->explain }}</p>
				<p class="person">{{ $explain->person }}</p>
			</div>
			<div class="different">
				{{ $different }}
			</div>
			<p class="notice">注意：这个性格测试寻找的是哪个是你天生的倾向，就像在问你是右撇子还是左撇子。图中的百分比仅仅代表你在做题的时候对自己是左撇子还是右撇子有多确定。每个人一定有性格倾向性，可能明显，可能不那么明显。一旦确定了你的性格类型，百分比就不是很重要，没有“我是80%的内向的人”这种说法，就像没有“我是80%的左撇子”。性格无优劣，了解自​​己不是为了给自己加标签，而是为了发挥优势控制缺点。 </p>

		</div>
	</div>
	<script type="text/javascript">
	$(function () {
	    $('#result-graph').highcharts({
	        chart: {
	            polar: true,
	            type: 'line'
	        },
	        title: {
	            text: '{{ $type }}'
	        },
	        xAxis: {
	            categories: ['{{ implode('\',\'', $labels) }}'],
	            tickmarkPlacement: 'on',
	            lineWidth: 0,
	        },
	        yAxis: {
	            gridLineInterpolation: 'polygon',
	            lineWidth: 0,
	            min: 0,
	            max: 100,
	            labels: {
	            	formatter: function(){
		            	return this.value + '%';
		            }
	            }
	        },
	        tooltip: {
	        	enabled: false
	        },
	        legend: {
	            enabled: false
	        },
	        series: [{
	            data: {{ $series }},
	            pointPlacement: 'on'
	        }]
	    });
	});
	</script>
@stop