@section('header')
<div id="progress" class="progress">
	<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: {{ 100*($id/47) }}%;">
	    {{ $id }}/47
	</div>
</div>
@stop

@section('content')
<div id="question-box" class="row">
	<div class="col-md-6 col-md-offset-3">
		<h1>{{ $question }}</h1>
		@foreach($choices as $c)
		{{ Form::open(array('action' => array('PsychologyTestController@postAnswer'))) }}
		<input type="hidden" name="type" value="{{ $c->type }}">
		<button class="btn btn-block btn-info">{{ $c->text }}</button>
		{{ Form::close() }}
		@endforeach
	</div>
</div>
@stop