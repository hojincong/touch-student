<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMarkingToApuserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('appointments', function(Blueprint $table){
			$table->integer('marked_by')->nullable()->after('is_auto');
			$table->timestamp('marked_at')->nullable()->after('is_auto');
			$table->mediumText('notes')->nullable()->after('is_auto');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('appointments', function(Blueprint $table){
			$table->dropColumn('marked_by');
			$table->dropColumn('marked_at');
			$table->dropColumn('notes');
		});
	}

}
