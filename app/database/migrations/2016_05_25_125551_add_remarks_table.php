<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRemarksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('remarks', function(Blueprint $table)
		{
			$table->double('g5', 2, 1)->nullable();
			$table->double('g6', 2, 1)->nullable();
			$table->double('g7', 2, 1)->nullable();
			$table->longText('g8')->nullable();
			$table->double('v1', 2, 1)->nullable();
			$table->double('v2', 2, 1)->nullable();
			$table->double('v3', 2, 1)->nullable();
			$table->longText('v4')->nullable();
			$table->double('r1', 2, 1)->nullable();
			$table->double('r2', 2, 1)->nullable();
			$table->double('r3', 2, 1)->nullable();
			$table->longText('r4')->nullable();
			$table->double('w1', 2, 1)->nullable();
			$table->double('w2', 2, 1)->nullable();
			$table->double('w3', 2, 1)->nullable();
			$table->longText('w4')->nullable();
			$table->double('s8', 2, 1)->nullable();
			$table->double('s9', 2, 1)->nullable();
			$table->double('s10', 2, 1)->nullable();
			$table->longText('s11')->nullable();
			$table->double('l1', 2, 1)->nullable();
			$table->double('l2', 2, 1)->nullable();
			$table->double('l3', 2, 1)->nullable();
			$table->longText('l4')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('remarks', function(Blueprint $table)
		{
			$table->dropColumn('g5');
			$table->dropColumn('g6');
			$table->dropColumn('g7');
			$table->dropColumn('v1');
			$table->dropColumn('v2');
			$table->dropColumn('v3');
			$table->dropColumn('r1');
			$table->dropColumn('r2');
			$table->dropColumn('r3');
			$table->dropColumn('w1');
			$table->dropColumn('w2');
			$table->dropColumn('w3');
			$table->dropColumn('s8');
			$table->dropColumn('s9');
			$table->dropColumn('s10');
			$table->dropColumn('l1');
			$table->dropColumn('l2');
			$table->dropColumn('l3');
		});
	}

}
