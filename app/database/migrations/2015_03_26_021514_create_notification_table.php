<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('notifications', function($table){
			$table->increments('id');
			$table->integer('user_id')->unsigned();
			$table->longText('message')->nullable();
			$table->longText('link')->nullable();
			$table->enum('type', array('danger', 'warning', 'info', 'success'))->default('info');
			$table->longText('observer')->nullable();
			$table->longText('observer_id')->nullable();
			$table->longText('observer_id2')->nullable();
			$table->longText('observer_id3')->nullable();
			$table->boolean('is_read')->default(false);
			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('notifications');	
	}

}
