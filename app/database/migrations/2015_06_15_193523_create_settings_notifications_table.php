<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsNotificationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('settings_notifications', function($table){
			$table->increments('id');
			$table->text('message')->nullable();
			$table->text('link')->nullable();
			$table->enum('type', array('danger', 'warning', 'info', 'success'))->default('info');
			$table->text('target');
			$table->text('target_id1')->nullable();
			$table->text('target_id2')->nullable();
			$table->text('target_id3')->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('settings_notifications');	
	}

}
