<?php

use Illuminate\Database\Migrations\Migration;

class CreateRemarksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('remarks', function ($table) {
			$table->increments('id');
			$table->integer('student_id')->unsigned();
			$table->integer('appointment_id')->unsigned();
			//$table->longText('remark');
			$table->integer('s1')->nullable();
			$table->integer('s2')->nullable();
			$table->integer('s3')->nullable();
			$table->integer('s4')->nullable();
			$table->integer('s5')->nullable();
			$table->integer('s6')->nullable();
			$table->longText('s7')->nullable();
			$table->integer('g1')->nullable();
			$table->integer('g2')->nullable();
			$table->integer('g3')->nullable();
			$table->longText('g4')->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('remarks');
	}

}
