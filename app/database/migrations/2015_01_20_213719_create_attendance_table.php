<?php

use Illuminate\Database\Migrations\Migration;

class CreateAttendanceTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('attendances', function ($table) {
			$table->increments('id');
			$table->integer('student_id')->unsigned();
			$table->integer('appointment_id')->unsigned();
			$table->tinyInteger('attend')->unsigned()->default(0);
			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('attendances');
	}

}
