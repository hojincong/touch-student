<?php

use Illuminate\Database\Migrations\Migration;

class CreateSchedulesTutorsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('schedules_tutors', function ($table) {
			$table->integer('schedule_id')->unsigned();
			$table->integer('user_id')->unsigned();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('schedules_tutors');
	}

}
