<?php

use Illuminate\Database\Migrations\Migration;

class CreateAttachmentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::connection('courses')->create('attachments', function ($table) {
			$table->increments('id');
			$table->integer('syllabus_id')->unsigned();
			$table->text('name');
			$table->bigInteger('size');
			$table->string('mime');
			$table->text('uniqid');
			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::connection('courses')->dropIfExists('attachments');
	}

}
