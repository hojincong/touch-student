<?php

use Illuminate\Database\Migrations\Migration;

class CreateAttachurlTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::connection('courses')->create('attachurl', function ($table) {
			$table->increments('id');
			$table->integer('syllabus_id')->unsigned();
			$table->text('url');
			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migration.
	 *
	 * @return void
	 */
	public function down() {
		Schema::connection('courses')->dropIfExists('attachurl');
	}

}
