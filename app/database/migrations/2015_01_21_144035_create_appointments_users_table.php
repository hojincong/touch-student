<?php

use Illuminate\Database\Migrations\Migration;

class CreateAppointmentsUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('appointments_users', function ($table) {
			$table->integer('appointment_id')->unsigned();
			$table->integer('user_id')->unsigned();
			$table->integer('syllabus_id')->unsigned()->nullable();
			$table->boolean('is_exam')->default(0)->nullable();
			$table->boolean('is_missed')->default(0)->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('appointments_users');
	}

}
