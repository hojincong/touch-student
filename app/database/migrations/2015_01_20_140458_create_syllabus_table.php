<?php

use Illuminate\Database\Migrations\Migration;

class CreateSyllabusTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::connection('courses')->create('syllabus', function ($table) {
			$table->increments('id');
			$table->integer('course_id')->unsigned();
			$table->text('name');
			$table->text('description')->nullable();
			$table->integer('order')->nullable()->unsigned();
			$table->boolean('is_exam')->default(false);
			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::connection('courses')->dropIfExists('syllabus');
	}

}
