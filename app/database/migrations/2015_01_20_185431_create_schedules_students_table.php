<?php

use Illuminate\Database\Migrations\Migration;

class CreateSchedulesStudentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('schedules_students', function ($table) {
			$table->increments('id');
			$table->integer('schedule_id')->unsigned();
			$table->integer('user_id')->unsigned();
			$table->date('start_date');
			$table->date('expiry_date');
			$table->integer('total_session')->nullable()->default(0);
			$table->integer('used_session')->nullable()->default(0);
			$table->integer('exam')->nullable()->default(0);
			$table->integer('miss_count')->unsigned()->nullable()->default(0);
			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('schedules_students');
	}

}
