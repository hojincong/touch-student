<?php

use Illuminate\Database\Migrations\Migration;

class CreateAclTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('acl', function ($table) {
			$table->integer('role_id')->unsigned();
			$table->string('permission');
			$table->boolean('value')->default(true);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('acl');
	}

}
