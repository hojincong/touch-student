<?php

use Illuminate\Database\Migrations\Migration;

class CreateAppointmentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('appointments', function ($table) {
			$table->increments('id');
			$table->integer('schedule_id')->unsigned();
			$table->integer('seats')->unsigned();
			$table->timestamp('start_time');
			$table->timestamp('end_time');
			$table->boolean('is_auto')->default(false);
			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('appointments');
	}

}
