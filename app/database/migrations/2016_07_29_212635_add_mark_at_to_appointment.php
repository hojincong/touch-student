<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMarkAtToAppointment extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('appointments', function(Blueprint $table){
			$table->integer('assessed_by')->nullable()->after('is_auto');
			$table->timestamp('assessed_at')->nullable()->after('is_auto');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('appointments', function(Blueprint $table){
			$table->dropColumn('assessed_by');
			$table->dropColumn('assessed_at');
		});
	}

}
