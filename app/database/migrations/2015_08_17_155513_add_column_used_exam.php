<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnUsedExam extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if(!Schema::hasTable('schedules_students')) return;

		Schema::table('schedules_students', function($table){
			$table->integer('used_exam')->nullable()->default(0)->after('exam');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if(!Schema::hasTable('schedules_students')) return;

		Schema::table('schedules_students', function($table){
			$table->dropColumn('used_exam');
		});
	}

}
