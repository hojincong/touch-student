<?php

use Illuminate\Database\Migrations\Migration;

class CreateUserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('users', function ($table) {
			$table->increments('id');
			$table->integer('role');
			$table->text('name');
			$table->string('password');
			$table->string('email')->nullable();
			$table->string('ic')->nullable();
			$table->text('address')->nullable();
			$table->text('avatar')->nullable();
			$table->integer('age')->nullable();
			$table->string('phone')->nullable();
			$table->text('remarks')->nullable();
			$table->text('ref_id')->nullable();
			$table->date('start_date')->nullable();
			$table->rememberToken();
			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('users');
	}

}
