<?php

class SyllabusTableSeeder extends Seeder {

	public function run() {
		for ($i = 0; $i <= 3; $i++) {
			Syllabus::create(array('name' => 'test' . $i, 'order' => $i, 'is_exam' => 0, 'course_id' => '1'));
		}
	}

}