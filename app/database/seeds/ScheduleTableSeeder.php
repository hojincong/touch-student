<?php

class ScheduleTableSeeder extends Seeder {

	public function run() {
		$course = Course::find(1);

		$schedule = new Schedule();
		$schedule->course()->associate($course);
		$schedule->save();

		$tutor = User::find(1);
		$tutor->schedules()->attach($schedule);

		$info = array(
			'start_date' => date('Y-m-d'), 
			'expiry_date' => date('y-m-d', time() + (60 * 60 * 24 * 31)), 
			'total_session' => 30, 
			'exam' => '5', 
			'miss_count' => '5'
		);

		$student = User::find(2);
		$student->schedules()->attach($schedule, $info);
		$student = User::find(3);
		$student->schedules()->attach($schedule, $info);
		$student = User::find(4);
		$student->schedules()->attach($schedule, $info);
	}

}