<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		Eloquent::unguard();

		$this->call('UserTableSeeder');
		$this->command->info('User table seeded!');

		$this->call('CourseTableSeeder');
		$this->command->info('Course table seeded!');

		$this->call('SyllabusTableSeeder');
		$this->command->info('Syllabus table seeded!');

		$this->call('ScheduleTableSeeder');
		$this->command->info('Schedule table seeded!');
	}

}
