<?php

class UserTableSeeder extends Seeder {

	public function run() {
		$user = new User;
		$user->name = 'admin';
		$user->password = 'password';
		$user->role = 1;
		$user->save();

		$student = new User;
		$student->name = 'student 1';
		$student->password = 'password';
		$student->role = 3;
		$student->save();

		$student = new User;
		$student->name = 'student 2';
		$student->password = 'password';
		$student->role = 3;
		$student->save();

		$student = new User;
		$student->name = 'student 3';
		$student->password = 'password';
		$student->role = 3;
		$student->save();
	}

}