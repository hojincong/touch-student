<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
 */

Route::group(array('before' => 'auth'), function () {

	Route::group(array('prefix' => 'users'), function(){

		Route::get('/search', 'UserController@search');
		Route::get('/{users}/edit/schedules', 'UserController@editSchedules');
		Route::post('/{users}/edit/schedules', 'UserController@createSchedules');
		Route::post('/{users}/edit/schedules/{sid}', 'UserController@updateSchedules');
		Route::delete('/{users}/edit/schedules/{sid}/destroy', 'UserController@destroySchedules');

		Route::get('/{users}/edit/appointments/{sid?}', 'UserController@editAppointments');
		Route::delete('/{users}/edit/appointments/{sid?}/destroy', 'UserController@destroyAppointments');
		Route::get('/{users}/show/appointments/{sid?}', 'UserController@showAppointments');
		
		Route::get('/{users}/show/assessments/{sid?}', 'UserController@showAssessments');

	});

	Route::resource('users', 'UserController');

	Route::get('attachments/{id}/destroy', 'AttachmentController@destroy');
	Route::get('attachments/{id}/destroyUrl', 'AttachmentController@destroyUrl');
	Route::resource('attachments', 'AttachmentController', array('only' => array('show')));

	Route::post('courses/{id}/edit/lessons/reorder', 'SyllabusController@reorder');
	Route::resource('courses/{id}/edit/lessons', 'SyllabusController', array('except' => array('show', 'create', 'edit')));
	Route::get('courses/{id}/edit/exam', 'SyllabusController@exam');
	Route::get('courses/{id}/lessons', array(
		'as' => 'courses.show.lessons', 
		'uses' => 'CourseController@show'
	));
	Route::get('courses/{id}/exams', array(
		'as' => 'courses.show.exam', 
		'uses' => 'CourseController@exam'
	));
	Route::resource('courses', 'CourseController');

	Route::get('schedules/{id}/edit/students', 'ScheduleController@editStudents');
	Route::resource('schedules', 'ScheduleController', array('except' => array('show')));

	Route::get('appointment/list/{year?}/{month?}/{day?}', 'AppointmentController@listIndex');
	Route::get('appointment/exam/{year?}/{month?}', 'AppointmentController@listExam');
	Route::get('appointment/events/{sid?}', 'AppointmentController@getEvents');
	Route::post('appointment/events/add', 'AppointmentController@addEventStudent');
	Route::post('appointment/events/delete', 'AppointmentController@deleteEventStudent');

	Route::get('appointment/{id}/students', 'AppointmentController@getStudents');
	Route::post('appointment/{id}/students', 'AppointmentController@addStudent');
	Route::post('appointment/{id}/students/delete', 'AppointmentController@deleteStudent');

	Route::get('appointment/{id}/lessons', 'AppointmentController@getLesson');
	Route::post('appointment/{id}/lessons', array(
		'as' => 'appointment.lesson.add', 
		'uses' => 'AppointmentController@addLesson'
	));
	Route::post('appointment/{id}/lessons/delete', array(
		'as' => 'appointment.lesson.delete', 
		'uses' => 'AppointmentController@deleteLesson'
	));
	Route::post('appointment/{id}/lessons/auto', array(
		'as' => 'appointment.lesson.choose', 
		'uses' => 'AppointmentController@chooseAuto'
	));
	/*Route::post('appointment/{id}/lessons/auto', array(
		'as' => 'appointment.lesson.auto', 
		'uses' => 'AppointmentController@toggleAuto'
	));*/

	Route::get('appointment/{id}/attendance', 'AppointmentController@getAttend');
	Route::post('appointment/{id}/attendance/update', array(
		'as' => 'appointment.attend.update', 
		'uses' => 'AppointmentController@updateAttend')
	);

	Route::get('appointment/{id}/remarks', 'AppointmentController@getRemarks');
	Route::post('appointment/{id}/remarks/update', array(
		'as' => 'appointment.remarks.update', 
		'uses' => 'AppointmentController@updateRemarks')
	);
	Route::resource('appointment', 'AppointmentController');

	Route::get('reports/{uid}/{sid}', 'ReportController@getReport');

	Route::resource('setting/notification', 'Setting\\NotificationController', array('except' => array('show')));
	Route::controller('setting', 'SettingController');

	Route::controller('dashboard', 'HomeController');

	Route::controller('mbti', 'PsychologyTestController');

	Route::get('/', 'HomeController@getIndex');

});

/*
Route::group(array('prefix' => 'demo'), function(){

	Route::get('/assessment', function(){
		$data['questions'] = Config::get('question.ver2');
		$data['assets_version'] = 2;

		return View::make('demo.assessment', $data);
	});

	Route::get('/history', function(){
		$data['questions'] = Config::get('question.ver2');
		$data['assets_version'] = 2;

		return View::make('demo.history', $data);
	});

});*/

/*Route::get('update', function(){
	Artisan::call('migrate', array('--force' => true));
});*/

//Route::controller('/utility', 'DevController');

Route::controller('/', 'AuthController');