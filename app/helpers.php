<?php

function hasError($errors, $field, $preExp = true) {
	return ($errors->has($field) && $preExp) ? 'has-error' : '';
}

function hasDanger($errors, $field, $preExp = true) {
	return ($errors->has($field) && $preExp) ? 'danger' : '';
}

function errorBlock($errors, $field, $preExp = true) {
	return ($errors->has($field) && $preExp) ? '<span class="help-block">' . $errors->first($field) . '</span>' : '';
}

if (!function_exists('o')) {
	function o($str, $default = null) {
		if (isset($str)) {
			return $str;
		} elseif ($default) {
			return $default;
		}
	}
}

if (!function_exists('io')) {
	function io($condition, $str, $default = null) {
		if ($condition) {
			return $str;
		} elseif ($default) {
			return $default;
		}
	}
}