<?php

class SettingController extends \BaseController{

	protected $layout = "layout";

	protected $active = "setting";

	protected $fillable = array('pHolidays');

	public function __construct(){
		parent::__construct();

		$this->beforeFilter(function(){
			$role = $this->data['cu']->role;

			if($role > 1) return Redirect::action('HomeController@getIndex');
		});
	}

	public function getIndex(){
		$this->data['settings'] = Setting::lists('value', 'key');

		$this->layout->with($this->data);
		$this->layout->content = View::make('settings.index', $this->data);
	}

	public function postIndex(){
		$input = Input::only($this->fillable);
		$validation = Validator::make($input, array(
			//'pHolidays' => 'required'
		));

		if($validation->fails()){
			return Redirect::action('SettingController@getIndex')->withInput($input)->withErrors($validation)->with('msg_error', 'There are some errors in your form.');
		}else{
			foreach($this->fillable as $field){
				if(isset($input[$field])){
					$setting = Setting::firstOrNew(array('key' => $field));
					$setting->value = $input[$field];
					$setting->save();
				}
			}

			return Redirect::action('SettingController@getIndex')->with('msg_success', 'Settings updated successfully.');			
		}
	}

}