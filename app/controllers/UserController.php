<?php

class UserController extends \BaseController {

	protected $layout = "layout";

	protected $active = "user";

	public function __construct(Processes\User $user) {
		parent::__construct();

		$this->user = $user;

		$this->beforeFilter('@onlyAdmin', array('except' => array('show', 'showAppointments', 'showAssessments')));
		$this->beforeFilter('@onlyCu', array('only' => array('show', 'showAssessments', 'showAppointments')));
	}

	public function onlyAdmin($route, $request){

		$role = $this->data['cu']->role;
		$uid = $this->data['cu']->id;

		if($role > 1) return Redirect::action('UserController@show', array($uid));

	}

	public function onlyCu($route, $request){

		$role = $this->data['cu']->role;
		$uid = $this->data['cu']->id;

		if(($role == 3) && ($route->parameter('users') != $uid)) return Redirect::action('UserController@show', array($uid));		

	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index() {
		$this->data['role'] = $this->user->role_filter(Input::get('role', '3'));
		$this->data['inactive'] = Input::get('inactive', false);
		$this->data['users'] = $this->user->get($this->data['role'], 50, $this->data['inactive']);
		$this->data['paginator'] = $this->data['users']->appends(array('role' => $this->data['role'], 'inactive' => $this->data['inactive']))->links();
		$this->data['user_count'] = array(
			'student' 	=> $this->user->count(3),
			'inactive'	=> $this->user->count(3,1),
			'teacher'	=> $this->user->count(2),
			'admin'		=> $this->user->count(1)
		);

		$this->layout->with($this->data);
		$this->layout->content = View::make('users.index', $this->data);
	}

	public function search() {
		$this->data['role'] = 'all';
		$this->data['users'] = $this->user->search(Input::get('q'), 50);
		$this->data['search'] = true;
		$this->data['user_count'] = array(
			'student' 	=> $this->user->count(3),
			'inactive'	=> $this->user->count(3,1),
			'teacher'	=> $this->user->count(2),
			'admin'		=> $this->user->count(1)
		);
		$this->data['paginator'] = $this->data['users']->appends(array('q' => Input::get('q')))->links();

		$this->layout->with($this->data);
		$this->layout->content = View::make('users.index', $this->data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create() {
		$this->data['schedule'] = Schedule::all()->lists('name', 'id');
		$this->data['schedule'][-1] = 'None';
		ksort($this->data['schedule']);

		$this->layout->with($this->data);
		$this->layout->content = View::make('users.form', $this->data);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store() {
		return $this->user->save(Input::all(), Input::file('avatar'));
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id) {
		$this->data['id'] = $id;
		$this->data['user'] = User::findOrFail($id);
		$this->data['tabs'] = "schedules";
		$this->data['schedules'] = $this->data['user']->schedules;
		$this->data['appointments'] = $this->data['user']->appointments->groupBy('schedule_id');
		$this->data['exams'] = $this->data['user']->appointments->filter(function($a){
			return ($a->pivot->is_exam && !$a->pivot->is_missed);
		})->groupBy('schedule_id');
		$this->data['miss'] = $this->data['user']->appointments->filter(function($a) use ($id){
			return $a->attendance->filter(function($attend) use ($id){
				return (($attend->student_id == $id) && ($attend->attend == 0));
			})->count();
		})->groupBy('schedule_id');

		$this->data['is_current_user'] = ($id == $this->data['cu']->id);

		$this->layout->with($this->data);
		$this->layout->content = View::make('users.show', $this->data);
	}

	public function showAppointments($id, $sid = null){
		$this->data['id'] = $id;
		$this->data['sid'] = $sid;
		$this->data['tabs'] = "appointments";
		$this->data['user'] = User::findOrFail($id);

		if($this->data['user']->schedules->count() > 0){
			if(!isset($sid) || !$this->data['user']->schedules->contains($sid))
				$sid = $this->data['user']->schedules->first()->id;
			$this->data['schedule'] = $this->data['user']->schedules->lists('name', 'id');
			$this->data['appointments'] = $this->data['user']->appointments()->where('schedule_id', $sid)->orderBy('start_time')->get();
			$this->data['index'] = $this->data['user']->schedules->find($sid)->pivot->used_session;
			$this->data['max_miss'] = $this->data['user']->schedules->find($sid)->pivot->miss_count;
		}else{
			$this->data['schedule'] = array();
			$this->data['appointments'] = array();
		}

		$this->data['is_current_user'] = ($id == $this->data['cu']->id);

		$this->layout->with($this->data);
		$this->layout->content = View::make('users.show', $this->data);
	}

	public function showAssessments($id, $sid = null){
		$this->data['id'] = $id;
		$this->data['tabs'] = "assessments";
		$this->data['user'] = User::findOrFail($id);
		$this->data['ver'] = Input::get('ver');
		$this->data['qmeta'] = Config::get('question._meta.versions');
		if($this->data['user']->schedules->count() >= 1){
			//schedule list
			if(!isset($sid) || !$this->data['user']->schedules->contains($sid))
				$sid = $this->data['user']->schedules->first()->id;
			$this->data['schedule'] = $this->data['user']->schedules->lists('name', 'id');

			//get related assessment version
			$this->data['versions'] = $this->user->getRemarksVersion($id, $sid);
			if(!in_array($this->data['ver'], $this->data['versions']) && (count($this->data['versions']) > 0))
				$this->data['ver'] = $this->data['versions'][0];

			$after = null;
			$before = null;
			if(count($this->data['versions']) > 0){
				$index = array_search($this->data['ver'], $this->data['versions'])+1;
				if(isset($this->data['versions'][$index])){
					$after = $this->data['qmeta'][$this->data['versions'][$index]]." 23:59:59";
				}else{
					$after = null;
				}

				if($this->data['qmeta'][$this->data['ver']] == 'default'){
					$before = null;
				}else{
					$before = $this->data['qmeta'][$this->data['ver']].' 23:59:59';
				}
			}

			$this->data['remarks'] = $this->user->getRemarks($id, $sid, $after, $before);
		}else{
			$this->data['schedule'] = array();
		}

		$this->data['sid'] = $sid; 

		$this->data['questions'] =  Config::get('question.'.$this->data['ver']);

		$this->data['is_current_user'] = ($id == $this->data['cu']->id);

		$this->layout->with($this->data);
		$this->layout->content = View::make('users.show', $this->data);	
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id) {
		$this->data['id'] = $id;
		$this->data['user'] = User::findOrFail($id);

		$this->data['schedule'] = Schedule::all()->lists('name', 'id');
		$this->data['schedule'][-1] = 'None';
		ksort($this->data['schedule']);

		$this->layout->with($this->data);
		$this->layout->content = View::make('users.form', $this->data);
	}

	public function editSchedules($id){
		$this->data['id'] = $id;
		$this->data['schedules'] = User::findOrFail($id)->schedules;

		$this->data['schedule_list'] = Schedule::all()->lists('name', 'id');
		$this->data['schedule_list'][-1] = 'None';
		foreach($this->data['schedule_list'] as $key => $s){
			if($this->data['schedules']->contains($key))
				unset($this->data['schedule_list'][$key]);
		}
		ksort($this->data['schedule_list']);

		$this->layout->with($this->data);
		$this->layout->content = View::make('users.schedules', $this->data);
	}

	public function createSchedules($id){
		$input = Input::only('schedule_id', 'start_date', 'expiry_date', 'total_session', 'exam', 'used_exam', 'miss_count', 'used_session');

		$v = $this->user->validate_schedule($input, null, Redirect::action('UserController@editSchedules', array($id)));
		if(!($v === true))
			return $v;
		$this->user->save_schedule(User::findOrFail($id),$input);

		return Redirect::action('UserController@editSchedules', array($id))->with('msg_success', 'User added to new schedule.');
	}

	public function updateSchedules($id, $sid){
		$input = array(
			'schedule_id'	=> $sid,
			'start_date' 	=> Input::get('start_date.'.$sid),
			'expiry_date'	=> Input::get('expiry_date.'.$sid),
			'total_session'	=> Input::get('total_session.'.$sid),
			'exam'			=> Input::get('exam.'.$sid),
			'used_exam'		=> Input::get('used_exam.'.$sid),
			'miss_count'	=> Input::get('miss_count.'.$sid),
			'used_session'	=> Input::get('used_session.'.$sid)
		);
		$v = $this->user->validate_schedule($input, $sid, Redirect::action('UserController@editSchedules', array($id)));
		if(!($v === true))
			return $v;
		$this->user->save_schedule(User::findOrFail($id), $input);

		return Redirect::action('UserController@editSchedules', array($id))->with('msg_success', 'Schedule\'s settings updated.');
	}

	public function destroySchedules($id, $sid){
		$user = User::findOrFail($id);
		$user->schedules()->detach($sid);

		return Redirect::action('UserController@editSchedules', array($id))->with('msg_success', 'User\'s schedules deleted.');
	}

	public function editAppointments($id, $sid = null){
		$this->data['id'] = $id;
		$this->data['user'] = User::findOrFail($id);
		if($this->data['user']->schedules->count() > 0){
			if(!isset($sid) || !$this->data['user']->schedules->contains($sid))
				$sid = $this->data['user']->schedules->first()->id;
			$this->data['schedule'] = $this->data['user']->schedules->lists('name', 'id');
			$this->data['appointments'] = $this->data['user']->appointments()->where('schedule_id', $sid)->orderBy('start_time')->paginate(50);
			$this->data['index'] = $this->data['user']->schedules->find($sid)->pivot->used_session;
			$this->data['max_miss'] = $this->data['user']->schedules->find($sid)->pivot->miss_count;
		}else{
			$this->data['appointments'] = array();
			$this->data['schedule'] = array();
		}
		$this->data['sid'] = $sid;

		$this->layout->with($this->data);
		$this->layout->content = View::make('users.appointments', $this->data);
	}

	public function destroyAppointments($id, $aid = null){
		$appointment = Appointment::findOrFail($aid);
		$student = User::findOrFail($id);

		$appointment->user()->detach($student);
		return Redirect::action('UserController@editAppointments', array($id, $appointment->schedule->id))->with('msg_success', 'Student removed from the appointment.');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id) {
		return $this->user->save(Input::all(), Input::file('avatar'), $id);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id) {
		User::destroy($id);

		return Redirect::action('UserController@index')->with('msg_success', 'User deleted successfully.');
	}

}
