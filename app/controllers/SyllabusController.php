<?php

class SyllabusController extends \BaseController {

	protected $layout = "layout";

	protected $active = "course";

	public function __construct(Processes\Course $c) {
		parent::__construct();

		$this->course = $c;
	}

	public function index($courseId) {
		$this->data['courseId'] = $courseId;
		$this->data['course'] = Course::findOrFail($courseId);

		$this->data['tab_count'] = $this->course->get_counts($this->data['course']);
		$this->data['syllabuses'] = $this->course->get_syllabus($this->data['course']);

		$this->layout->with($this->data);
		$this->layout->content = View::make('syllabus.index', $this->data);
	}

	public function exam($courseId) {
		$this->data['is_exam'] = true;
		$this->data['courseId'] = $courseId;
		$this->data['course'] = Course::findOrFail($courseId);

		$this->data['tab_count'] = $this->course->get_counts($this->data['course']);
		$this->data['syllabuses'] = $this->course->get_syllabus($this->data['course'], true);

		$this->layout->with($this->data);
		$this->layout->content = View::make('syllabus.index', $this->data);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store($courseId) {
		return $this->course->save_syllabus(Input::all(), $courseId, Input::file('attachments'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($courseId, $syllabusId) {
		return $this->course->save_syllabus(Input::all(), $courseId, Input::file('attachments'), $syllabusId);
	}

	public function reorder($courseId) {
		return $this->course->reorder_syllabus($courseId, Input::get('order'), Input::has('is_exam'));
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($courseId, $syllabusId) {
		return $this->course->delete_syllabus($syllabusId);
	}

}
