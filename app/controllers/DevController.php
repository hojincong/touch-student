<?php

//use Illuminate\Console\Application as Console;

class DevController extends BaseController{

	public function getArchive()
    {
        Artisan::call('cron:archivestudent');    
    }

	public function getUpdate()
	{
		Artisan::call('migrate', array('--force' => true));
	}

	public function getNewBranch()
	{
		//create admins(remember to set superadmin in config/touchedu.php)
		$user = new User;
		$user->name = 'Admin';
		$user->password = 'qW240abc';
		$user->role = 1;
		$user->remarks = 'Please reserve for programmer to access system.';
		$user->save();

		$user = new User;
		$user->name = 'Enzo';
		$user->password = 'password';
		$user->role = 1;
		$user->save();
	}

	public function getKeepStudent()
	{
		$schedules = Schedule::whereIn('id', array(22,24,25,26))->get();

		foreach($schedules as $s)
		{
			$s->students()->orderBy('name', 'asc')->get()->each(function($user){
				$user->delete();
			});
		}
	}

	public function getLoginAs($id)
	{
		$user = User::find($id);
		Auth::login($user);
	}

	public function getPing()
	{
		return "pong";
	}

	public function getCheckExams($apId){
		$ap = Appointment::findOrFail($apId);
		$st = $ap->user;
		$s = $ap->schedule->students;
		$sid = $ap->schedule->id;
		$course = $ap->schedule->course;
		if(isset($course)){
			$exams = $ap->schedule->course->syllabus()->where('is_exam', '=', true)->orderBy('order')->get();
		}else{
			$exams = array();
		}

		$return = array();
		$log = array();
		$index = -1;

		//each appointment user
		$st->each(function($u) use ($s, $sid, &$return, &$exams, &$ap, &$log, &$index){
			$index++;
			$log[$index]['0_name'] = $u->name." ";
			if(!$s->find($u->id)) return;
			
			$f = $s->find($u->id)->pivot;
			$t = $f->total_session;
			$e = $f->exam;
			$us = $f->used_session;
			$ue = $f->used_exam;
			$m = $f->miss_count;
			$missed = 0;
			$log[$index]['1_total'] = $t;
			$log[$index]['2_exam'] = $e;
			$log[$index]['3_usedsession'] = $us;
			$log[$index]['4_usedexam'] = $ue;
			$log[$index]['5_misscount'] = $m;

			if($e == 0) return;

			//each user's appointments(current appointment will be counted)
			$c = $u->appointments->filter(function($a) use ($sid, &$ap, &$missed, &$m, &$log, &$index){
				if($a->schedule_id != $sid) return false;
				if($a->pivot->is_missed) $missed++;
				$count = $a->pivot->is_missed ? ($missed > $m) : true;

				$filter = ($a->schedule_id == $sid) && ($a->start_time <= $ap->start_time) && ($count || ($ap->id == $a->id));
				$log[$index]['ap'][$a->id] = array(
					($a->schedule_id == $sid),
					($a->start_time <= $ap->start_time),
					$count,
					($ap->id == $a->id)
				);

				return $filter;
			})->count() + $us;
			$log[$index]['6_count'] = $c;

			//second last session is also exam
			//count exam
			//@TODO: error on odd number,  eg: 5/3
			if($e>$t){
				$is_exam = true;
				$times = $c;
				$has_exam = (count($exams) >= $times);
			}else{
				$is_exam = (((($c % floor($t/$e)) == 0) && ($c != $t)) || ($c == ($t-1)));
				$times = ceil($c/floor($t/$e));
				$has_exam = (count($exams) >= $times);
			}
			$log[$index]['7_is_exam'] = (int) $is_exam;
			$log[$index]['8_times'] = $times;
			$log[$index]['9_has_exam'] = (int) $has_exam;
			if($is_exam && $has_exam && ($times>$ue))
				$return[$u->id] = $exams->offsetGet($times-1);
		});
		print_r($log);

		echo 'Results:<br>';
		dd($return);
	}

	public function getTestAppContain($apId, $userId)
	{
		$ap = Appointment::find($apId);

		dd($ap->user->contains($userId));
	}

	public function getIsNextAptExam($uid, $sid)
	{
		$process = new Processes\Appointment;
		dd($process->isNextAptExam($uid, $sid));
	}

}