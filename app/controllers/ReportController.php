<?php

set_time_limit(0);

class ReportController extends BaseController {

	public function __construct(Processes\User $user) {
		parent::__construct();

		$this->user = $user;
	}

	public function getReport($uid, $sid)
	{
		$this->data['user'] 	= User::findOrFail($uid);
		if(!(($this->data['user']->role == 3) && (($uid == $this->data['cu']->id) || ($this->data['cu']->role <= 2))))
			return Redirect::route('users.index');

		if(App::environment() == 'production'){
			$this->data['avatar'] 	= $this->getAvatar($this->data['user']);
		}
		$this->data['schedule'] = Schedule::findOrFail($sid);
		$this->data['appointment'] = $this->data['user']->appointments()->orderBy('start_time')->where('schedule_id', $sid)->get();
		$this->data['info'] 	= $this->data['schedule']->students->find($uid);
		if(!$this->data['info']) return Redirect::action('UserController@index');
		$this->data['info'] 	= $this->data['info']->pivot;

		/*$history = array();
		$order = 1;
		$this->data['appointment']->each(function($a) use (&$history, &$uid, &$order){
			$first = $a->remarks()->where('student_id', $uid)->first();
			$o  = array('order' => $order++, 'appointment_id' => $a->id);
			$history[] = ($first) ? array_merge($first->toArray(), $o) : $o;
		});
		$this->data['remarks'] = $history;*/

		$this->data['remarks_meta'] = $remarks_meta = Config::get('question._meta.versions');
		$this->data['versions'] = $this->user->getRemarksVersion($uid, $sid);
		sort($this->data['versions']);
		$this->data['remarks'] = array();
		$this->data['questions'] = array_only(Config::get('question'), array_merge(array('_meta'), $this->data['versions']));
		foreach($this->data['versions'] as $i=>$version){
			$date = $remarks_meta[$version];

			$before = null;
			if($date != 'default')
				$before = $date." 23:59:59";

			$after = null;
			if($i > 0)
				$after = $remarks_meta[$this->data['versions'][$i-1]];

			$this->data['remarks'][$version] = $this->user->getRemarksAll($uid, $sid, $after, $before);
		}

		return View::make('reports.template', $this->data);
		
		$report = Pdf::loadView('reports.template', $this->data)->setOrientation('landscape')->setPaper('a4');

		return $report->stream();
	}

	protected function getAvatar(&$user)
	{
		$path = Config::get('touchedu.avatar_path').'/'.((isset($user->avatar) && ($user->avatar != '')) ? $user->avatar : '../gravatar.png');
		$orientation = @exif_read_data($path);
		$rotate = (isset($orientation['Orientation']) && in_array($orientation['Orientation'], array('6', '8')));

		return array(base64_encode(file_get_contents($path)), $rotate);
	}

	protected function getQuestions()
	{
		return array(
			'speaking' => array(
				array(
					'question' 	=> 'Did the student speak in class?',
					'type'		=> 'range',
					'model'		=> 's1'
				),
				array(
					'question' 	=> 'Is there fluency in his/her speaking?',
					'type'		=> 'range',
					'model'		=> 's2'
				),
				array(
					'question' 	=> 'Did he/she give relevant points?',
					'type'		=> 'range',
					'model'		=> 's3'
				),
				array(
					'question' 	=> 'Did he/she elaborate the points?',
					'type'		=> 'range',
					'model'		=> 's4'
				),
				array(
					'question' 	=> 'Did he/she participate in class activities?',
					'type'		=> 'range',
					'model'		=> 's5'
				),
				array(
					'question' 	=> 'Did he/she cooperate with other students?',
					'type'		=> 'range',
					'model'		=> 's6'
				),
				array(
					'question' 	=> 'Others (please specify)',
					'type'		=> 'textarea',
					'model'		=> 's7'
				)
			),
			'grammar' => array(
				array(
					'question' 	=> 'Can the student understand teacher\'s explanation?',
					'type'		=> 'range',
					'model'		=> 'g1'
				),
				array(
					'question' 	=> 'Did he/she ask questions when doesn\'t understand?',
					'type'		=> 'range',
					'model'		=> 'g2'
				),
				array(
					'question' 	=> 'Can he/she do the exercise without problem?',
					'type'		=> 'range',
					'model'		=> 'g3'
				),
				array(
					'question' 	=> 'Others (please specify)',
					'type'		=> 'textarea',
					'model'		=> 'g4'
				),
			)
		);
	}

}