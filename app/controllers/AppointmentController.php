<?php

class AppointmentController extends \BaseController {

	protected $layout = "layout";

	protected $active = "appointment";

	/****/
	protected $appointment;

	public function __construct(Processes\Appointment $ap) {
		parent::__construct();

		$this->appointment = $ap;

		$this->beforeFilter('@onlyAdmin', array('except' => array('index', 'getEvents', 'show', 'listIndex', 'listExam', 'edit', 'getStudents', 'addStudent', 'deleteStudent', 'getLesson', 'addLesson', 'deleteLesson', 'chooseAuto', 'getAttend', 'updateAttend', 'getRemarks', 'updateRemarks', 'addEventStudent', 'deleteEventStudent')));
		$this->beforeFilter('@onlyTeacher', array('except' => array('index', 'getEvents', 'show', 'addEventStudent', 'deleteEventStudent')));
		$this->beforeFilter('@onlyActiveStud', array('only' => array('index', 'getEvents', 'show', 'addEventStudent', 'deleteEventStudent')));	
	}

	public function onlyAdmin($route, $request){

		$role = $this->data['cu']->role;

		if($role > 1) return Redirect::action('AppointmentController@index');

	}

	public function onlyTeacher($route, $request){

		$role = $this->data['cu']->role;

		$hasSchedule = count($this->data['cu']->schedules) > 0;

		if(($role == 3) && $hasSchedule) return Redirect::action('AppointmentController@index');		

	}

	public function onlyActiveStud($route, $request){

		$isInactive = $this->data['cu']->is_inactive;

		if($isInactive) return Redirect::action('HomeController@getIndex')->with('msg_error', 'Please contact our centre to register a course before making appointment.');

	}

	public function index() {
		if($this->data['cu']->role <= 1)
			$this->data['schedules'] = Schedule::all()->lists('name', 'id');
		elseif($this->data['cu']->role <= 2)
			$this->data['schedules'] = $this->data['cu']->schedules()->get()->lists('name', 'id');
		else
			$this->data['schedules'] = $this->data['cu']->schedules()->where('expiry_date', '>', date('Y-m-d'))->get()->lists('name', 'id');

		if(count($this->data['schedules']) > 0)
			$this->data['id'] = Input::get('class', array_keys($this->data['schedules'])[0]);
		else
			return Redirect::action('ScheduleController@index')->with('msg_success', 'Create some classes before viewing calendar.');

		$this->layout->with($this->data);
		$this->layout->content = View::make('appointments.calendar', $this->data);
	}

	//ajax for calendar view
	public function getEvents($id = null) {
		$this->layout = null;
		if($this->data['cu']->role <= 1){
			return $this->appointment->getEvents(Input::get('start'), Input::get('end'), $id);
		}elseif($this->data['cu']->role <= 2){
			if($this->data['cu']->schedules->contains($id))
				return $this->appointment->getEvents(Input::get('start'), Input::get('end'), $id);
		}else{
			if($this->data['cu']->schedules->contains($id) && ($this->data['cu']->schedules()->find($id)->expiry_date > date('Y-m-d')))
				return $this->appointment->getEvents(Input::get('start'), Input::get('end'), $id);	
		}
	}

	//ajax for calendar view booking - add
	public function addEventStudent() {
		return $this->appointment->addEventStudent(Input::get('ap_id'));
	}

	//ajax for calendar view booking - delete
	public function deleteEventStudent() {
		return $this->appointment->deleteEventStudent(Input::get('ap_id'));
	}

	public function listIndex($year = null, $month = null, $day = null) {
		$year = isset($year) ? $year : date('Y');
		$month = isset($month) ? $month : date('m');
		$day = isset($day) ? $day : date('d');
		$date = $year . '-' . $month . '-' . $day;
		$this->data['date'] = $year . '/' . $month . '/' . $day;

		if($this->data['cu']->role <= 1){
			$sid = Schedule::lists('id');
			$this->data['appointments'] = Appointment::whereBetween('start_time', array($date . ' 00:00:00', $date . ' 23:59:59'))->whereIn('schedule_id', $sid)->orderBy('start_time')->get();
		}else{
			$sid = $this->data['cu']->schedules()->get()->lists('id');
			$this->data['appointments'] = Appointment::whereBetween('start_time', array($date . ' 00:00:00', $date . ' 23:59:59'))->orderBy('start_time');
			if(count($sid))
				$this->data['appointments'] = $this->data['appointments']->whereIn('schedule_id', $sid)->get();
			else
				$this->data['appointments'] = $this->data['appointments']->get();
		}

		$this->layout->with($this->data);
		$this->layout->content = View::make('appointments.index', $this->data);
	}

	public function listExam($year = null, $month = null){
		$year = ltrim(isset($year) ? $year : date('Y'), '0');
		$month = ltrim(isset($month) ? $month : date('m'), '0');
		$this->data['year'] = $year;
		$this->data['month'] = $month;

		$sid = Schedule::lists('id');
		$this->data['appointments'] = Appointment::whereRaw('MONTH(start_time) = '.$month)->whereRaw('YEAR(start_time) = '.$year)->whereIn('schedule_id', $sid)->orderBy('start_time', 'desc')->get();
		$this->data['appointments'] = $this->data['appointments']->filter(function($item){

			$item->user = $item->user->filter(function($u) use (&$item){
				//apt must be exam
				//user must be in that schedules(remained record of exam of removed student will not be shown)
				//user must attend class
				$uid = $u->id;
				return (($u->pivot->is_exam == 1) && ($u->schedules->contains($item->schedule->id)) && (!$u->pivot->is_missed));
			});

			return (count($item->user) > 0);
		});

		$this->data['processor'] = $this->appointment;

		$this->layout->with($this->data);
		$this->layout->content = View::make('appointments.exam', $this->data);
	}

	public function create() {
		$this->data['schedule'] = Schedule::with('course', 'course.syllabus')->get();
		$this->data['schedule_list'] = $this->data['schedule']->lists('name', 'id');

		$this->layout->with($this->data);
		$this->layout->content = View::make('appointments.form', $this->data);
	}

	public function store() {
		$input = Input::only('schedule_id', 'classname', 'seats', 'start_time', 'end_time', 'notes', 'notes2', 'is_repeating', 'end_date');
		$st_before = Input::has('end_time') ? '|before:'.Input::get('end_time') : '';
		$et_after = Input::has('start_time') ? '|after:'.Input::get('start_time') : '';
		$validation = Validator::make($input, array(
			'schedule_id' => 'required|exists:schedules,id',
			'start_time' => 'required|date_format:Y-m-d H:i'.$st_before,
			'end_time' => 'required|date_format:Y-m-d H:i'.$et_after,
			'seats' => 'required|integer',
			'end_date'	=> 'required_with:is_repeating|date_format:Y-m-d'.$et_after
		));

		if ($validation->fails()) {
			return Redirect::action('AppointmentController@create')->withInput($input)->withErrors($validation)->with('msg_error', 'There are some errors in your form.');
		} else {
			$info = Input::only('schedule_id', 'classname', 'seats', 'start_time', 'end_time', 'notes', 'notes2');

			//repeating event
			if(Input::has('is_repeating')){
				$apt = $this->gen_appointments($info, $input['start_time'], $input['end_time'], $input['end_date']);
				
				DB::table('appointments')->insert($apt);

				return Redirect::action('AppointmentController@create')->with('msg_success', 'You\'ve created '.count($apt).' new appointments, the first one starts on '.$apt[0]['start_time'].'. View them in calendar.');
			}else{
				$info['is_auto'] = true;
				$appointment = Appointment::create($info);

				return Redirect::action('AppointmentController@create')->with('msg_success', 'You\'ve created a new appointment. View it\'s info <a href="' . action('AppointmentController@edit', array($appointment->id)) . '">here</a>.');
			}
		}
	}

	private function gen_appointments($input, $start, $end, $until){

		$dates = $this->gen_recurrence($start, $end, $until);
		$now = date('Y-m-d H:i:s');

		$results = array();
		foreach ($dates as $value) {
			$results[] = array_merge($input, array(
				'start_time'	=> $value[0],
				'end_time'		=> $value[1],
				'is_auto'		=> true,
				'created_at'	=> $now,
				'updated_at'	=> $now
			));
		}

		return $results;

	}

	private function gen_recurrence($start, $end, $until){

		$to = strtotime($until);
		$results = array();
		$c = 0;

		do{

			$results[] =  array($start, $end);
			$start = date('Y-m-d H:i', strtotime($start . " +1 week"));
			$end = date('Y-m-d H:i', strtotime($end . " +1 week"));
		    $day = strtotime(substr($start, 0, 10));

		}while($day <= $to);

		return $results;
	}

	public function show($id) {
		$this->data['id'] = $id;
		$this->data['is_win'] = stripos($_SERVER['HTTP_USER_AGENT'], 'win');
		$this->data['appointment'] = Appointment::findOrFail($id);
		if(count($this->data['appointment']->syllabus) > 0)
			$this->data['lessons'] = Syllabus::whereIn('id', $this->data['appointment']->syllabus)->get();
		$this->data['users'] = $this->data['appointment']->user;

		$this->layout->with($this->data);
		$this->layout->content = View::make('appointments.show', $this->data);
	}

	public function edit($id) {
		$this->data['id'] = $id;
		$this->data['appointment'] = Appointment::findOrFail($id);
		$this->data['schedule'] = Schedule::with('course', 'course.syllabus')->get();
		$this->data['schedule_list'] = $this->data['schedule']->lists('name', 'id');

		$this->layout->with($this->data);
		$this->layout->content = View::make('appointments.form', $this->data);
	}

	public function update($id) {
		$input = Input::only('schedule_id', 'classname', 'seats', 'start_time', 'end_time', 'notes', 'notes2');
		$validation = Validator::make($input, array(
			'schedule_id' => 'required|exists:schedules,id',
			'start_time' => 'required|date_format:Y-m-d H:i|before:' . $input['end_time'],
			'end_time' => 'required|date_format:Y-m-d H:i|after:' . $input['start_time'],
			'seats' => 'required|integer',
		));

		if ($validation->fails()) {
			return Redirect::action('AppointmentController@edit', array($id))->withInput($input)->withErrors($validation)->with('msg_error', 'There are some errors in your form.');
		} else {
			$appointment = Appointment::find($id);
			$appointment->update($input);

			return Redirect::action('AppointmentController@edit', array($id))->with('msg_success', 'You\'ve updated appointment successfully.');
		}
	}

	public function getStudents($id) {
		$this->data['id'] = $id;
		$this->data['appointment'] = Appointment::findOrFail($id);
		$this->data['student'] = $this->data['appointment']->user;
		$this->data['schedule_student'] = $this->appointment->getStudents($id);
		$this->data['exam'] = count($this->appointment->checkExams($id));

		$this->layout->with($this->data);
		$this->layout->content = View::make('appointments.students', $this->data);
	}

	public function addStudent($id) {
		return $this->appointment->addStudent($id, Input::get('user_id'));
	}

	public function deleteStudent($id) {
		return $this->appointment->deleteStudent($id, Input::get('user_id'));
	}

	public function getLesson($id) {
		$this->data['id'] = $id;
		$appointment = $this->data['appointment'] = Appointment::with('user', 'user.appointments', 'schedule', 'schedule.course')->findOrFail($id);
		if(isset($appointment->schedule->course)){
			$this->data['lessons'] = $appointment->schedule->course->syllabus()->where('is_exam', false)->orderBy('order')->get();
			$this->data['history'] = $this->appointment->getLessons($id);
		}else{
			$this->data['lessons'] = array();
			$this->data['history'] = array();
		}
		$this->data['student'] = $appointment->user;
		$this->data['exams'] = $this->appointment->checkExams($id);

		$this->layout->with($this->data);
		$this->layout->content = View::make('appointments.lesson', $this->data);
	}

	public function addLesson($id){
		if (Input::has('lesson_id')) 
			$this->appointment->updateLesson($id, Input::get('lesson_id'));

		return Redirect::action('AppointmentController@getLesson', array($id))->with('msg_success', 'Update lesson plan successfully.');
	}

	public function deleteLesson($id){
		$this->appointment->updateLesson($id);

		return Redirect::action('AppointmentController@getLesson', array($id))->with('msg_success', 'Lesson plan removed successfully.');
	}

	public function chooseAuto($id){
		//$this->appointment->toggleAuto($id, true);
		$this->appointment->autoSyllabus($id);

		return Redirect::action('AppointmentController@getLesson', array($id))->with('msg_success', 'Update lesson plan successfully.');	
	}

	public function toggleAuto($id){
		$this->appointment->toggleAuto($id, Input::get('is_auto'));
		/*if(Input::get('is_auto'))
			$this->appointment->autoSyllabus($id);*/

		return Redirect::action('AppointmentController@getLesson', array($id))->with('msg_success', Input::get('is_auto') ? 'Lesson plan is set to auto mode.' : 'Lesson plan is set to manual mode.');	
	}

	public function getAttend($id) {
		$this->data['id'] = $id;
		$this->data['appointment'] = Appointment::with('user', 'attendance')->findOrFail($id);
		$this->data['student'] = $this->data['appointment']->user;
		$this->data['attendance'] = $this->data['appointment']->attendance;
		$this->data['is_updated'] = (count($this->data['attendance']) >= count($this->data['appointment']->user));

		$this->layout->with($this->data);
		$this->layout->content = View::make('appointments.attendance', $this->data);
	}

	public function updateAttend($id){
		$this->appointment->updateAttend($id, Input::get('attend'));

		return Redirect::action('AppointmentController@getAttend', array($id))->with('msg_success', 'Attendance record updated successfully.');	
	}

	public function getRemarks($id){
		$this->data['id'] = $id;
		$this->data['appointment'] = Appointment::with('user', 'remarks')->findOrFail($id);
		$this->data['student'] = $this->data['appointment']->user;
		$this->data['records'] = $this->data['appointment']->remarks;
		$version = $this->appointment->chooseVersion(Config::get('question._meta.versions'), $this->data['appointment']->start_time);
		$this->data['questions'] = Config::get('question.'.$version);
		//only show exam marking label if has student in exam
		$this->data['has_exam'] = $this->data['student']->filter(function($u){
			return $u->pivot->is_exam == 1;
		})->count(); 

		$this->layout->with($this->data);
		$this->layout->content = View::make('appointments.remarks', $this->data);
	}

	public function updateRemarks($id){
		return $this->appointment->updateRemarks($id, Input::all());
	}

	public function destroy($id) {
		Appointment::findOrFail($id)->delete();

		return Redirect::action('AppointmentController@listIndex')->with('msg_success', 'Appointment deleted successfully.');
	}

}