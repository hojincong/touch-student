<?php namespace Setting;

use View;
use Input;
use Validator;
use Redirect;
use Notification;
use User;
use Setting\Notification as SNotification;

class NotificationController extends \BaseController {

	protected $layout = "layout";

	protected $active = 'setting';

	protected $target = array(
		'all'	=> 'All User'
	);

	protected $targetRule = 'all';

	public function __construct(){
		parent::__construct();

		$this->data['target'] = $this->target;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index() {
		$this->data['notifications'] = SNotification::all();

		$this->layout->with($this->data);
		$this->layout->content = \View::make('settings.notifications.index', $this->data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create() {
		$this->layout->with($this->data);
		$this->layout->content = \View::make('settings.notifications.create', $this->data);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store() {
		$input = Input::only('target', 'message');
		$validation = Validator::make($input, array(
			'message'	=> 'required',
			'target'	=> 'required|in:'.$this->targetRule
		));

		if($validation->fails()){
			return Redirect::action('Setting\\NotificationController@create')->withInput($input)->withErrors($validation)->with('msg_error', 'There are some errors in your form.');
		}else{
			$n = SNotification::create($input);

			if($input['target'] == 'all'){
				$users = User::all();
				$users->each(function($u) use ($n){
					Notification::create(array(
						'user_id' 	=> $u->id,
						'type'		=> 'info',
						'observer'	=> 'SettingNotification',
						'observer_id' => $n->id,
						'message'	=> $n->message
					));
				});
			}

			return Redirect::action('Setting\\NotificationController@create')->with('msg_success', 'You\'ve sent a new notification. View it\'s info <a href="' . action('Setting\\NotificationController@edit', array($n->id)) . '">here</a>.');
		}
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id) {
		$this->data['id'] = $id;
		$this->data['notification'] = SNotification::findOrFail($id);

		$this->layout->with($this->data);
		$this->layout->content = View::make('settings.notifications.create', $this->data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id) {
		$input = Input::only('target', 'message');
		$validation = Validator::make($input, array(
			'message'	=> 'required',
			'target'	=> 'required|in:'.$this->targetRule
		));

		if($validation->fails()){
			return Redirect::action('Setting\\NotificationController@create')->withInput($input)->withErrors($validation)->with('msg_error', 'There are some errors in your form.');
		}else{
			$n = SNotification::findOrFail($id);
			$n->update($input);

			//@TODO: precautious for changing target
			if($input['target'] == 'all'){
				$notifications = Notification::where('observer', 'SettingNotification')->where('observer_id', $n->id)->update(array('message' => $n->message));
			}

			return Redirect::action('Setting\\NotificationController@edit', array($n->id))->with('msg_success', 'Notification updated successfully.');
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id) {
		$notifications = Notification::where('observer', 'SettingNotification')->where('observer_id', $id)->delete();
		SNotification::destroy($id);

		return Redirect::action('Setting\\NotificationController@index')->with('msg_success', 'Notification deleted successfully.');
	}

}
