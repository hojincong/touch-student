<?php

class CourseController extends \BaseController {

	protected $layout = "layout";

	protected $active = 'course';

	public function __construct(Processes\Course $c) {
		parent::__construct();

		$this->course = $c;

		$this->beforeFilter('@onlyAdmin', array('except' => array('index', 'show', 'exam')));
	}

	public function onlyAdmin($route, $request){
		$role = $this->data['cu']->role;

		if($role > 1) return Redirect::action('CourseController@index');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index() {
		if($this->data['cu']->role >= 2)
			$this->data['courses'] = $this->course->getUserCourse($this->data['cu']->id);
		else
			$this->data['courses'] = Course::all();

		$this->layout->with($this->data);
		$this->layout->content = View::make('courses.index', $this->data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create() {
		$this->layout->with($this->data);
		$this->layout->content = View::make('courses.form', $this->data);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store() {
		return $this->course->save(Input::only('name', 'description'));
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id) {
		$this->data['course'] = Course::findOrFail($id);
		$this->data['is_win'] = stripos($_SERVER['HTTP_USER_AGENT'], 'win');
		$this->data['tabs'] = "lessons";

		if($this->data['cu']->role == 3){
			$this->data['syllabuses'] = $this->course->get_user_syllabus($this->data['cu']->id, $id);
			$recent = array();
			$this->data['cu']->appointments()->where('end_time', '>', date('Y-m-d', time() - 60*60*72))->get()->each(function($a) use (&$recent){
				if(!$a->pivot->is_missed && !$a->pivot->is_exam) $recent[] = $a->pivot->syllabus_id;
			});
			$this->data['recent'] = $recent;
		}else{
			$this->data['syllabuses'] = $this->course->get_syllabus($this->data['course']);
			$this->data['tab_count'] = $this->course->get_counts($this->data['course']);
		}

		$this->layout->with($this->data);
		$this->layout->content = View::make('courses.show', $this->data);
	}

	public function exam($id){
		$this->data['course'] = Course::findOrFail($id);
		$this->data['is_win'] = stripos($_SERVER['HTTP_USER_AGENT'], 'win');
		$this->data['tabs'] = "exam";

		if($this->data['cu']->role == 3){
			$this->data['syllabuses'] = $this->course->get_user_syllabus($this->data['cu']->id, $id, true);

			$recent = array();
			$this->data['cu']->appointments()->where('end_time', '>', date('Y-m-d', time() - 60*60*72))->get()->each(function($a) use (&$recent){
				if(!$a->pivot->is_missed && $a->pivot->is_exam) $recent[] = $a->pivot->syllabus_id;
			});
			$this->data['recent'] = $recent;
		}else{
			$this->data['syllabuses'] = $this->course->get_syllabus($this->data['course'], true);
			$this->data['tab_count'] = $this->course->get_counts($this->data['course']);
		}

		$this->layout->with($this->data);
		$this->layout->content = View::make('courses.show', $this->data);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id) {
		$this->data['id'] = $id;
		$this->data['course'] = Course::findOrFail($id);

		$this->data['tab_count'] = $this->course->get_counts($this->data['course']);

		$this->layout->with($this->data);
		$this->layout->content = View::make('courses.form', $this->data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id) {
		return $this->course->save(Input::only('name', 'description'), $id);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id) {
		return $this->course->delete($id);
	}

}
