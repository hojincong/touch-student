<?php

class BaseController extends Controller {

	protected $data = array();

	public function __construct() {
		if (Auth::check()) {
			$this->data['count'] = array(
				'user' 		=> User::count(),
				'course' 	=> Course::count(),
				'syllabus' 	=> Syllabus::count(),
				'schedule' 	=> Schedule::count(),
				'dashboard'	=> Auth::user()->notifications()->where('is_read', false)->count()
			);

			$this->data['cu'] = Auth::user();
			if(in_array($this->data['cu']->id, Config::get('touchedu.super_admin')))
				$this->data['cu']->role = 0;
		}

		if (isset($this->active)) {
			$this->data['active'] = $this->active;
		}

		$this->data['assets_version'] = 2;

		$this->data['settings'] = Setting::lists('value', 'key');
	}

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout() {
		if (!is_null($this->layout)) {
			$this->layout = View::make($this->layout);
		}
	}

	protected function checkPermission($permission, $action) {
		return !Auth::user()->acl($permission)->where('permission', $permission)->first();
	}

}
