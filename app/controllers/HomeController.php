<?php

class HomeController extends BaseController {

	protected $layout = 'layout';

	protected $active = "dashboard";

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function getIndex()
	{
		$this->data['notifications'] = $this->data['cu']->notifications()->orderBy('created_at', 'desc')->get();

		$this->layout->with($this->data);
		$this->layout->content = View::make('dashboard.notifications', $this->data);
	}

	public function postMarkAllRead()
	{
		$notifications = $this->data['cu']->notifications()->where('is_read', false)->update(array('is_read' => true));

		return Redirect::action('HomeController@getIndex')->with('msg_success', 'Marked all notification as read.');
	}

}
