<?php

class AuthController extends BaseController {

	protected $layout = 'layout';

	public function __construct(){
		parent::__construct();

		$this->beforeFilter('auth', array('only' => array('getLogout', 'getProfile')));
		$this->beforeFilter('guest', array('only' => array('getLogin', 'postLogin')));
	}

	public function getProfile(){
		$this->data['user'] = User::findOrFail(Auth::id());
		$this->data['user']['password'] = '';
		$this->data['active'] = 'user';

		$this->layout->with($this->data);
		$this->layout->content = View::make('users.form', $this->data);
	}

	public function getLogin() {
		return View::make('auth.login');
	}

	public function postLogin() {
		if(Auth::attempt(array('name' => ucwords(trim(Input::get('name'))), 'password' => Input::get('password')), true)){
			//return Redirect::action('UserController@index');
			return Redirect::action('HomeController@getIndex');
		}else{
			return Redirect::action('AuthController@getLogin')->withInput()->with('msg_error', 'Username and password not match.');
		}
	}

	public function getLogout() {
		Auth::logout();
		return Redirect::action('AuthController@getLogin');
	}

}