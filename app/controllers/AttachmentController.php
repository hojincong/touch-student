<?php

class AttachmentController extends \BaseController {

	public function show($id) {
		$attachment = Attachment::findOrFail($id);
		return Response::download(Config::get('touchedu.attachment_path') . '/' . $attachment->uniqid, $attachment->name, array(
			'Content-Type' => $attachment->mime,
			'Content-Length' => $attachment->size,
		));
	}

	public function destroy($id) {
		$attachment = Attachment::findOrFail($id);
		$name = $attachment->name;
		$exam = $attachment->syllabus->is_exam;
		$syllabusId = $attachment->syllabus->id;
		$course = $attachment->syllabus->course->id;
		$attachment->delete();

		return Redirect::action(($exam == '1') ? 'SyllabusController@exam' : 'SyllabusController@index', array($course))->with('msg_success', 'Attachment "' . $name . '" deleted successfully.')->with('syllabusId', $syllabusId);
	}

	public function destroyUrl($id) {
		$attachurl = Attachurl::findOrFail($id);
		$url = $attachurl->url;
		$exam = $attachurl->syllabus->is_exam;
		$syllabusId = $attachurl->syllabus->id;
		$course = $attachurl->syllabus->course->id;
		$attachurl->delete();

		return Redirect::action(($exam == '1') ? 'SyllabusController@exam' : 'SyllabusController@index', array($course))->with('msg_success', 'Attached URL "' . $url . '" deleted successfully.')->with('syllabusId', $syllabusId);
	}

}