<?php

class PsychologyTestController extends \BaseController{

	protected $layout = 'psychologytest.layout';

	public function __construct() {
		parent::__construct();

		$this->beforeFilter('@onlyAdmin');
	}

	public function onlyAdmin($route, $request){

		$role = $this->data['cu']->role;

		if($role > 1) return Redirect::action('AppointmentController@index');

	}

	public function getIndex(){
		if(!Session::has('mbti'))
			Session::put('mbti', true);

		if(Session::has('psychotest')){
			$pt = Session::get('psychotest');

			if($pt<47){
				$this->data['id'] = $pt;

				$json = json_decode(file_get_contents(dirname(__FILE__).'/../storage/files/mbti/questions.json'));
				$this->data['question'] = $json[$pt]->question;
				$this->data['choices'] = $json[$pt]->choice;

				$this->layout->with($this->data);
				$this->layout->content = View::make('psychologytest.question', $this->data);
			}elseif($pt==47){
				/*Session::put('ptanswer', array('E', 'I', 'S', 'N', 'F', 'T', 'J', 'P', 'E', 'E', 'S', 'S', 'F', 'F', 'P', 'P'));*/
				$this->data['result'] = $this->getResult(Session::get('ptanswer'));
				$this->data['type'] = $this->getType($this->data['result']);
				$this->data['percentage'] = $this->getPercentage($this->data['result']);

				$explain = json_decode(file_get_contents(dirname(__FILE__).'/../storage/files/mbti/types.json'));
				$this->data['explain'] = $explain->{$this->data['type']};

				$this->data['different'] = $this->getDifferent($this->data['result']);
				$this->data['labels'] = $this->getLabels($this->data['result']);
				$this->data['series'] = $this->getSeries($this->data['result']);

				//dd($this->data['type']);

				$this->layout->with($this->data);
				$this->layout->content = View::make('psychologytest.result', $this->data);
			}else{
				Session::forget('psychotest');
				Session::forget('ptanswer');

				return Redirect::action('PsychologyTestController@getIndex');
			}
		}else{
			$this->layout->with($this->data);
			$this->layout->content = View::make('psychologytest.start', $this->data);
		}
	}

	protected function getResult($answers){
		$result = array_count_values($answers);

		if(!isset($result['E'])) $result['E'] = 0;
		if(!isset($result['I'])) $result['I'] = 0;
		if(!isset($result['S'])) $result['S'] = 0;
		if(!isset($result['N'])) $result['N'] = 0;
		if(!isset($result['F'])) $result['F'] = 0;
		if(!isset($result['T'])) $result['T'] = 0;
		if(!isset($result['J'])) $result['J'] = 0;
		if(!isset($result['P'])) $result['P'] = 0;

		return $result;
	}

	protected function getType($results){
		$type = '';

		if($results['E'] > $results['I']){
			$type .= 'E';
		}else{
			$type .= 'I';
		}

		if($results['S'] > $results['N']){
			$type .= 'S';
		}else{
			$type .= 'N';
		}

		if($results['F'] > $results['T']){
			$type .= 'F';
		}else{
			$type .= 'T';
		}

		if($results['J'] > $results['P']){
			$type .= 'J';
		}else{
			$type .= 'P';
		}

		return $type;		
	}

	protected function getLabels($results){

		$labels = array('I内向(Introversion)', 'S感觉(Sensing)', 'T思考(Thinking)', 'J判断(Judging)', 'E外向(Extraversion)', 'N直觉(iNtuition)', 'F情感(Feeling)', 'P感知(Perceiving)');

		if($results['E'] > $results['I']){
			$labels[4] = '<span style="color: red; font-weight: 900">'.$labels[4].'</span>';
		}else{
			$labels[0] = '<span style="color: red; font-weight: 900">'.$labels[0].'</span>';
		}

		if($results['S'] > $results['N']){
			$labels[1] = '<span style="color: red; font-weight: 900">'.$labels[1].'</span>';
		}else{
			$labels[5] = '<span style="color: red; font-weight: 900">'.$labels[5].'</span>';
		}

		if($results['F'] > $results['T']){
			$labels[6] = '<span style="color: red; font-weight: 900">'.$labels[6].'</span>';
		}else{
			$labels[2] = '<span style="color: red; font-weight: 900">'.$labels[2].'</span>';
		}

		if($results['J'] > $results['P']){
			$labels[3] = '<span style="color: red; font-weight: 900">'.$labels[3].'</span>';
		}else{
			$labels[7] = '<span style="color: red; font-weight: 900">'.$labels[7].'</span>';
		}

		return $labels;

	}

	protected function getSeries($results){
		$p = $this->getPercentage($results);

		foreach ($p as $k=>$v) {
			$p[$k] = array('y' => $v);

			if($v>=50){
				$p[$k]['marker'] = array(
					'radius' => 6,
					'fillColor' => '#f00',
					'states'	=> array(
						'hover' => array(
							'radius' => 6,
							'fillColor' => '#f00'
						)
					)
				);
			}
		}

		return json_encode($p);
	}

	protected function getPercentage($results){
		$p = array();

		$p[] = 100*($results['I']/($results['E']+$results['I']));
		$p[] = 100*($results['S']/($results['S']+$results['N']));
		$p[] = 100*($results['T']/($results['F']+$results['T']));
		$p[] = 100*($results['J']/($results['J']+$results['P']));
		$p[] = 100-$p[0];
		$p[] = 100-$p[1];
		$p[] = 100-$p[2];
		$p[] = 100-$p[3];

		return $p;
	}

	protected function getDifferent($results){

		$json = json_decode(file_get_contents(dirname(__FILE__).'/../storage/files/mbti/different.json'));

		$d = '';

		$d .= '<h3>内向I还是外向E - 你把注意力放在哪里,从哪里获得能量?</h3>';
		if($results['E'] > $results['I']){
			$d .= '<p>你是 '.$json->E.'<br>和你不同的人是 '.$json->I.'</p>';
		}else{
			$d .= '<p>你是 '.$json->I.'<br>和你不同的人是 '.$json->E.'</p>';
		}

		$d .= '<h3>感觉S还是直觉N - 在接受和感知周围世界的信息时，你更注意什么？</h3>';
		if($results['S'] > $results['N']){
			$d .= '<p>你是 '.$json->S.'<br>和你不同的人是 '.$json->N.'</p>';
		}else{
			$d .= '<p>你是 '.$json->N.'<br>和你不同的人是 '.$json->S.'</p>';
		}

		$d .= '<h3>思考T还是情感F - 你是如何下判断，作决定的？</h3>';
		if($results['T'] > $results['F']){
			$d .= '<p>你是 '.$json->T.'<br>和你不同的人是 '.$json->F.'</p>';
		}else{
			$d .= '<p>你是 '.$json->F.'<br>和你不同的人是 '.$json->T.'</p>';
		}

		$d .= '<h3>判断J还是感知P - 你怎么组织和规划生活？</h3>';
		if($results['J'] > $results['P']){
			$d .= '<p>你是 '.$json->J.'<br>和你不同的人是 '.$json->P.'</p>';
		}else{
			$d .= '<p>你是 '.$json->P.'<br>和你不同的人是 '.$json->J.'</p>';
		}

		return $d;

	}

	public function getStart(){
		Session::put('psychotest', 0);
		Session::forget('ptanswer');

		return Redirect::action('PsychologyTestController@getIndex');
	}

	public function getRestart(){
		Session::forget('psychotest');
		Session::forget('ptanswer');

		return Redirect::action('PsychologyTestController@getIndex');
	}

	public function postAnswer(){
		$pt = Session::get('psychotest');

		if(Session::has('psychotest') && ($pt<47)){
			Session::push('ptanswer', Input::get('type'));
			Session::put('psychotest', $pt+1);
		}

		return Redirect::action('PsychologyTestController@getIndex');

	}

}