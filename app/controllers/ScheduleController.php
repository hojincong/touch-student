<?php

class ScheduleController extends \BaseController {

	protected $layout = "layout";

	public function __construct() {
		parent::__construct();
		$this->data['active'] = 'schedule';

		$this->beforeFilter('@onlyAdmin', array('except' => array('index', 'editStudents')));
	}

	public function onlyAdmin($route, $request){
		$role = $this->data['cu']->role;

		if($role > 1) return Redirect::action('ScheduleController@index');
	}

	public function index() {
		if($this->data['cu']->role <= 1)
			$this->data['schedules'] = Schedule::all()->load('course');
		else 
			$this->data['schedules'] = $this->data['cu']->schedules->load('course');

		$this->layout->with($this->data);
		$this->layout->content = View::make('schedule.index', $this->data);
	}

	public function create() {
		$this->data['course'] = Course::lists('name', 'id');
		$this->data['tutors'] = User::whereIn('role', array(1,2))->lists('name', 'id');
		$this->data['tutors'][-1] = "None";
		ksort($this->data['tutors']);
		$this->data['days'] = array(0 => 'Sun', 1 => 'Mon', 2 => 'Tues', 3 => 'Wed', 4 => 'Thurs', 5 => 'Fri', 6 => 'Sat');

		$this->layout->with($this->data);
		$this->layout->content = View::make('schedule.form', $this->data);
	}

	public function store() {
		$courses = implode(Course::lists('id'), ',');
		$rules = array('course' => 'required|in:'.$courses);

		$tutors = array();
		foreach(Input::get('tutors') as $key=>$value){
			if($value > 0) {
				$rules['tutors.'.$key] = 'exists:users,id';
				$tutors[] = $value;
			}
		}
		$validation = Validator::make(Input::all(), $rules);

		if ($validation->fails()) {
			return Redirect::action('ScheduleController@create')->withInput(Input::except('day', 'shour', 'smin', 'ehour', 'emin'))->withErrors($validation)->with('msg_error', 'There are some errors in your form.');
		} else {
			$day = Input::get('day');
			$times = array();
			if (is_array($day)) {
				foreach ($day as $key => $value) {
					$input = array(
						'day' => Input::get('day.' . $key),
						'shour' => Input::get('shour.' . $key),
						'smin' => Input::get('smin.' . $key),
						'ehour' => Input::get('ehour.' . $key),
						'emin' => Input::get('emin.' . $key),
					);
					$validate = Validator::make($input, array(
						'day' => 'required_with:shour,smin,ehour,emin',
						'shour' => 'required_with:day,smin,ehour,emin',
						'smin' => 'required_with:day,shour,ehour,emin',
						'ehour' => 'required_with:day,shour,smin,emin',
						'emin' => 'required_with:day,shour,smin,ehour',
					));

					if ($validate->fails()) {
						return Redirect::action('ScheduleController@create')->withInput(Input::except('day', 'shour', 'smin', 'ehour', 'emin'))->withErrors($validation)->with('msg_error', 'There are some errors in your form.');
					} else {
						$times[] = ScheduleTime::create($input); //@TODO: order?
					}
				}
			}

			$schedule = Schedule::create(array('course_id' => Input::get('course')));
			DB::transaction(function () use ($times, $schedule, $tutors) {
				$schedule->tutors()->sync($tutors);
			});

			return Redirect::action('ScheduleController@create')->with('msg_success', 'You\'ve created a new schedule. View it\'s info <a href="' . action('ScheduleController@edit', array($schedule->id)) . '">here</a>.');
		}
	}

	public function show($id) {

	}

	public function edit($id) {
		$this->data['schedule'] = Schedule::findOrFail($id)->load('course');
		$this->data['tabs'] = array('students' => $this->data['schedule']->students->count());
		$this->data['course'] = Course::lists('name', 'id');
		$this->data['tutors'] = User::whereIn('role', array(1,2))->lists('name', 'id');
		$this->data['tutors'][-1] = "None";
		ksort($this->data['tutors']);
		$this->data['days'] = array(0 => 'Sun', 1 => 'Mon', 2 => 'Tues', 3 => 'Wed', 4 => 'Thurs', 5 => 'Fri', 6 => 'Sat');

		$this->layout->with($this->data);
		$this->layout->content = View::make('schedule.form', $this->data);
	}

	public function editStudents($id){
		$this->data['schedule'] = Schedule::findOrFail($id);
		$this->data['tabs'] = array('students' => $this->data['schedule']->students->count());
		$this->data['users'] = $this->data['schedule']->students()->orderBy('name', 'asc')->paginate(50);

		$this->layout->with($this->data);
		$this->layout->content = View::make('schedule.student', $this->data);	
	}

	public function update($id) {
		$courses = implode(Course::lists('id'), ',');
		$rules = array('course' => 'required|in:'.$courses);
		$tutors = array();
		foreach(Input::get('tutors') as $key=>$value){
			if($value > 0) {
				$rules['tutors.'.$key] = 'exists:users,id';
				$tutors[] = $value;
			}
		}
		$validation = Validator::make(Input::all(), $rules);

		if ($validation->fails()) {
			return Redirect::action('ScheduleController@edit', array($id))->withInput(Input::except('day', 'shour', 'smin', 'ehour', 'emin'))->withErrors($validation)->with('msg_error', 'There are some errors in your form.');
		} else {
			$day = Input::get('day');
			$times = array();
			if (is_array($day)) {
				foreach ($day as $key => $value) {
					$input = array(
						'day' => Input::get('day.' . $key),
						'shour' => Input::get('shour.' . $key),
						'smin' => Input::get('smin.' . $key),
						'ehour' => Input::get('ehour.' . $key),
						'emin' => Input::get('emin.' . $key),
					);
					$validate = Validator::make($input, array(
						'day' => 'required_with:shour,smin,ehour,emin',
						'shour' => 'required_with:day,smin,ehour,emin',
						'smin' => 'required_with:day,shour,ehour,emin',
						'ehour' => 'required_with:day,shour,smin,emin',
						'emin' => 'required_with:day,shour,smin,ehour',
					));

					if ($validate->fails()) {
						return Redirect::action('ScheduleController@edit', array($id))->withInput(Input::except('day', 'shour', 'smin', 'ehour', 'emin'))->withErrors($validation)->with('msg_error', 'There are some errors in your form.');
					} else {
						$times[] = ScheduleTime::create($input);
					}
				}
			}

			$schedule = Schedule::findOrFail($id);
			$schedule->course_id = Input::get('course');
			$schedule->save();
			DB::transaction(function () use ($times, $schedule, $tutors) {
				//$schedule->tutors()->detach();
				//$schedule->tutors()->attach(array(Input::get('tutors')));
				//$schedule->times()->delete();
				//$schedule->times()->saveMany($times);
				$schedule->tutors()->sync($tutors);
			});

			return Redirect::action('ScheduleController@edit', array($id))->with('msg_success', 'You\'ve updated schedule successfully.');
		}
	}

	public function destroy($id) {
		Schedule::findOrFail($id)->delete();

		return Redirect::action('ScheduleController@index')->with('msg_success', 'Schedule deleted successfully.');
	}

}