<?php

class Acl extends Eloquent {

	protected $table = 'acl';

	protected $timestamps = false;

	protected $fillable = array('permission', 'value', 'role_id');

}