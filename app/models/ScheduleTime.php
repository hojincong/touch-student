<?php

class ScheduleTime extends Eloquent{

	use SoftDeletingTrait;

	protected $table = 'schedules_times';

	protected $fillable = array('day', 'shour', 'smin', 'ehour', 'emin');

	protected $dates = array('deleted_at');

	public function schedule(){
		return $this->belongsTo('Schedule', 'schedule_id', 'id');
	}

}