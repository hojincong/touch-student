<?php namespace Setting;

class Notification extends \Eloquent{

	use \SoftDeletingTrait;

	protected $table = 'settings_notifications';

	protected $fillable = array('message', 'link', 'type', 'target', 'target_id1', 'target_id2', 'target_id3');

	protected $dates = array('deleted_at');

	protected $appends = array('read');

	public function sent(){
		return $this->hasMany('Notification', 'observer_id', 'id')->where('observer', 'SettingNotification');
	}

	public function getReadAttribute(){
		return $this->sent()->where('is_read', true)->count()."/".$this->sent()->count();
	}

}