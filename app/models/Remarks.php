<?php

class Remarks extends Eloquent {

	use SoftDeletingTrait;

	protected $table = 'remarks';

	protected $fillable = array('s1', 's2', 's3', 's4', 's5', 's6', 's7', 'g1', 'g2', 'g3', 'g4', 'student_id', 'appointment_id');

	protected $guarded = array('id');

	protected $dates = array('deleted_at');

	public function student() {
		return $this->belongsTo('User', 'student_id', 'id');
	}

	public function appointment() {
		return $this->belongsTo('Appointment', 'appointment_id', 'id');
	}

}