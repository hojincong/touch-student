<?php

class Setting extends Eloquent{

	protected $table = "settings";

	protected $primaryKey = 'key';

	protected $fillable = array('key', 'value');

	public $timestamps = false;

	public function scopeKey($query, $value){
		return $query->where('key', $value)->pluck('value');
	}

}