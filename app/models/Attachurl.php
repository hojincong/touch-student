<?php

class Attachurl extends Eloquent {

	use SoftDeletingTrait;

	protected $table = 'attachurl';

	protected $connection = 'courses';

	protected $fillable = array('url');

	protected $guarded = array('id', 'syllabus_id');

	protected $dates = array('deleted_at');

	public function syllabus() {
		return $this->belongsTo('Syllabus', 'syllabus_id', 'id');
	}

}