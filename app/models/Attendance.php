<?php

class Attendance extends Eloquent {

	use SoftDeletingTrait;

	protected $table = 'attendances';

	protected $fillable = array('attend', 'student_id', 'appointment_id');

	protected $guarded = array('id');

	protected $dates = array('deleted_at');

	public function student() {
		return $this->belongsTo('User', 'student_id', 'id');
	}

	public function appointment() {
		return $this->belongsTo('Appointment', 'appointment_id', 'id');
	}

}