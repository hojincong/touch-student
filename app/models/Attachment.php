<?php

class Attachment extends Eloquent {

	use SoftDeletingTrait;

	protected $connection = 'courses';

	protected $table = 'attachments';

	protected $fillable = array('name', 'size', 'mime', 'uniqid');

	protected $guarded = array('id', 'syllabus_id');

	protected $dates = array('deleted_at');

	public function syllabus() {
		return $this->belongsTo('Syllabus', 'syllabus_id', 'id');
	}

}