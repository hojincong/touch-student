<?php

class Appointment extends Eloquent {

	use SoftDeletingTrait;

	protected $table = 'appointments';

	protected $fillable = array('seats', 'start_time', 'end_time', 'is_auto', 'schedule_id', 'notes', 'notes2', 'classname');

	protected $guarded = array('id', 'syllabus_id', 'slot_id');

	protected $dates = array('deleted_at');

	protected $visible = array('id', 'title', 'start', 'end', 'url', 'available', 'color', 'contains');

	protected $appends = array('title', 'start', 'end', 'url', 'syllabus', 'available', 'color', 'contains');

	public function schedule() {
		return $this->belongsTo('Schedule', 'schedule_id', 'id');
	}

	public function timeslot() {
		return $this->belongsTo('ScheduleTimeSlot', 'slot_id', 'id');
	}

	public function attendance() {
		return $this->hasMany('Attendance', 'appointment_id', 'id');
	}

	public function remarks() {
		return $this->hasMany('Remarks', 'appointment_id', 'id');
	}

	public function examMarker(){
		return $this->hasOne('User', 'id', 'marked_by');
	}

	public function assessmentMarker(){
		return $this->hasOne('User', 'id', 'assessed_by');
	}

	public function user() {
		return $this->belongsToMany('User', 'appointments_users', 'appointment_id', 'user_id')->withTimestamps()->withPivot('syllabus_id', 'is_exam', 'is_missed');
	}

	public function getTitleAttribute() {
		$this->process = new Processes\Appointment;
		$exam = count($this->process->checkExams($this->attributes['id']));

		if($exam > 0){
			$title = $this->schedule->name." (Seats: ".$this->user->count()."/".$this->attributes['seats'].", ".$exam." in exam)";
		}else{
			$title = $this->schedule->name." (Seats: ".$this->user->count()."/".$this->attributes['seats'].")";
		}

		return $title."\n".$this->attributes['classname'];
	}

	public function getAvailableAttribute(){
		if(Auth::check()){
			$this->process = new Processes\Appointment;
			$exam = count($this->process->checkExams($this->attributes['id']));

			if((Auth::user()->role == 3)){
				if($this->process->isNextAptExam(Auth::user()->id, $this->schedule->id)){
					return true;
				}else{
					return ($this->attributes['seats'] - ($this->user->count() - $exam));
				}
			}else{
				return ($this->attributes['seats'] - ($this->user->count() - $exam));
			}
		}
		
	}

	public function getStartAttribute() {
		return $this->attributes['start_time'];
	}

	public function getEndAttribute() {
		return $this->attributes['end_time'];
	}

	public function getUrlAttribute() {
		return route('appointment.edit', array($this->attributes['id']));
	}

	public function getSyllabusAttribute(){
		$r = array();
		$this->user->each(function($u) use (&$r){
			if(isset($u->pivot->syllabus_id) && !in_array($u->pivot->syllabus_id, $r))
				$r[] = $u->pivot->syllabus_id;
		});
		return $r;
	}

	public function getColorAttribute(){
		if(Auth::check()){
			$this->process = new Processes\Appointment;
			$in_exam = $this->process->checkExams($this->attributes['id']);
			$exam = count($in_exam);
			
			if((Auth::user()->role == 3) && array_key_exists(Auth::user()->id, $in_exam)){
			    return '#f1c40f';
			}elseif($this->user->contains(Auth::user()->id)){
				return '#3498db';
			}

			$left_seat = (($this->attributes['seats'] - ($this->user->count() - $exam)) > 0);
			$cu_exam = ((Auth::user()->role == 3) && $this->process->isNextAptExam(Auth::user()->id, $this->schedule->id));
			
			if($left_seat || $cu_exam){
				return '#2ecc71';
			}else{
				return '#e74c3c';
			}
		}
	}

	public function getContainsAttribute(){
		if(Auth::check() && (Auth::user()->role == 3) && $this->user->contains(Auth::user()->id))
			return true;
	}

}