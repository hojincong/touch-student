<?php

class ScheduleStudent extends Eloquent{

	use SoftDeletingTrait;

	protected $table = 'schedules_students';

	protected $fillable = array('expiry_date', 'start_date', 'total_session', 'exam', 'miss_count', 'schedule_id', 'user_id');

	protected $dates = array('deleted_at');

	public function schedule(){
		return $this->belongsTo('Schedule', 'schedule_id', 'id');
	}

	public function user(){
		return $this->belongsTo('User', 'user_id', 'id');
	}

}