<?php

use Illuminate\Auth\Reminders\RemindableInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\UserTrait;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait, SoftDeletingTrait;

	protected $table = 'users';

	protected $fillable = array('email', 'address', 'age', 'phone', 'remarks', 'avatar', 'start_date', 'remember_token', 'is_inactive');

	protected $guarded = array('id', 'ref_id', 'ic', 'name', 'role', 'password');

	protected $dates = array('deleted_at');

	protected $appends = array('role_name', 'sd_day', 'sd_month', 'sd_year');

	public function getAgeAttribute($value) {
		return (($value == null) or ($value == '') or ($value == 0)) ? '' : $value;
	}

	public function getRefIdAttribute($value) {
		return (($value == null) or ($value == '')) ? '' : $value;
	}

	public function getRoleNameAttribute($value) {
		$role = $this->attributes['role'];
		if ($role == 1) {
			$name = 'Admin';
		} elseif ($role == 2) {
			$name = 'Teacher';
		} else {
			$name = 'Student';
		}
		return $name;
	}

	public function getSdDayAttribute() {
		return date_parse_from_format('Y-m-d', $this->attributes['start_date'])['day'];
	}

	public function getSdMonthAttribute() {
		return date_parse_from_format('Y-m-d', $this->attributes['start_date'])['month'];
	}

	public function getSdYearAttribute() {
		return date_parse_from_format('Y-m-d', $this->attributes['start_date'])['year'];
	}

	public function setPasswordAttribute($value) {
		$this->attributes['password'] = Hash::make($value);
	}

	public function setNameAttribute($value) {
		$this->attributes['name'] = ucwords(strtolower($value));
	}

	public function acl() {
		return $this->hasMany('Acl', 'role_id', 'role');
	}

	public function schedules() {
		if(!isset($this->attributes['role']))
			return null;

		if ($this->attributes['role'] == 3) {
			return $this->belongsToMany('Schedule', 'schedules_students', 'user_id', 'schedule_id')->withPivot('start_date', 'expiry_date', 'total_session', 'used_session', 'exam', 'used_exam', 'miss_count', 'deleted_at', 'id')->withTimestamps();
		} else {
			return $this->belongsToMany('Schedule', 'schedules_tutors', 'user_id', 'schedule_id');
		}
	}

	public function notifications()
	{
		return $this->hasMany('Notification', 'user_id');
	}

	public function appointments() {
		return $this->belongsToMany('Appointment', 'appointments_users', 'user_id', 'appointment_id')->withTimestamps()->withPivot('syllabus_id', 'is_exam', 'is_missed');
	}

	public function remarks(){
		return $this->hasMany('Remarks', 'student_id', 'id');
	}

}
