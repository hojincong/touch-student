<?php

class Course extends Eloquent {

	use SoftDeletingTrait;

	protected $connection = 'courses';

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'courses';

	protected $fillable = array('name', 'description');

	protected $guarded = array('id');

	protected $dates = array('deleted_at');

	public function syllabus() {
		return $this->hasMany('Syllabus', 'course_id', 'id');
	}

	public function attachment() {
		return $this->hasManyThrough('Attachment', 'Syllabus', 'course_id', 'syllabus_id');
	}

	public function attachurl() {
		return $this->hasManyThrough('Attachurl', 'Syllabus', 'course_id', 'syllabus_id');
	}

	public function schedule() {
		return $this->hasMany('Schedule', 'course_id', 'id');
	}

}
