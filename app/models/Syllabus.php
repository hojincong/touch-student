<?php

class Syllabus extends Eloquent {

	use SoftDeletingTrait;

	protected $connection = 'courses';

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'syllabus';

	protected $fillable = array('name', 'description', 'order', 'is_exam');

	protected $guarded = array('id');

	protected $dates = array('deleted_at');

	public function getAttributeIsExam($value) {
		return ($value == 1);
	}

	public function course() {
		return $this->belongsTo('Course', 'course_id', 'id');
	}

	public function appointment() {
		return $this->hasMany('Appointment', 'syllabus_id', 'id');
	}

	public function attachment() {
		return $this->hasMany('Attachment', 'syllabus_id', 'id');
	}

	public function attachurl() {
		return $this->hasMany('Attachurl', 'syllabus_id', 'id');
	}

}
