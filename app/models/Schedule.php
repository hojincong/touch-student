<?php

class Schedule extends Eloquent {

	use SoftDeletingTrait;

	protected $table = 'schedules';

	protected $fillable = array('course_id');

	protected $guarded = array('id');

	protected $dates = array('deleted_at');

	protected $appends = array('name');

	public function getNameAttribute() {
		$tutor = $this->tutors()->first();
		//$tutorname = $tutor ? '/' . $tutor->name : '/(No Tutor)';

		$course = $this->course;
		$coursename = isset($course->name) ? $course->name : '(Course Deleted)';

		return $this->attributes['id'] . ". " . $coursename;
	}

	public function appointments(){
		return $this->hasMany('Appointment', 'schedule_id', 'id');
	}

	public function course() {
		return $this->belongsTo('Course', 'course_id', 'id');
	}

	public function times() {
		return $this->hasMany('ScheduleTime', 'schedule_id', 'id');
	}

	public function tutors() {
		return $this->belongsToMany('User', 'schedules_tutors', 'schedule_id', 'user_id');
	}

	public function students() {
		return $this->belongsToMany('User', 'schedules_students', 'schedule_id', 'user_id')->withPivot('start_date', 'expiry_date', 'total_session', 'used_session', 'exam', 'used_exam', 'miss_count', 'deleted_at', 'id')->withTimestamps();
	}

}