<?php

class Notification extends Eloquent{

	use SoftDeletingTrait;

	protected $table = "notifications";

	protected $fillable = array('user_id', 'message', 'link', 'type', 'observer', 'observer_id', 'observer_id2', 'observer_id3');

	protected $guarded = array();

	protected $dates = array('deleted_at');

	public function user()
	{
		return $this->belongsTo('User', 'user_id');
	}

}