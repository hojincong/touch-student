<?php namespace Processes;

use User as UModel;
use Redirect;
use Validator;
use DB;
use Config;
use Input;
use Setting;
use Paginator;
use Auth;

class User{

	public function getRemarksVersion($uid, $sid){
		$meta = Config::get('question._meta.versions');
		$user = UModel::findOrFail($uid);
		$schedule = $user->schedules->find($sid);
		if(!$schedule) return;

		$first = $user->appointments()->where('schedule_id', $sid)->orderBy('start_time')->first();
		if($first){	
			$version = array();
			arsort($meta);
			$first_time = explode(" ", $first->start_time)[0];
			foreach($meta as $ver=>$time){
				if($time == 'default'){
					$version[] = $ver;
					continue;
				}

				if($first_time <= $time)
					$version[] = $ver;
			}

			return $version;
		}else{
			return array();
		}
	}

	public function getRemarks($uid, $sid, $after = null, $before = null, $paginate = 30){

		$history = $this->getRemarksAll($uid, $sid, $after, $before);

		return Paginator::make(array_slice($history, (Input::get('page', 1)-1)*$paginate ,$paginate), count($history), $paginate);
	}

	public function getRemarksAll($uid, $sid, $after = null, $before = null){

		$user = UModel::findOrFail($uid);

		$schedule = $user->schedules->find($sid);
		if(!$schedule) return;

		//get apppointments
		$ap = $user->appointments()->where('schedule_id', $sid)->orderBy('start_time');
		if(isset($after))
			$ap = $ap->where('start_time', '>', $after);
		if(isset($before))
			$ap = $ap->where('start_time', '<=', $before);
		$ap = $ap->get();

		//starting order
		$order = 0;
		if(isset($schedule->pivot->used_session))
			$order += $schedule->pivot->used_session;
		if(isset($after))
			$order += $user->appointments()->where('schedule_id', $sid)->where('start_time', '<=', $after)->count();

		$history = array();
		$max_missed = $schedule->pivot->miss_count;
		$missed = 0;

		$ap->each(function($a) use (&$history, &$uid, &$order, &$missed, &$max_missed){
			$attend = $a->attendance()->where('student_id', $uid)->first(); 
			$is_missed = !(isset($attend->attend) && $attend->attend);
			if($is_missed) $missed++;

			$first = $a->remarks()->where('student_id', $uid)->first();
			$o  = array('order' => ($is_missed && ($missed <= $max_missed)) ? '-' : ++$order, 'appointment_id' => $a->id);
			$history[] = ($first) ? array_merge($first->toArray(), $o) : $o;
		});

		return $history;
	}

	public function role_filter($role){
		if(!in_array($role, array('1', '2', '3')))
			return '3';
		else
			return $role;
	}

	public function get($role = null, $paginate = null, $inactive = false){
		$user = UModel::orderBy('name', 'asc')->where('is_inactive', $inactive);
		if(isset($role))
			$user = $user->where('role', $role);

		if(isset($paginate)){
			return $user->paginate($paginate);
		}else{
			return $user->get();
		}
	}

	public function count($role = null, $inactive = false){
		$user = UModel::query();

		if(isset($role) && !empty($role))
			$user->where('role', $role);

		if(isset($inactive) && !empty($inactive))
			$user = $user->where('is_inactive', $inactive);

		return $user->count();
	}

	/** Fuzzy Search **/
	public function search($term, $paginate = null){
		$c = UModel::where('ref_id', $term)->count();

		if(isset($c) && ($c > 0)){
			$s = UModel::where('ref_id', $term)->whereNull('deleted_at');
		}else{
			$s = UModel::whereNull('deleted_at')
						->where(function($query) use ($term){
							$query->orwhere('ref_id', 'like', '%'.$term.'%');
							$query->orWhere('name', 'like', '%'.$term.'%');
							$query->orWhere('ic', 'like', '%'.$term.'%');
							$query->orWhere('email', 'like', '%'.$term.'%');
							$query->orWhere('address', 'like', '%'.$term.'%');
							$query->orWhere('phone', 'like', '%'.$term.'%');
							$query->orWhere('remarks', 'like', '%'.$term.'%');
						});
		}
		if(isset($paginate)){
			return $s->paginate($paginate);
		}else{
			return $s->get();
		}
	}

	public function save($info, $avatar = null, $userId = null){
		$rules = array(
			'name' => 'required|unique:users,name',
			'role' => 'required|in:1,2,3',
			'avatar' => 'image',
			'email' => 'email',
			'age' => 'numeric',
			'start_date' => 'date_format:Y-m-d'
		);

		if(!isset($userId)){
			$rules['name'] .= ',,id,deleted_at,NULL';
			$rules['password'] = 'required_without:password_auto|required_if:role,1,2';
		}else{
			$rules['name'] .= ','.$userId.',id,deleted_at,NULL';
		}

		$redirect = (isset($userId)) ? Redirect::action('UserController@edit', array($userId)) : Redirect::action('UserController@create');

		$role = $info['role'];
		if ($role == '3') 
			$rules['ic'] = 'required_with:password_auto';
		
		$validation = Validator::make($info, $rules);
		if ($validation->fails()) {
			return $redirect->withInput(Input::except('avatar'))->withErrors($validation)->with('msg_error', 'There are some errors in your form.');
		} else {
			if(isset($userId) && Auth::check() && (Auth::user()->role >= $info['role']) && (Auth::user()->role != 0))
				return $redirect->withInput(Input::except('avatar'))->with('msg_error', 'You don\'t have the permission to edit this user\'s info.');

			if(isset($info['schedule_id']) && is_array($info['schedule_id']) && ($role == '3')){
				foreach ($info['schedule_id'] as $key => $s) {
					if((int) $s > 0){
						$v = $this->validate_schedule(array(
							'schedule_id'	=> $s,
							'sstart_date' 	=> Input::get('sstart_date.'.$key),
							'sexpiry_date'	=> Input::get('sexpiry_date.'.$key),
							'total_session'	=> Input::get('total_session.'.$key),
							'used_session'	=> Input::get('used_session.'.$key),
							'exam'			=> Input::get('exam.'.$key),
							'used_exam'		=> Input::get('used_exam.'.$key),
							'miss_count'	=> Input::get('miss_count.'.$key),
						), $key);
						if(!($v === true)) return $v;
					}
				}
			}

			$input = array_only($info, array('email', 'address', 'age', 'phone', 'remarks', 'start_date'));

			if(isset($userId)){
				$user = UModel::findOrFail($userId);
				$user->update($input);
			}else{
				$user = UModel::create($input);
			}
			
			$user->name = ucwords(trim($info['name']));
			$user->role = $role;
			$user->ic = trim($info['ic']);

			$autopass = @$info['password_auto'];
			if ($autopass == 'on' && $role == '3') {
				$user->password = $user->ic;
			} elseif (trim($info['password']) != '') {
				$user->password = trim($info['password']);
			}

			$user->ref_id = $ref_id = @$info['ref_id'];
			/*$autoref = @$info['ref_id_auto'];
			if ($role == '1') {
				$key = 'ref_last_admin';
			} elseif ($role == '2') {
				$key = 'ref_last_teacher';
			} else {
				$key = 'ref_last_student';
			}
			$last = Setting::key($key)->first();
			if ($last == null) 
				$last = new Setting(array('key' => $key, 'value' => '0'));
			$newref = ((int) $last->value) + 1;
			if ($ref_id) {
				if ($ref_id > (int) $last->value) {
					$last->value = $ref_id;
					$last->save();
				}
			} elseif ($autoref == 'on') {
				$user->ref_id = (int) $newref;
				$last->value = $newref;
				$last->save();
			}*/

			if (isset($info['avatar_del']) && $info['avatar_del'] == '1') {
				$user->avatar = '';
			} elseif(isset($avatar) && $avatar->isValid()){
				$filename = md5($user->id) . '.png';
				$avatar->move(Config::get('touchedu.avatar_path'), $filename);
				$user->avatar = $filename;
			}

			if(isset($info['schedule_id']) && is_array($info['schedule_id']) && ($role == '3')){
				foreach ($info['schedule_id'] as $key => $s) {
					if((int) $s > 0)
						$this->save_schedule($user, array(
							'schedule_id'	=> $s,
							'start_date' 	=> Input::get('sstart_date.'.$key),
							'expiry_date'	=> Input::get('sexpiry_date.'.$key),
							'total_session'	=> Input::get('total_session.'.$key),
							'used_session'	=> Input::get('used_session.'.$key),
							'exam'			=> Input::get('exam.'.$key),
							'used_exam'		=> Input::get('used_exam.'.$key),
							'miss_count'	=> Input::get('miss_count.'.$key),
						));
				}
			}

			$user->save();

			return $redirect->with('msg_success', isset($userId) ? 'User\'s info updated successfully.' : 'You\'ve created a new user. Edit his profile <a href="' . action('UserController@edit', array($user->id)) . '">here</a>.');
		}
	}

	public function validate_schedule($info, $arrayId = null, $redirect = null){
		$rules = array(
			'schedule_id' 	=> 'required|exists:schedules,id',
			'total_session'	=> 'numeric',
			'exam'			=> 'numeric|max:'.$info['total_session'],
			'used_exam'		=> 'numeric|max:'.$info['exam'],
			'miss_count'	=> 'numeric',
			'used_session'	=> 'numeric|max:'.$info['total_session']
		);

		if(isset($info['sstart_date']))
			$rules['sstart_date'] = 'required|date_format:Y-m-d';
		else
			$rules['start_date'] = 'required|date_format:Y-m-d';
		
		if(isset($info['sexpiry_date']))
			$rules['sexpiry_date'] = 'required|date_format:Y-m-d';
		else
			$rules['expiry_date'] = 'required|date_format:Y-m-d';

		if(isset($arrayId)){
			foreach ($info as $key => $value) {
				$info[$key.".".$arrayId] = $value;
				unset($info[$key]);
			}
		}

		if(isset($arrayId)){
			foreach ($rules as $key => $value) {
				$rules[$key.".".$arrayId] = $value;
				unset($rules[$key]);
			}
		}

		$validate = Validator::make($info, $rules);

		if(!isset($redirect))
			$redirect = Redirect::action('UserController@create');

		if($validate->fails())
			return $redirect->withInput(Input::except('avatar'))->withErrors($validate)->with('msg_error', 'There are some errors in your form.');
		else
			return true;
	}

	public function save_schedule(UModel $user, $info){
		if(!$user->schedules->contains($info['schedule_id']))
			$user->schedules()->attach($info['schedule_id']);
		$user->schedules()->updateExistingPivot($info['schedule_id'], array_except($info, array('schedule_id')));
		return true;
	}

}