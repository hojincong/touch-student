<?php namespace Processes;

use Course as CModel;
use Attachment; 
use Attachurl;
use Syllabus;
use User;
use Redirect;
use Validator;
use Config;
use Input;
use DB;
use Session;

class Course{

	public function getUserCourse($uid){
		$user = User::findOrFail($uid);
		$course = array();
		$repeat = array();

		$user->schedules->each(function($s) use (&$course, &$repeat){
			if(in_array($s->course_id, $repeat)) return;
			$course[] = $s->course;
			$repeat[] = $s->course_id;
		});

		return $course;
	}

	public function save($info, $courseId = null){
		$validation = Validator::make($info, array(
			'name' => 'required'
		));

		$redirect = (isset($courseId)) ? Redirect::action('CourseController@edit', array($courseId)) : Redirect::action('CourseController@create');

		if($validation->fails()){
			return $redirect->withInput()->withErrors($validation)->with('msg_error', 'There are some errors in your form.');
		}

		if($courseId){
			$course = CModel::findOrFail($courseId);
			$course->update($info);

			return Redirect::action('CourseController@edit', array($courseId))->with('msg_success', 'Course\'s info updated successfully.');
		}else{
			$course = CModel::create($info);

			return Redirect::action('CourseController@create')->with('msg_success', 'You\'ve created a new course. Edit it\'s info <a href="'.action('CourseController@edit', array($course->id)).'">here</a>.');
		}
	}

	public function delete($courseId){
		$course = CModel::destroy($courseId);

		return Redirect::action('CourseController@index')->with('msg_success', 'Course deleted successfully.');
	}

	public function get_counts($course){
		return array(
			'syllabus' 	=> $course->syllabus()->where('is_exam', '=', false)->count(),
			'exam'		=> $course->syllabus()->where('is_exam', '=', true)->count()
		);
	}

	public function get_user_syllabus($uid, $cid, $is_exam = false){
		$user = User::findOrFail($uid);
		$schedules = $user->schedules()->where('course_id', $cid)->get()->lists('id');
		$syllabus = array();

		$user->appointments->each(function($a) use (&$schedules, &$syllabus, &$is_exam){
			if(in_array($a->schedule_id, $schedules) && (!$a->pivot->is_missed) && ($a->start_time < date('Y-m-d h:i:s'))){
				if($is_exam){
					if($a->pivot->is_exam) $syllabus[] = $a->pivot->syllabus_id;
				}else{
					if(!$a->pivot->is_exam) $syllabus[] = $a->pivot->syllabus_id;
				}
			}
		});
		
		$syllabus = array_unique($syllabus);

		return (count($syllabus) > 0) ? Syllabus::whereIn('id', $syllabus)->get() : $syllabus;
	}

	public function get_syllabus($course, $is_exam = false){
		return $course->syllabus()->where('is_exam', '=', $is_exam)->with('attachment', 'attachurl')->orderBy('order')->get();
	}

	public function save_syllabus($info, $courseId, $attachments = array(), $syllabusId = null){
		if(isset($syllabusId)){
			Session::flash('syllabusId', $syllabusId);
			$input = array(
				'name' => $info['name'][$syllabusId], 
				'description' => $info['description'][$syllabusId]
			);
			$user = array('name.'.$syllabusId => 'required');
		}else{
			$input = array_only($info, array('name', 'description'));
			$user = array('name' => 'required');
		}

		$validation = Validator::make($info, $user);

		if ($validation->fails()) {
			return Redirect::action('SyllabusController@'. (isset($info['is_exam']) ? 'exam' : 'index'), array($courseId))->withInput(Input::except('attachment_url'))->withErrors($validation)->with('msg_error', 'There are some errors in your form.');
		}

		if(isset($info['is_exam']) && ($info['is_exam'] == '1'))
			$input['is_exam'] = $info['is_exam'];

		$uploaded = array();
		if(is_array($attachments) && count($attachments) > 0){
			foreach($attachments as $a){
				if(!isset($a) || ($a == null))
					continue;

				if($a->isValid()){
					$ac = Attachment::create(array(
						'name' => $a->getClientOriginalName(),
						'size' => $a->getSize(),
						'mime' => $a->getMimeType(),
					));
					$ac->uniqid = $ac->id . explode(" ", microtime())[1];
					$uploaded[] = $ac;

					$a->move(Config::get('touchedu.attachment_path'), $ac->uniqid);
				}else{
					return Redirect::action('SyllabusController@'. (isset($info['is_exam']) ? 'exam' : 'index'), array($courseId))->withInput(array_except($info, array('attachment_url')))->with('msg_error', 'Error occurs when uploading files.');
				}
			}
		}

		$reference = array();
		if(isset($info['attachment_url']) && is_array($info['attachment_url'])) {
			$links = $info['attachment_url'];
			foreach ($links as $value) {
				$value = trim($value);
				if ($value == '') 
					continue;
				
				$validation = Validator::make(
					array('attachment_url' => $value), 
					array('attachment_url' => 'url')
				);

				if ($validation->fails()) {
					return Redirect::action('SyllabusController@'. (isset($info['is_exam']) ? 'exam' : 'index'), array($courseId))->withInput(array_except($info, array('attachment_url')))->withErrors($validation)->with('msg_error', 'URL links provided are unrecognisable.');
				} else {
					$reference[] = Attachurl::create(array('url' => $value));
				}
			}
		}

		if($syllabusId){
			$syllabus = Syllabus::findOrFail($syllabusId);
			$syllabus->update($input);
		}else{
			$syllabus = Syllabus::create($input);
		}

		DB::transaction(function () use ($syllabus, $input, $uploaded, $reference, $courseId, $syllabusId) {
			$course = CModel::findOrFail($courseId);
			
			$syllabus->attachment()->saveMany($uploaded);
			$syllabus->attachurl()->saveMany($reference);
			$syllabus->course()->associate($course);

			if(!isset($syllabusId))
				$syllabus->order = $course->syllabus->max('order')+1;

			$syllabus->save();
		});

		if(isset($syllabusId)){
			return Redirect::action("SyllabusController@" . (isset($info['is_exam']) ? 'exam' : 'index'), array($courseId))->with('msg_success', 'You\'ve updated syllabus "' . $syllabus->name . '" successfully.')->with('syllabusId', $syllabusId);
		}else{
			return Redirect::action('SyllabusController@'.(isset($info['is_exam']) ? 'exam' : 'index'), array($courseId))->with('msg_success', 'You\'ve created a new Syllabus.');
		}
	}

	public function delete_syllabus($syllabusId){
		$syllabus = Syllabus::findOrFail($syllabusId);
		$name = $syllabus->name;
		$exam = $syllabus->is_exam;
		$courseId = $syllabus->course->id;
		$syllabus->delete();

		return Redirect::action(($exam == '1') ? 'SyllabusController@exam' : 'SyllabusController@index', array($courseId))->with('msg_success', 'Syllabus "' . $name . '" deleted successfully.');
	}

	public function reorder_syllabus($courseId, $order, $is_exam = false){
		$order = explode(',', $order);
		$count = 0;
		foreach ($order as $id) {
			$syllabus = Syllabus::find($id);
			if ($syllabus) {
				$syllabus->order = $count;
				$syllabus->save();
				$count++;
			}
		}

		return Redirect::action($is_exam ? 'SyllabusController@exam' : 'SyllabusController@index', array($courseId))->with('msg_success', 'Syllabus sorted successfully.');
	}

}