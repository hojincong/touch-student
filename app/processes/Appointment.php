<?php namespace Processes;

use Appointment as ApModel;
use Syllabus;
use User;
use Attendance;
use Validator;
use Redirect;
use Remarks;
use Auth;
use Schedule;
use DateTime;
use DateInterval;
use Config;
use DB;

class Appointment {

	protected $is_next_apt = array();
	
	/* Calendar Ajax */  

	public function getEvents($start, $end, $sid = null) {
		$validate = Validator::make(array('start' => $start, 'end' => $end, 'sid' => $sid), array(
			'start' => 'required|date_format:Y-m-d',
			'end' 	=> 'required|date_format:Y-m-d',
			'sid'	=> 'exists:schedules,id'
		));

		if (!$validate->fails()) {
			if(Auth::check() && (Auth::user()->role == 3)){
				if($start < date('Y-m-d'))
					$start = date('Y-m-d', time());
				if($end > date('Y-m-d', time() + 60*60*24*14))
					$end = date('Y-m-d', time() + 60*60*24*14);
			}
			$events = ApModel::whereBetween('start_time', array($start, $end))->where('schedule_id', $sid)->get();
			return $events->toJson();
		}
	}

	public function addEventStudent($apId){

		$redirect = Redirect::action('AppointmentController@index');

		if(!Auth::check()) return $redirect;

		$user_id = Auth::user()->id;
		$ap = ApModel::findOrFail($apId);
		$sid = $ap->schedule_id;

		//must book before 10pm before the day of the sessinos
		$start = new DateTime($ap->start_time);
		$yesterday = $start->sub(new DateInterval("P1D"));

		if((date('Y-m-d H:i:s') > $yesterday->format("Y-m-d 22:00:00"))) 
			return $redirect->with('msg_error', 'Sorry, booking for this appointment has closed .');

		//student must be in the class(schedules)
		$u = User::findOrFail($user_id);
		$uInfo = $u->schedules->find($sid);
		if(!$uInfo) return $redirect->with('msg_error', 'You were not added to the class.');	

		//expiry time must not have exceeded
		$uInfo = $uInfo->pivot;
		$is_expired = ($ap->start_time > $uInfo->expiry_date);
		if($is_expired) return $redirect->with('msg_error', 'Your subscription for the class has expired.');	

		//must still have sessions
		$count = 0;
		$missed = 0;
		$after = 0;
		$u->appointments->each(function($a) use (&$sid, &$ap, &$count, &$missed, &$after){
			if($a->schedule_id == $sid)
				$count++;
			if(($a->schedule_id == $sid) && $a->pivot->is_missed)
				$missed++;
			if($a->start_time > $ap->start_time)
				$after++;
		});

		$missed = min($uInfo->miss_count, $missed);
		//if($missed > $uInfo->miss_count) $missed = -($missed - $uInfo->miss_count);
		$has_sessions = ($uInfo->total_session - $uInfo->used_session) - ($count - $missed);
		if($has_sessions < 1) return $redirect->with('msg_error',' You have finished your sessions.');

		//maximum 4 sessions per day
		$today = $u->appointments()->whereBetween('start_time', array(date('Y-m-d 00:00:00', strtotime($ap->start_time)), date('Y-m-d 23:59:59', strtotime($ap->start_time))))->count();
		if($today >= 4)
			return $redirect->with('msg_error', 'Sorry, you can only book 4 sessions per day.');
		

		if(!$ap->user->contains($u->id)){
			$ap->user()->attach($u);

			$exam = count($this->checkExams($apId));
			if(($ap->seats == 0) || ($ap->seats <= ($ap->user->count() - $exam))){
				$ap->user()->detach($u);

				return $redirect->with('msg_error', 'Sorry, no more seats.');
			}else{
				\Event::fire('appointment.user.attach', array(array(
					'appointment' 	=> $ap, 
					'user' 			=> $u, 
					'pivot' 		=> $uInfo,
					'count' 		=> ($count + $uInfo->used_session - $missed - $after) + 1
				)));
			}
		}

		return $redirect->with('msg_success', 'You have been added to the appointment.');

	}

	public function deleteEventStudent($apId){

		$redirect = Redirect::action('AppointmentController@index');

		if(!Auth::check()) return $redirect;

		$user_id = Auth::user()->id;

		if(isset($user_id)){
			$appointment = ApModel::findOrFail($apId);
			$student = User::findOrFail($user_id);
			$uInfo = $student->schedules->find($appointment->schedule_id);

			if(!$uInfo) return Redirect::action('AppointmentController@getStudents', array($apId))->with('msg_error', 'You were not added to the class.');	

			//must cancel before 10pm before the day of the sessions
			$start = new DateTime($appointment->start_time);
			$yesterday = $start->sub(new DateInterval("P1D"));

			if((date('Y-m-d H:i:s') > $yesterday->format("Y-m-d 22:00:00"))) 
				return $redirect->with('msg_error', 'Sorry, you cannot cancel the appointment now.');

			$appointment->user()->detach($student);

			$uInfo = $uInfo->pivot;

			\Event::fire('appointment.user.detach', array(array(
				'appointment' 	=> $appointment, 
				'user' 			=> $student, 
				'pivot' 		=> $uInfo
			)));
		}
		return $redirect->with('msg_success', 'Cancel appointment successfully.');

	}

	/* Students */

	//get all available student in a schedule
	public function getStudents($apId){
		$ap = ApModel::findOrFail($apId);
		$st = $ap->user;
		return $ap->schedule->students->filter(function($u) use (&$st, &$ap){
			$is_added = $st->contains($u->id);
			if($is_added) return false;
			
			$is_expired = ($ap->start_time > $u->pivot->expiry_date);
			if($is_expired) return false;
			
			$sid = $ap->schedule_id;
			$appointments = $u->appointments()->where('schedule_id', $sid)->get();
			$count = count($appointments);
			$missed = 0;
			$appointments->each(function($a) use (&$sid, &$missed){
				if($a->pivot->is_missed)
					$missed++;
			});

			$missed = min($u->pivot->miss_count, $missed);
			//if($missed > $u->pivot->miss_count) $missed = -($missed - $u->pivot->miss_count);
			$has_session = ($u->pivot->total_session - $u->pivot->used_session) - ($count - $missed);

			return ($has_session > 0);
		});
	}

	public function addStudent($apId, $user_id = null){
		if(!isset($user_id)) return Redirect::action('AppointmentController@getStudents', array($apId));

		$ap = ApModel::with('schedule')->findOrFail($apId);
		$sid = $ap->schedule_id;

		$u = User::findOrFail($user_id);
		$uInfo = $u->schedules->find($sid);

		if(!$uInfo) return Redirect::action('AppointmentController@getStudents', array($apId))->with('msg_error', 'Student was not added to the class.');	

		$uInfo = $uInfo->pivot;

		$is_expired = ($ap->start_time > $uInfo->expiry_date);
		if($is_expired) return Redirect::action('AppointmentController@getStudents', array($apId))->with('msg_error', 'Student\'s subscribtion for the class has expired.');	

		$count = 0;
		$missed = 0;
		$after = 0;
		$u->appointments->each(function($a) use (&$sid, &$ap, &$count, &$missed, &$after){
			if($a->schedule_id == $sid)
				$count++;
			if(($a->schedule_id == $sid) && $a->pivot->is_missed)
				$missed++;
			if($a->start_time > $ap->start_time)
				$after++;
		});
		$missed = min($uInfo->miss_count, $missed);
		//if($missed > $uInfo->miss_count) $missed = -($missed - $uInfo->miss_count);
		$has_sessions = ($uInfo->total_session - $uInfo->used_session) - ($count - $missed);
		if($has_sessions < 1) return Redirect::action('AppointmentController@getStudents', array($apId))->with('msg_error', 'Student has finished his sessions.');

		if(!$ap->user->contains($u->id)){
			$ap->user()->attach($u);

			$exam = count($this->checkExams($apId));
			if(($ap->seats == 0) || ($ap->seats <= ($ap->user->count() - $exam))){
				$ap->user()->detach($u);

				return Redirect::action('AppointmentController@getStudents', array($apId))->with('msg_error', 'Sorry, no more seats.');
			}else{
				\Event::fire('appointment.user.attach', array(array(
					'appointment' 	=> $ap, 
					'user' 			=> $u, 
					'pivot' 		=> $uInfo,
					'count' 		=> ($count + $uInfo->used_session - $missed - $after) + 1
				)));
			}
		}

		return Redirect::action('AppointmentController@getStudents', array($apId))->with('msg_success', 'Student added to appointment.');
	}

	public function deleteStudent($apId, $user_id = null){
		if(isset($user_id)){
			$appointment = ApModel::findOrFail($apId);
			$student = User::with('appointments')->findOrFail($user_id);
			$uInfo = $student->schedules->find($appointment->schedule_id);

			if(!$uInfo) return Redirect::action('AppointmentController@getStudents', array($apId))->with('msg_error', 'Student was not added to the class.');	

			$uInfo = $uInfo->pivot;

			$appointment->user()->detach($student);
			\Event::fire('appointment.user.detach', array(array(
				'appointment' 	=> $appointment, 
				'user' 			=> $student, 
				'pivot' 		=> $uInfo
			)));
		}
		return Redirect::action('AppointmentController@getStudents', array($apId))->with('msg_success', 'Student removed from appointment.');
	}

	/* Student Appointment No Info */
	public function studentAptNo($uid, $apId){
		$student = User::findOrFail($uid);
		$appointment = ApModel::findOrFail($apId);
		$schedule = $student->schedules->find($appointment->schedule->id);

		$sessions = $student->appointments()->orderBy('start_time')->get();
		$count = 0;
		$miss = 0;
		foreach($sessions as $s){
			//omit apt in other schedules
			if($s->schedule_id != $appointment->schedule->id) continue;

			//missed sessions
			if($s->attendance->filter(function($attend) use (&$uid){
				return (($attend->student_id == $uid) && ($attend->attend == 0));
			})->count()){
				$miss++;
			}

			$count++;

			if($s->id == $apId) break;
		}

		return $count+$schedule->pivot->used_session-min($miss, $schedule->pivot->miss_count);
	}

	/* Lesson Plan */

	/*public function toggleAuto($apId, $is_auto){
		$appointment = ApModel::findOrFail($apId);
		$appointment->is_auto = (bool) $is_auto;
		if($is_auto)
			$appointment->syllabus_id = null;
		$appointment->save();
	}*/

	public function isNextAptExam($uid, $sid)
	{
		if(isset($this->is_next_apt[$uid][$sid])) return $this->is_next_apt[$uid][$sid];

		$user = User::findOrFail($uid);
		$schedule = Schedule::findOrFail($sid);
		$course = $schedule->course;

		//student not in schedule
		$su = $schedule->students->find($uid);
		if(!$su) return -1;

		//load exam in schedule's syllabus
		if(isset($course)){
			$exams = $course->syllabus()->where('is_exam', '=', true)->orderBy('order')->get();
		}else{
			$exams = array();
		}

		//load student's schedule info
		$info 	= $su->pivot;
		$total 	= $info->total_session;
		$exam 	= $info->exam;
		$used_session	= $info->used_session;
		$used_exam		= $info->used_exam;
		$miss_count		= $info->miss_count;
		$missed = 0;

		if($exam == 0) {
			$this->is_next_apt[$uid][$sid] = false;
			return false;
		}

		//count used appointment in schedule
		$passedExam = 0;
		$c = $user->appointments->filter(function($a) use ($sid, &$missed, &$miss_count, &$passedExam){
			if($a->schedule_id != $sid) return false;
			if($a->pivot->is_missed) $missed++;
			if($a->pivot->is_exam) $passedExam++;
			$count = $a->pivot->is_missed ? ($missed > $miss_count) : true;

			return $count;
		})->count() + $used_session;

		//finished course
		if($c == $total){
			$this->is_next_apt[$uid][$sid] = false;
			return false;
		} 
		$c += 1;

		//count exam(second last is exam)
		if($exam>$total){
			$is_exam = true;
			$times = $c;
			$has_exam = (count($exams) >= $times);
		}else{
			$is_exam = (((($c % floor($total/$exam)) == 0) && ($c != $total)) || ($c == ($total-1)));
			$times = ceil($c/floor($total/$exam));
			$has_exam = (count($exams) >= $times);
		}

		if($is_exam && $has_exam && ($times>($passedExam+$used_exam))){
			$this->is_next_apt[$uid][$sid] = $exams->offsetGet($times-1);
		}else{
			$this->is_next_apt[$uid][$sid] = false;
		}

		return $this->is_next_apt[$uid][$sid];
	}

	//check if current appointment is exam
	// @return array(user_id => exam_id)
	public function checkExams($apId){
		$ap = ApModel::findOrFail($apId);
		$st = $ap->user;
		$s = $ap->schedule->students;
		$sid = $ap->schedule->id;
		$course = $ap->schedule->course;
		if(isset($course)){
			$exams = $ap->schedule->course->syllabus()->where('is_exam', '=', true)->orderBy('order')->get();
		}else{
			$exams = array();
		}

		$return = array();

		//each appointment user
		$st->each(function($u) use ($s, $sid, &$return, &$exams, &$ap){
			if(!$s->find($u->id)) return;
			
			$f = $s->find($u->id)->pivot;
			$t = $f->total_session;
			$e = $f->exam;
			$us = $f->used_session;
			$ue = $f->used_exam;
			$m = $f->miss_count;
			$missed = 0;

			if($e == 0) return;

			//each user's appointments(current appointment will be counted)
			$c = $u->appointments->filter(function($a) use ($sid, &$ap, &$missed, &$m){
				if($a->schedule_id != $sid) return false;
				if($a->pivot->is_missed) $missed++;
				$count = $a->pivot->is_missed ? ($missed > $m) : true;

				return ($a->schedule_id == $sid) && ($a->start_time <= $ap->start_time) && ($count || ($ap->id == $a->id));
			})->count() + $us;

			//second last session is also exam
			//count exam
			//@TODO: error on odd number,  eg: 5/3
			if($e>$t){
				$is_exam = true;
				$times = $c;
				$has_exam = (count($exams) >= $times);
			}else{
				$is_exam = (((($c % floor($t/$e)) == 0) && ($c != $t)) || ($c == ($t-1)));
				$times = ceil($c/floor($t/$e));
				$has_exam = (count($exams) >= $times);
			}
			if($is_exam && $has_exam && ($times>$ue))
				$return[$u->id] = $exams->offsetGet($times-1);
		});
//dd($return);
		return $return;
	}

	public function getLessons($apId){
		$ap = ApModel::findOrFail($apId);
		$st = $ap->user;
		$sid = $ap->schedule_id;
		$in = $ap->schedule->students;

		$history = array();
		$st->each(function($u) use (&$history, &$sid, &$in, &$ap){
			//$mc = $in->find($u->id)->pivot->miss_count;
			//$missed = 0;
			$u->appointments->each(function($a) use (&$history, &$u, &$sid, &$ap/*, &$mc, &$missed*/){
				if(($a->schedule_id != $sid) || ($a->start_time > $ap->start_time) || ($a->id == $ap->id)) return;
				//if($a->pivot->is_missed) $missed++;
				if(/*($a->pivot->is_missed && ($missed > $mc)) || */!$a->pivot->is_missed)
					$history[$u->id][$a->pivot->syllabus_id] = true;
			});
		});

		return $history;
	}

	public function updateLesson($apId, $syllabusId = null){
		$appointment = ApModel::findOrFail($apId);
		$exam = $this->checkExams($apId);
		if($syllabusId){
			$syllabus = Syllabus::findOrFail($syllabusId);
			$appointment->user->each(function($user) use ($syllabusId, $apId, &$exam){
				if(isset($exam[$user->id]))
					$user->appointments()->updateExistingPivot($apId, array('syllabus_id' => $exam[$user->id]->id, 'is_exam' => true));
				else
					$user->appointments()->updateExistingPivot($apId, array('syllabus_id' => $syllabusId));
			});
			$appointment->is_auto = false;
		}else{
			$appointment->user->each(function($user) use ($syllabusId, $apId){
				$user->appointments()->updateExistingPivot($apId, array('syllabus_id' => null, 'is_exam' => 0));
			});
			$appointment->is_auto = true;
		}
		$appointment->save();
	}

	public function autoSyllabus($apId){
		$app = ApModel::findOrFail($apId);
		$stu = $app->user;
		if($stu->isEmpty()) return;

		//update exams students
		$exa = $this->checkExams($apId);
		$stu->only(array_keys($exa))->each(function($u) use ($apId, $exa){
			$u->appointments()->updateExistingPivot($apId, array('syllabus_id' => $exa[$u->id]->id, 'is_exam' => true));
		});

		//update normal students
		$syl = $app->schedule->course->syllabus()->where('is_exam', 'false')->orderBy('order')->lists('id');
		$syl = array_combine($syl, array_fill(0, count($syl), 0));
		$sid = $app->schedule_id;

		$stu->except(array_keys($exa))->each(function($u) use (&$syl, $apId, $app, $sid){
			$u->appointments->each(function($a) use (&$syl, $apId, $app, &$missed, $sid){
				if(($a->schedule_id != $sid) || $a->pivot->is_missed || $a->pivot->is_exam) return;
				if(isset($a->pivot->syllabus_id) && ($a->id != $apId))
					$syl[$a->pivot->syllabus_id]++;
			});
		});

		//find min
		$val = min(array_values($syl));
		foreach($syl as $k=>$s){
			if($s == $val){
				$stu->except(array_keys($exa))->each(function($user) use ($k, $apId){
					$user->appointments()->updateExistingPivot($apId, array('syllabus_id' => $k));
				});
				return;
			}
		}
	}

	/* Attendance */

	public function updateAttend($apId, $list = array()){
		$appointment = ApModel::with('user')->findOrFail($apId);

		if(!is_array($list)) $list = array();

		$appointment->user->each(function($u) use (&$list, &$apId){

			$attend = Attendance::firstOrCreate(array(
				'student_id'	=> $u->id,
				'appointment_id'=> $apId
			));

			$attend->attend = in_array($u->id, $list);
			$attend->save();

			$u->pivot->is_missed = !in_array($u->id, $list);
			$u->pivot->save();

		});

		\Event::fire('appointment.attend.update', array(array(
			'apuser'=> $appointment->user,
			'ap'	=> $appointment
		)));
		
	}

	/* Remarks */

	public function updateRemarks($apId, $input){
		$ap = ApModel::findOrFail($apId);
		//update exam marking status
		if(isset($input['exam_marking']) && ($input['exam_marking'] == 1)){
			$ap->marked_at = DB::raw('NOW()'); //current timestamp
			if(Auth::check())
				$ap->marked_by = Auth::user()->id;
		}else{
			$ap->marked_at = null;
			$ap->marked_by = null;
		}

		//update marking status
		$ap->assessed_at = DB::raw('NOW()');
		if(Auth::check())
			$ap->assessed_by = Auth::user()->id;
		
		$ap->save();

		//tidy remarks data
		$st = $ap->user;
		$version = $this->chooseVersion(Config::get('question._meta.versions'), $ap->start_time);
		$questions = Config::get('question.'.$version);
		$ke = array();

		foreach ($questions as $section => $remarks) {
			foreach($remarks as $value){
				$ke[$value['model']] = isset($value['type']) ? $value['type'] : 'range';
			}
		}
		$in = array_only($input, array_keys($ke));

		$ru = array();
		foreach($in as $keys=>$vals){
			foreach($vals as $key=>$val){
				if($ke[$keys] == 'range')
					$ru[$keys.'.'.$key] = 'numeric|min:0|max:5';
			}
		}
		$va = Validator::make($in, $ru);

		if($va->fails())
			return Redirect::action('AppointmentController@getRemarks', array($apId))->withErrors($va)->withInput()->with('msg_error', 'There are some errors in your ratings.');

		//save remarks
		$st->each(function($u) use ($apId, $in){
			$re = Remarks::firstOrCreate(array('student_id' => $u->id, 'appointment_id' => $apId));
			foreach($in as $keys=>$vals){
				foreach($vals as $key=>$val){
					if($key == $u->id)
						$re->{$keys} = $val;
				}
			}
			$re->save();
		});

		return Redirect::action('AppointmentController@getRemarks', array($apId))->with('msg_success', 'Ratings updated successfully.');	
	}

	public function chooseVersion($config, $apDate){
		asort($config);
		$now = explode(" ", $apDate)[0];

		foreach($config as $ver=>$time){
			if($time == 'default'){
				$default = $ver;
				unset($config[$ver]);
			}
		}

		foreach($config as $ver=>$time){
			if($now <= $time)
				return $ver;
		}

		if(isset($default)){
			return $default;
		}else{
			return false;
		}
	}

}