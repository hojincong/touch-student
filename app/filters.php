<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
 */

App::before(function ($request) {
	if (App::environment('local') && !Auth::check()) {
		//Auth::login(User::find(1));
	}

	//@experiment: record user action
	if(!App::environment('local')){
		DB::table('weblog')->insert(array(
			'path' 	=> $request->path(),
			'method'=> $request->method(),
			'input' => count($request->input()) ? serialize($request->input()) : null,
			'uid'	=> Auth::check() ? Auth::user()->id : null,
			'role'	=> Auth::check() ? Auth::user()->role : null
		));
	}
});

App::after(function ($request, $response) {
	//
});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
 */

Route::filter('auth', function () {
	if(Session::has('mbti') && (Request::segment(1)!='mbti')){
		Session::forget('mbti');
		Session::flash('msg_error', 'You need to login again.');
		Auth::logout();
	}

	if (Auth::guest()) {
		if (Request::ajax()) {
			return Response::make('Unauthorized', 401);
		} else {
			return Redirect::guest('login');
		}
	}
});

Route::filter('auth.basic', function () {
	return Auth::basic();
});

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
 */

Route::filter('guest', function () {
	if (Auth::check()) {
		return Redirect::to('/');
	}
}
);

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
 */

Route::filter('csrf', function () {
	if (Session::token() !== Input::get('_token')) {
		throw new Illuminate\Session\TokenMismatchException;
	}
});
