<?php namespace Observers\Notifications;

use Notification;

/**
 * Notifications Observer
 * Send notification to user based on appointment no.
 *
 * observer_id Appointment Id
 * observer_id2 Appointment Count
 * observer_id3 Schedule Id
 */

abstract class AppointmentCount
{

	abstract protected function check($count, $pivot);

	public function attach($data)
	{
		if($this->check($data['count'], $data['pivot']))
		{
			$this->updateByCount($data['user']->id, $data['appointment'], $data['count'], str_replace('%date%', date('Y-m-d G:i', strtotime($data['appointment']->start_time)), static::MESSAGE));
		}

		$this->recalculate($data['user'], $data['count'], $data['appointment']->schedule_id, $data['pivot']);
	}

	public function detach($data)
	{
		$count = $this->getCountByAp($data['user']->id, $data['appointment']->schedule_id, $data['appointment']->id);
		if($count) $ap = $data['user']->appointments()->where('schedule_id', $data['appointment']->schedule_id)->orderBy('start_time')->skip(($count-$data['pivot']->used_session)-1)->take(1)->first();

		//replacement from future session
		if(isset($ap) && $ap){
			$this->updateByCount($data['user']->id, $ap, $count, str_replace('%date%', date('Y-m-d G:i', strtotime($data['appointment']->start_time)), static::MESSAGE));
		}elseif($count){
			$this->deleteByCount($data['user']->id, $data['appointment']->schedule_id, $count);
		}

		$this->recalculate($data['user'], isset($count) ? $count : 0, $data['appointment']->schedule_id, $data['pivot']);
	}

	protected function updateByCount($uid, $ap, $count, $message)
	{
		$find = Notification::firstOrCreate(array(
			'user_id'	=> $uid,
			'type'		=> static::TYPE,
			'observer'	=> static::OBSERVER,
			'observer_id2'	=> $count,
			'observer_id3'	=> $ap->schedule_id
		));

		$find->message = $message;
		$find->observer_id = $ap->id;
		$find->save();
	}

	protected function getCountByAp($uid, $sid, $apid)
	{
		$prev = Notification::where('user_id', $uid)
							->where('observer', static::OBSERVER)
							->where('observer_id', $apid)
							->where('observer_id3', $sid)
							->first();

		if(isset($prev) && $prev->count()) {
			return $prev->observer_id2;
		}
	}

	protected function deleteByCount($uid, $sid, $count)
	{
		$prev = Notification::where('user_id', $uid)
							->where('observer', static::OBSERVER)
							->where('observer_id2', $count)
							->where('observer_id3', $sid)
							->first();

		if(isset($prev) && $prev->count()) $prev->delete();
	}

	protected function recalculate($user, $count, $sid, $pivot)
	{
		$future = Notification::where('user_id', $user->id)->where('observer', static::OBSERVER)->where('observer_id2', '>', $count)->where('observer_id3', $sid)->get();
		foreach ($future as $notification) {
			$c = $notification->observer_id2;
			$r = $user->appointments()->where('schedule_id', $sid)->orderBy('start_time')->skip(($c-$pivot->used_session)-1)->take(1)->first();

			if($r){
				$this->updateByCount($user->id, $r, $c, str_replace('%date%', date('Y-m-d G:i', strtotime($r->start_time)), static::MESSAGE));
			}else{ 
				$this->deleteByCount($user->id, $sid, $c);
			}
		}
	}

}

/**
 * Notifications Observer
 * Send notification to user if next appointment is exam
 *
 * type 'warning'
 * observer 'AppointmentIsBeforeExam'
 */

class AppointmentIsBeforeExam extends AppointmentCount{

	const OBSERVER = 'AppointmentIsBeforeExam';
	const TYPE = 'warning';
	const MESSAGE = 'You have booked an appointment for %date%. Please be reminded that you will have to sit for exam in the next appointment.';

	protected function check($count, $pivot)
	{
		if($pivot->exam <= 0) return false;

		return ((($count % floor($pivot->total_session/$pivot->exam)) == (floor($pivot->total_session/$pivot->exam) - 1) && !($count == ($pivot->total_session-1))) || ($count == ($pivot->total_session - 2)));
	}

}

/**
 * Notifications Observer
 * Send notification to user if this is the last five appointment
 *
 * type 'warning'
 * observer 'AppointmentIsLast'
 */

class AppointmentIsLast extends AppointmentCount{

	const OBSERVER = 'AppointmentIsLast';
	const TYPE = 'warning';
	const MESSAGE = 'You have booked an appointment for %date%. You still have 5 more sessions.';

	public function check($count, $pivot)
	{
		return ($count == ($pivot->total_session - 5));
	}

}

/**
 * Notifications Observer
 * Send notification to user if this is the last appointment
 *
 * type 'warning'
 * observer 'AppointmentIsEnded'
 */

class AppointmentIsEnded extends AppointmentCount{

	const OBSERVER = 'AppointmentIsEnded';
	const TYPE = 'warning';
	const MESSAGE = 'You have booked an appointment for %date%. This is your last apppointment for this class.';

	public function check($count, $pivot)
	{
		return ($count == ($pivot->total_session));
	}

}

/**
 * Notification Observer
 * Send notification to user if the appointment is missed
 * 
 * type 'warning'
 * observer 'AppointmentIsMissed'
 * observer_id Appointment Id
 * observer_id2 Schedule Id
 * observer_id3 Covered/Not Covered
 */

class AppointmentIsMissed{

	const OBSERVER = 'AppointmentIsMissed';
	const TYPE = 'warning';
	const MESSAGE_COVERED = 'You have missed an appointment on %date%. The absent class will be charged after %absent% times.';
	const MESSAGE_NOTCOVERED = 'You have missed an appointment on %date%. The absent class will be charged from your total session.';

	public function update($data){
		foreach($data['apuser'] as $u){
			$c = 0;
			$l = array();
			$t = $data['ap']->start_time;
			$missed = $u->appointments()->where('schedule_id', $data['ap']->schedule_id)->orderBy('start_time')->get()->each(function($a) use (&$c, $t, &$l){
				if($a->pivot->is_missed && ($a->pivot->start_time < $t)) $c++;
				if($a->pivot->is_missed) $l[] = $a->id;
			});

			$total = $u->schedules->find($data['ap']->schedule_id)->pivot->miss_count;

			$mc = str_replace('%date%', date('Y-m-d G:i', strtotime($t)), self::MESSAGE_COVERED);
			$mc = str_replace('%absent%', $total, self::MESSAGE_NOTCOVERED);
			$mnc = str_replace('%date%', date('Y-m-d G:i', strtotime($t)), self::MESSAGE_NOTCOVERED);

			if($u->pivot->is_missed){
				$notification = Notification::firstOrCreate(array(
					'user_id'	=> $u->id,
					'type'		=> self::TYPE,
					'observer'	=> self::OBSERVER,
					'observer_id'	=> $data['ap']->id,
					'observer_id2'	=> $data['ap']->schedule_id
				));

				if($c > $total){
					$notification->message = $mnc;
					$notification->observer_id3 = false;
				}else{
					$notification->message = $mc;
					$notification->observer_id3 = true;
				}
				$notification->save();
			}else{
				$notification = Notification::where('user_id', $u->id)
							 ->where('type', self::TYPE)
							 ->where('observer', self::OBSERVER)
							 ->where('observer_id', $data['ap']->id)
							 ->where('observer_id2', $data['ap']->schedule_id)
							 ->first();
				if($notification) $notification->delete();
			}

			//recalculate future
			foreach($l as $key=>$id){
				$notification = Notification::where('user_id', $u->id)
							 ->where('type', self::TYPE)
							 ->where('observer', self::OBSERVER)
							 ->where('observer_id', $id)
							 ->where('observer_id2', $data['ap']->schedule_id)
							 ->first();
				$covered = (($key+1) <= $total);
				if(isset($notification) && $covered != $notification->observer_id3){
					$notification->observer_id3 = $covered;
					$notification->message = ($covered) ? $mc : $mnc;
					$notification->save();
				}
			}

		}
	}

	public function detach($data)
	{
		$notification = Notification::where('user_id', $data['user']->id)
							 ->where('type', self::TYPE)
							 ->where('observer', self::OBSERVER)
							 ->where('observer_id', $data['appointment']->id)
							 ->where('observer_id2', $data['appointment']->schedule_id)
							 ->first();
		if($notification) $notification->delete();

		$l = array();
		$missed = $data['user']->appointments()->where('schedule_id', $data['appointment']->schedule_id)->orderBy('start_time')->get()->each(function($a) use ( &$l){
			if($a->pivot->is_missed) $l[] = $a->id;
		});

		$total = $data['pivot']->miss_count;
		$t = $data['appointment']->start_time;
		$mc = str_replace('%date%', date('Y-m-d G:i', strtotime($t)), self::MESSAGE_COVERED);
		$mnc = str_replace('%date%', date('Y-m-d G:i', strtotime($t)), self::MESSAGE_NOTCOVERED);
		$mnc = str_replace('%absent%', $total, self::MESSAGE_NOTCOVERED);

		foreach($l as $key=>$id){
			$notification = Notification::where('user_id', $data['user']->id)
						 ->where('type', self::TYPE)
						 ->where('observer', self::OBSERVER)
						 ->where('observer_id', $id)
						 ->where('observer_id2', $data['appointment']->schedule_id)
						 ->first();
			$covered = (($key+1) <= $total);
			if(($notification) && ($covered != $notification->observer_id3)){
				$notification->observer_id3 = $covered;
				$notification->message = ($covered) ? $mc : $mnc;
				$notification->save();
			}
		}
	}

}


\Event::listen('appointment.user.attach', 'Observers\Notifications\AppointmentIsBeforeExam@attach');
\Event::listen('appointment.user.attach', 'Observers\Notifications\AppointmentIsLast@attach');
\Event::listen('appointment.user.attach', 'Observers\Notifications\AppointmentIsEnded@attach');
\Event::listen('appointment.user.detach', 'Observers\Notifications\AppointmentIsBeforeExam@detach');
\Event::listen('appointment.user.detach', 'Observers\Notifications\AppointmentIsLast@detach');
\Event::listen('appointment.user.detach', 'Observers\Notifications\AppointmentIsEnded@detach');
\Event::listen('appointment.user.detach', 'Observers\Notifications\AppointmentIsMissed@detach');
\Event::listen('appointment.attend.update', 'Observers\Notifications\AppointmentIsMissed@update');