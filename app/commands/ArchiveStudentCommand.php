<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class ArchiveStudentCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'cron:archivestudent';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Seperate inactive students to another group.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$start_time = time();

		$users = User::where('role', 3)->get();
		$users->each(function($u){
			$inactive = true;

			$classes = $u->schedules;
			foreach($classes as $c){
				$cid = $c->id;
				$class = $u->schedules->find($c->id)->pivot;

				$status = true;

				if(date('Y-m-d') > $class->expiry_date) continue;

				$count = 0;
				$missed = 0;
				$after = 0;
				$u->appointments->each(function($a) use (&$class, &$cid, &$count, &$missed, &$after){
					if($a->schedule_id == $cid) 
						$count++;
					if(($a->schedule_id == $cid) && $a->pivot->is_missed)
						$missed++;
					if($a->start_time > date('Y-m-d H:i:s'))
						$after++;
				});

				$missed = min($class->miss_count, $missed);
				//if($missed > $class->miss_count) $missed = $class->miss_count;
				$has_session = ($class->total_session - $class->used_session) - ($count - $missed) + $after;
				if($has_session >= 1){
					$inactive = false;
					break;
				}
			}

			$u->is_inactive = $inactive;
			$u->save();
		});

		$end_time = time();
		$this->updateTimestamp($start_time, $end_time);

		$this->info('cron job done.');
	}

	protected function updateTimestamp($start_time, $end_time)
	{
		$last_run = Setting::firstorNew(array('key' => 'cron_archive_student_lastrun'));
		$last_run->value = $start_time;
		$last_run->save();

		$last_run_duration = Setting::firstorNew(array('key' => 'cron_archive_student_lastrun_duration'));
		$last_run_duration->value = $end_time-$start_time;
		$last_run_duration->save();		
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array();
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array();
	}

}
