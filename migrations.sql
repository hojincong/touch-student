# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 128.199.73.105 (MySQL 5.7.21-0ubuntu0.16.04.1)
# Database: touchstu_jb
# Generation Time: 2018-04-13 09:35:14 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table te_migrations
# ------------------------------------------------------------

LOCK TABLES `te_migrations` WRITE;
/*!40000 ALTER TABLE `te_migrations` DISABLE KEYS */;

INSERT INTO `te_migrations` (`migration`, `batch`)
VALUES
	('2015_01_20_134324_create_acl_table',1),
	('2015_01_20_134541_create_user_table',1),
	('2015_01_20_135632_create_courses_table',1),
	('2015_01_20_140458_create_syllabus_table',1),
	('2015_01_20_140945_create_attachments_table',1),
	('2015_01_20_141552_create_attachurl_table',1),
	('2015_01_20_184358_create_schedules_table',1),
	('2015_01_20_185431_create_schedules_students_table',1),
	('2015_01_20_211633_create_schedules_tutors_table',1),
	('2015_01_20_212234_create_appointments_table',1),
	('2015_01_20_213719_create_attendance_table',1),
	('2015_01_20_230056_create_remarks_table',1),
	('2015_01_21_144035_create_appointments_users_table',1),
	('2015_02_16_010020_create_settings_table',1),
	('2015_03_26_021514_create_notification_table',1),
	('2015_06_15_193523_create_settings_notifications_table',1),
	('2015_07_03_131334_add_inactive_to_user_table',1),
	('2015_08_17_155513_add_column_used_exam',1),
	('2016_05_25_125551_add_remarks_table',2),
	('2016_07_11_175232_add_marking_to_apuser_table',3),
	('2016_07_28_195411_add_remarks2_to_appointments',4),
	('2016_07_29_212635_add_mark_at_to_appointment',4);

/*!40000 ALTER TABLE `te_migrations` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
